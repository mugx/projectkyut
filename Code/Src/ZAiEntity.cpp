#include "ZAiEntity.h"

ZAiEntity::ZAiEntity()
: m_numLives(0),
m_currentStatus(STATUS_IDLE)
{
	
}

ZAiEntity::~ZAiEntity()
{

}

ZAiEntity::AiStatus ZAiEntity::getStatus()
{
	return m_currentStatus;
}

void ZAiEntity::setStatus(AiStatus _status)
{
	m_currentStatus = _status;
}

short ZAiEntity::getNumLives()
{
	return m_numLives;
}

void ZAiEntity::setNumLives(short _numLives)
{
	m_numLives = _numLives;
}

bool ZAiEntity::getIsAlive() 
{
	return m_currentStatus == STATUS_ALIVE || m_currentStatus == STATUS_BEGIN_INVULNERABLE || m_currentStatus == STATUS_INVULNERABLE;
}

void ZAiEntity::takeHit()
{
	if (getStatus() != STATUS_ALIVE) return;

	--m_numLives;

	if (m_numLives == 0)
	{
		die();
	}
}