#include "ZScenarioManager.h"

#include "ZConst.h"
#include "ZCore.h"
#include "ZBackground.h"
#include "ZSkybox.h"

ZScenarioManager::ZScenarioManager()
: m_skybox(0)
{

}

ZScenarioManager::~ZScenarioManager()
{
	resetScenario();
}

void ZScenarioManager::resetScenario()
{
	//background
	for (std::vector<ZBackground*>::iterator it = m_backrounds.begin(), end = m_backrounds.end(); it != end;)
	{
		delete (*it++);
	}
	m_backrounds.clear();

	for (std::map<int, ZMesh*>::iterator it = m_meshesForBackgrounds.begin(), end = m_meshesForBackgrounds.end(); it != end;)
	{
		ZMeshManager::getInstance()->removeMesh(it++->second);
	}
	m_meshesForBackgrounds.clear();

	for (std::map<int, ZTexture*>::iterator it = m_texturesForBackgrounds.begin(), end = m_texturesForBackgrounds.end(); it != end;)
	{
		ZTextureManager::getInstance()->removeTexture(it++->second);
	}
	m_texturesForBackgrounds.clear();

	//skybox
	z_delete(m_skybox);

	for (std::map<int, ZTexture*>::iterator it = m_texturesForSkybox.begin(), end = m_texturesForSkybox.end(); it != end;)
	{
		ZTextureManager::getInstance()->removeTexture(it++->second);
	}
	m_texturesForSkybox.clear();
}

//---background---/
void ZScenarioManager::addBackground(int _id, ZLevel* _level, Vector3& _position)
{
	ZBackground* background = 0;
	switch (_id)
	{
	default:
		{
			background = new ZBackground(_id, _level, _position);
		}
		break;
	}

	if (background)
	{
		m_backrounds.push_back(background);
	}
}


ZBackground* ZScenarioManager::getBackground(int _id)
{
	for (int i = 0;i < m_backrounds.size();++i)
	{
		if (m_backrounds[i]->getId() == _id)
		{
			return m_backrounds[i];
		}
	}

	return 0;
}

void ZScenarioManager::addBackgroundTexture(int _id, QString _resourceName)
{
	QString fullResourceName = ZConst::PATH_BACKGROUND_TEXTURES + _resourceName;
	ZTexture* texture = ZTextureManager::getInstance()->loadTexture(fullResourceName);
	m_texturesForBackgrounds[_id] = texture;
}

ZTexture* ZScenarioManager::getBackgroundTexture(int _id)
{
	return m_texturesForBackgrounds.find(_id)->second;
}

ZMesh* ZScenarioManager::getBackgroundMesh(int _id)
{
	return m_meshesForBackgrounds.find(_id)->second;	
}

void ZScenarioManager::addBackgroundMesh(int _id, QString _resourceName, Vector3& _scale)
{
	QString fullResourceName = ZConst::PATH_BACKGROUND_MODELS + _resourceName;
	ZMesh* mesh = ZMeshManager::getInstance()->loadMesh(fullResourceName.toStdString(), _scale);
	m_meshesForBackgrounds[_id] = mesh;
}

void ZScenarioManager::addSkybox(int _id, ZLevel* _level)
{
	m_skybox = new ZSkybox(_id, _level);
}

ZSkybox* ZScenarioManager::getSkybox()
{
	return m_skybox;
}

void ZScenarioManager::addSkyboxTexture(int _id, QString _resourceName)
{
	QString fullResourceName = ZConst::PATH_SKYBOX_TEXTURES + _resourceName;
	ZTexture* texture = ZTextureManager::getInstance()->loadTexture(fullResourceName, true);
	m_texturesForSkybox[_id] = texture;
}

ZTexture* ZScenarioManager::getSkyboxTexture(int _id)
{
	return m_texturesForSkybox.find(_id)->second;
}

void ZScenarioManager::update()
{
	for_each(m_backrounds.begin(), m_backrounds.end(), std::mem_fun(&ZBackground::update));

	m_skybox->update();
}

void ZScenarioManager::drawBackgrounds()
{
	for_each(m_backrounds.begin(), m_backrounds.end(), std::mem_fun(&ZBackground::draw));
}

void ZScenarioManager::drawSkybox()
{
	m_skybox->draw();
}