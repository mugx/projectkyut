#include "ZSplashScreenHud.h"

#include "ZConfig.h"
#include "ZConst.h"

ZSplashScreenHud::ZSplashScreenHud()
: m_textComponent1(0),
m_textComponent2(0)
{
	refreshHud();
}

ZSplashScreenHud::~ZSplashScreenHud()
{
	destroyHud();
}

void ZSplashScreenHud::destroyHud()
{
	if (m_textComponent1) 
	{
		delete m_textComponent1;
		m_textComponent1 = 0;
	}

	if (m_textComponent2)
	{
		delete m_textComponent2;
		m_textComponent2 = 0;
	}
}

void ZSplashScreenHud::refreshHud()
{	
	destroyHud();

	//--- "Project Kyut" ---//
	m_textComponent1 = new ZTextComponent(40);
	m_textComponent1->m_text = tr("Project Kyut");
	m_textComponent1->m_x = ZConfig::s_size.width() / 2.0 - m_textComponent1->countPixelsWidth() / 3.0;
	m_textComponent1->m_y = ZConfig::s_size.height() / 2.0 + 20.0;
	m_textComponent1->setTopColor(ZConst::top_color_white);
	m_textComponent1->setBottomColor(ZConst::bottom_color_green);

	//--- "by Zed (2012)" ---//	
	m_textComponent2 = new ZTextComponent(10);
	m_textComponent2->m_text = tr("by Zed (2013)");
	m_textComponent2->m_x = ZConfig::s_size.width() / 2.0 - m_textComponent2->countPixelsWidth() / 3.0;
	m_textComponent2->m_y = 50.0;
	m_textComponent2->setTopColor(ZConst::top_color_white);
	m_textComponent2->setBottomColor(ZConst::bottom_color_violet);
}

void ZSplashScreenHud::draw()
{
	beginDrawString();
	m_textComponent1->renderText();
	m_textComponent2->renderText();
	endDrawString();
}