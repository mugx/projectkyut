#include "ZParticleProxy.h"
#include "ZParticleSystem.h"
#include "ZLevel.h"
#include "ZParticle.h"
#include <gl/glew.h>

#define NUM_PARTICLES_AVAILABLE 500

ZParticleProxy::ZParticleProxy(ZLevel& _level)
: m_level(_level)
{
	for (int i = 0;i < NUM_PARTICLES_AVAILABLE;++i)
	{
		m_particles.push_back(new ZParticle());
	}
}

ZParticleProxy::~ZParticleProxy()
{
	for (std::vector<ZParticle*>::iterator it = m_particles.begin(), end = m_particles.end();it != end;)
	{
		delete (*it++);
	}
	m_particles.clear();
	m_visibleParticles.clear();
}

void ZParticleProxy::doEmit(ZParticleSystem* _particleSystem)
{
	int numParticlesFound = 0;

	for (std::vector<ZParticle*>::iterator it = m_particles.begin(), end = m_particles.end();it != end;++it)
	{
		ZParticle* currentParticle = *it;
		if (!currentParticle->getIsVisible())
		{			
			_particleSystem->initParticle(currentParticle);
			++numParticlesFound;
		}

		if (numParticlesFound == _particleSystem->getNumParticles())
		{
			break;
		}
	}
}

void ZParticleProxy::update()
{
	m_visibleParticles.clear();

	for (std::vector<ZParticle*>::iterator it = m_particles.begin(), end = m_particles.end();it != end;++it)
	{
		ZParticle* currentParticle = *it;
		if (currentParticle->getIsVisible())
		{
			currentParticle->update();
			m_visibleParticles.push_back(currentParticle);
		}
	}
}

void ZParticleProxy::draw()
{
	glEnable(GL_BLEND);
	glBlendFunc(GL_ONE, GL_ONE);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);

	std::for_each(m_visibleParticles.begin(), m_visibleParticles.end(), std::mem_fun(&ZParticle::draw));

	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
	glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
}
