#ifndef ZExplosionSystem_h
#define ZExplosionSystem_h

#include "ZParticleSystem.h"
#include "ZTextureManager.h"
#include "ZParticle.h"
#include "ZBaseEntity.h"

class ZExplosionSystem : public ZParticleSystem
{
	friend class ZParticle;

public:
	ZExplosionSystem(ZBaseEntity& _baseEntityParent);
	virtual ~ZExplosionSystem();
	virtual void initParticle(ZParticle* _particle);
	virtual void update(ZParticle* _particle);
	virtual void draw(ZParticle* _particle);
	
	void orientTowardsCamera();
	
private:
	ZBaseEntity& m_baseEntityParent;
	ZTexture* m_textureExplosion;
	float m_slowdown;
};

#endif