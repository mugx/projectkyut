#ifndef ZGameEngine_h
#define ZGameEngine_h

#include <map>
#include "ZSingleton.h"

class ZGameEngine : public ZSingleton <ZGameEngine>
{
	friend class ZSingleton <ZGameEngine>;

public:

private:
	explicit ZGameEngine();
	virtual ~ZGameEngine();
};

#endif