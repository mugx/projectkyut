#include "ZConfig.h"
#include "ZCamera.h"

#ifdef DEBUG
bool ZConfig::s_isDrawFps = true;
bool ZConfig::s_isDrawDebug = true;
bool ZConfig::s_isDrawBV = false;
bool ZConfig::s_isFullScreen = false;
bool ZConfig::s_isSoundEnabledByDefault = false;
#endif

#ifdef RELEASE
bool ZConfig::s_isDrawFps = false;
bool ZConfig::s_isDrawDebug = false;
bool ZConfig::s_isDrawBV = false;
bool ZConfig::s_isFullScreen = true;
bool ZConfig::s_isSoundEnabledByDefault = true;
#endif

QSize ZConfig::s_size = QSize();
bool ZConfig::s_isPause = false;
bool ZConfig::s_isGameOver = false;

float ZConfig::s_interpolation = 0.0f;
int ZConfig::s_numLights = 2;

int ZConfig::s_currentDebugSelection = 0;
float ZConfig::s_exposure = 0.0f;
float ZConfig::s_factor = 0.0f;
float ZConfig::s_gamma = 0.0f;
float& ZConfig::s_zoom = ZCamera::getInstance()->eye.m_z;
int ZConfig::s_blinking = 5;

bool ZConfig::s_isLoading = false;