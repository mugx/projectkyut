#ifndef ZTimer_h
#define ZTimer_h

class ZTimer
{
public:
	ZTimer(int _timeout);
	~ZTimer();
	void startTimer();
	void stopTimer();
	bool isRunning();
	bool isTimeout();
	void update();

private:
	int m_timeout;
	int m_initialTimeout;
	bool m_isRunning;
};

#endif