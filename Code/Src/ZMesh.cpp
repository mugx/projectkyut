#include "ZMesh.h"
#include <iostream>

ZMesh::ZMesh(Vector3 _scale) 
: m_scale(_scale),
m_alpha(1.0f),
m_vertices(0),
m_normals(0),
m_texCoords(0),
m_triangles(0),
m_verticesArray(0),
m_normalsArray(0),
m_texCoordsArray(0),
m_polygonsCardinality(0),
m_verticesCardinality(0)
{
	m_idMesh = reinterpret_cast<unsigned int>(this);
}

ZMesh::~ZMesh() 
{
	if (0 != m_verticesArray)
	{
		delete[] m_verticesArray;
		m_verticesArray = 0;
	}
	
	if (0 != m_normalsArray)
	{
		delete[] m_normalsArray;
		m_normalsArray = 0;
	}

	if (0 != m_texCoordsArray)
	{
		delete[] m_texCoordsArray;
		m_texCoordsArray = 0;
	}
}

void ZMesh::bindTexture(ZTexture* _texture)
{
	if (_texture->m_loadingResult)
	{
		m_idTexture = _texture->m_idTexture;
	}
}

void ZMesh::makeTexCoordsArray()
{
	m_texCoordsArray = new GLfloat[m_polygonsCardinality * 6];
	for (int i = 0, j = 0;i < m_polygonsCardinality;++i)
	{
		m_texCoordsArray[j++] = m_texCoords[m_triangles[i].m_x].m_x;
		m_texCoordsArray[j++] = m_texCoords[m_triangles[i].m_x].m_y;

		m_texCoordsArray[j++] = m_texCoords[m_triangles[i].m_y].m_x;
		m_texCoordsArray[j++] = m_texCoords[m_triangles[i].m_y].m_y;

		m_texCoordsArray[j++] = m_texCoords[m_triangles[i].m_z].m_x;
		m_texCoordsArray[j++] = m_texCoords[m_triangles[i].m_z].m_y;
	}
	
	delete[] m_texCoords;
	m_texCoords = 0;
}

void ZMesh::makeNormalsArray()
{
	Vector3 vect1, vect2, vect3;
	Vector3 vect_b1, vect_b2, normal;
	int l_connections_qty[MAX_VERTICES] = {0}; // numero di poligoni collegati ad ogni vertice

	for (int i = 0; i < m_verticesCardinality;++i)
	{
		m_normals[i].m_x = 0.0;
		m_normals[i].m_y = 0.0;
		m_normals[i].m_z = 0.0;
		l_connections_qty[i] = 0;
	}

	for (int i = 0; i < m_polygonsCardinality;++i)
	{		
		vect1.m_x = m_vertices[m_triangles[i].m_x].m_x;
		vect1.m_y = m_vertices[m_triangles[i].m_x].m_y;
		vect1.m_z = m_vertices[m_triangles[i].m_x].m_z;

		vect2.m_x = m_vertices[m_triangles[i].m_y].m_x;
		vect2.m_y = m_vertices[m_triangles[i].m_y].m_y;
		vect2.m_z = m_vertices[m_triangles[i].m_y].m_z;

		vect3.m_x = m_vertices[m_triangles[i].m_z].m_x;
		vect3.m_y = m_vertices[m_triangles[i].m_z].m_y;
		vect3.m_z = m_vertices[m_triangles[i].m_z].m_z;         

		ZMath::vectCreate(vect1, vect2, vect_b1);
		ZMath::vectCreate(vect1, vect3, vect_b2);
		ZMath::crossProduct(vect_b1, vect_b2, normal);
		ZMath::vectNormalize(normal);

		// per ogni vertice condiviso di questo poligono, incrementiamo il numero di vertici
		++l_connections_qty[m_triangles[i].m_x]; 
		++l_connections_qty[m_triangles[i].m_y];
		++l_connections_qty[m_triangles[i].m_z];

		m_normals[m_triangles[i].m_x].m_x += normal.m_x; // per ogni vertice condiviso da questo poligono, aggiungiamo la normale
		m_normals[m_triangles[i].m_x].m_y += normal.m_y;
		m_normals[m_triangles[i].m_x].m_z += normal.m_z;
		m_normals[m_triangles[i].m_y].m_x += normal.m_x;
		m_normals[m_triangles[i].m_y].m_y += normal.m_y;
		m_normals[m_triangles[i].m_y].m_z += normal.m_z;
		m_normals[m_triangles[i].m_z].m_x += normal.m_x;
		m_normals[m_triangles[i].m_z].m_y += normal.m_y;
		m_normals[m_triangles[i].m_z].m_z += normal.m_z;	
	}	

	for (int i = 0; i < m_verticesCardinality;++i)
	{
		if (l_connections_qty[i] > 0)
		{
			m_normals[i].m_x /= l_connections_qty[i]; // calcoliamo la media delle normali per avere la normale risultante
			m_normals[i].m_y /= l_connections_qty[i];
			m_normals[i].m_z /= l_connections_qty[i];
		}
	}

	m_normalsArray = new GLfloat[m_polygonsCardinality * 9];
	for (int i = 0, k = 0;i < m_polygonsCardinality;++i)
	{
		m_normalsArray[k++] = m_normals[m_triangles[i].m_x].m_x;
		m_normalsArray[k++] = m_normals[m_triangles[i].m_x].m_y;
		m_normalsArray[k++] = m_normals[m_triangles[i].m_x].m_z;

		m_normalsArray[k++] = m_normals[m_triangles[i].m_y].m_x;
		m_normalsArray[k++] = m_normals[m_triangles[i].m_y].m_y;
		m_normalsArray[k++] = m_normals[m_triangles[i].m_y].m_z;

		m_normalsArray[k++] = m_normals[m_triangles[i].m_z].m_x;
		m_normalsArray[k++] = m_normals[m_triangles[i].m_z].m_y;
		m_normalsArray[k++] = m_normals[m_triangles[i].m_z].m_z;
	}

	delete[] m_normals;
	m_normals = 0;
}

void ZMesh::makeVerticesArray()
{
	m_verticesArray = new GLfloat[m_polygonsCardinality * 9];
	for (int i = 0, j = 0;i < m_polygonsCardinality;++i)
	{ 				
		m_verticesArray[j++] = m_vertices[m_triangles[i].m_x].m_x * m_scale.m_x;
		m_verticesArray[j++] = m_vertices[m_triangles[i].m_x].m_y * m_scale.m_y;
		m_verticesArray[j++] = m_vertices[m_triangles[i].m_x].m_z * m_scale.m_z;

		m_verticesArray[j++] = m_vertices[m_triangles[i].m_y].m_x * m_scale.m_x;
		m_verticesArray[j++] = m_vertices[m_triangles[i].m_y].m_y * m_scale.m_y;
		m_verticesArray[j++] = m_vertices[m_triangles[i].m_y].m_z * m_scale.m_z;

		m_verticesArray[j++] = m_vertices[m_triangles[i].m_z].m_x * m_scale.m_x;
		m_verticesArray[j++] = m_vertices[m_triangles[i].m_z].m_y * m_scale.m_y;
		m_verticesArray[j++] = m_vertices[m_triangles[i].m_z].m_z * m_scale.m_z;
	}

	/*
	delete[] vertices;
	vertices = 0;
	*/
}

void ZMesh::makeMesh(bool _isSimple)
{	
	if (!_isSimple)
	{
		makeNormalsArray();
		makeVerticesArray();
		makeTexCoordsArray();
	}
}

void ZMesh::draw()
{
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); //no alfa blending
	glVertexPointer(3, GL_FLOAT, 0, m_verticesArray);
	glNormalPointer(GL_FLOAT, 0, m_normalsArray);
	glTexCoordPointer(2, GL_FLOAT, 0, m_texCoordsArray);
	glColor4f(1.0f, 1.0f, 1.0f, m_alpha);
	glBindTexture(GL_TEXTURE_2D, m_idTexture);	
	glDrawArrays(GL_TRIANGLES, 0, m_polygonsCardinality * 3);
	glBindTexture(GL_TEXTURE_2D, 0);	
}
