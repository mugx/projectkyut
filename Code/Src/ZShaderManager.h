#ifndef ZShaderManager_h
#define ZShaderManager_h

#include "ZSingleton.h"
#include "ZShader.h"

class ZShaderManager : public ZSingleton <ZShaderManager>
{	
	friend class ZSingleton <ZShaderManager>;

public:
	ZShader shaderPHONG;
	ZShader shaderHIGHPASS;
	ZShader shaderGAUSSH;
	ZShader shaderGAUSSV;
	ZShader shaderTONEMAP;
	//others
	ZShader shaderSIMPLE;
	ZShader shaderPARTICLES;

private:
	explicit ZShaderManager();
	virtual ~ZShaderManager();
};

#endif
