#ifndef ZSkybox_h
#define ZSkybox_h

#include <gl/glew.h>
#include "ZMath.h"

class ZTexture;
class ZLevel;

class ZSkybox
{
public:
	enum SkyboxTextureType
	{
		SKYBOX_TOP = 0,
		SKYBOX_BOTTOM,
		SKYBOX_LEFT,
		SKYBOX_RIGHT,
		SKYBOX_FRONT,
		SKYBOX_BACK
	};

	explicit ZSkybox(int _id, ZLevel* _level);
	virtual ~ZSkybox();

	void update();
	void draw();	

private:
	static GLfloat s_verticesArray_back[12];
	static GLfloat s_verticesArray_front[12];
	static GLfloat s_verticesArray_right[12];
	static GLfloat s_verticesArray_left[12];
	static GLfloat s_verticesArray_top[12];
	static GLfloat s_verticesArray_bottom[12];
	static GLfloat s_texCoordsArray_back[8];
	static GLfloat s_texCoordsArray_right[8];
	static GLfloat s_texCoordsArray_front[8];
	static GLfloat s_texCoordsArray_left[8];
	static GLfloat s_texCoordsArray_bottom[8];
	static GLfloat s_texCoordsArray_top[8];
	
	ZTexture* m_textures[6];
	// space
	Vector3 m_position;
	Vector3 m_rotation;
	Vector3 m_viewRotation;
	Vector3 m_angularVelocity;
	// general
	int m_id;
	ZLevel* m_currentLevel;
};

#endif