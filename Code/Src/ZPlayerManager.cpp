#include "ZPlayerManager.h"

#include "ZConst.h"
#include "ZConfig.h"

ZPlayerManager* ZPlayerManager::s_instance = 0;

ZPlayerManager::ZPlayerManager()
: playerWithKeyboard(0),
playerWithPad(0),
m_level(0)
{

}

ZPlayerManager::~ZPlayerManager()
{
	resetPlayers();
}

void ZPlayerManager::resetPlayers()
{
	playerWithKeyboard = 0;
	playerWithPad = 0;
	for (std::vector<ZPlayer*>::iterator it = m_players.begin(), end = m_players.end(); it != end;)
	{
		delete (*it++);
	}
	m_players.clear();

	for (std::map<int, ZMesh*>::iterator it = m_meshesForPlayers.begin(), end = m_meshesForPlayers.end(); it != end;)
	{
		ZMeshManager::getInstance()->removeMesh(it++->second);
	}
	m_meshesForPlayers.clear();

	for (std::map<int, ZTexture*>::iterator it = m_texturesForPlayers.begin(), end = m_texturesForPlayers.end(); it != end;)
	{
		ZTextureManager::getInstance()->removeTexture(it++->second);
	}
	m_texturesForPlayers.clear();
}

void ZPlayerManager::setKeyboardAction(QKeyEvent* nKeyEvent, bool nIsPressed)
{
	if (playerWithKeyboard)
	{
		switch (nKeyEvent->key())
		{
		case Qt::Key_Up:
			playerWithKeyboard->setAction(ZPlayer::ACTION_UP, nIsPressed);
			break;
		case Qt::Key_Down:
			playerWithKeyboard->setAction(ZPlayer::ACTION_DOWN, nIsPressed);
			break;
		case Qt::Key_Left:
			playerWithKeyboard->setAction(ZPlayer::ACTION_LEFT, nIsPressed);
			break;
		case Qt::Key_Right:
			playerWithKeyboard->setAction(ZPlayer::ACTION_RIGHT, nIsPressed);
			break;
		case Qt::Key_Space:
			playerWithKeyboard->setAction(ZPlayer::ACTION_FIRE, nIsPressed);
			break;
		}
	}
}

void ZPlayerManager::addPlayer(int _id, ZLevel* _level, Vector3& _position)
{
	ZPlayer* player = getPlayer(_id);
	if (!player) 
	{
		player = new ZPlayer(_id, _level, _position);
		switch (_id)
		{
		case PLAYER_WITH_KEYBOARD_TYPE:
			{
				playerWithKeyboard = player;
			}
			break;
		case PLAYER_WITH_PAD_TYPE:
			{
				playerWithPad = player;
			}
			break;
		}
		
		if (player)
		{
			m_players.push_back(player);
		}	
	}
	else //What means? when there is a game over, after we restart (but we don't recreate everything)
	{
		player->restart(_level, _position);
	}
}

ZPlayer* const ZPlayerManager::getNearestPlayer(Vector3& nPosition)
{
	float min = 100000.0f;
	float tempDistance = 0.0f;
	ZPlayer* nearestPlayer = 0;
	for (std::vector<ZPlayer*>::iterator it = m_players.begin(), end = m_players.end();it != end;++it)
	{
		tempDistance = ZMath::distance(nPosition, (*it)->getPosition());
		if (tempDistance <= min)
		{	
			min = tempDistance;
			nearestPlayer = *it;
		}
	}
	return nearestPlayer;
}

ZPlayer* ZPlayerManager::getPlayer(int _id)
{
	int size = m_players.size();
	for (int i = 0;i < size;++i)
	{
		if (m_players[i]->getId() == _id)
		{
			return m_players[i];
		}
	}

	return 0;
}

bool ZPlayerManager::arePlayersAlive()
{
	bool areAlive = true;
	for (std::vector<ZPlayer*>::iterator it = m_players.begin(), end = m_players.end();it != end;++it)
	{
		areAlive &= (*it)->getIsAlive();
	}
	return areAlive;
}

ZMesh* ZPlayerManager::getMesh(int _id)
{
	return m_meshesForPlayers.find(_id)->second;	
}

ZTexture* ZPlayerManager::getTexture(int _id)
{
	return m_texturesForPlayers.find(_id)->second;
}

void ZPlayerManager::addPlayerMesh(int _id, QString _resourceName, Vector3& _scale)
{
	QString fullResourceName = ZConst::PATH_PLAYER_MODELS + _resourceName;
	ZMesh* mesh = ZMeshManager::getInstance()->loadMesh(fullResourceName.toStdString(), _scale);
	m_meshesForPlayers[_id] = mesh;
}

void ZPlayerManager::addPlayerTexture(int _id, QString _resourceName)
{
	QString fullResourceName = ZConst::PATH_PLAYER_TEXTURES + _resourceName;
	ZTexture* texture = ZTextureManager::getInstance()->loadTexture(fullResourceName);
	m_texturesForPlayers[_id] = texture;
}

void ZPlayerManager::update()
{
	for_each(m_players.begin(), m_players.end(), std::mem_fun(&ZPlayer::update));

	if (!arePlayersAlive())
	{
		ZConfig::s_isGameOver = true;
	}
}

void ZPlayerManager::draw()
{
	for_each(m_players.begin(), m_players.end(), std::mem_fun(&ZPlayer::draw));
}