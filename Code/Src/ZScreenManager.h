#ifndef ZScreenManager_h
#define ZScreenManager_h

#include <GL/glew.h>
#include <QtCore>
#include <QGLWidget>
#include "ZSingleton.h"
#include "ZScreen.h"

class ZScreen;
class ZHud;
class ZTexture;

class ZScreenManager : public QGLWidget, public ZSingleton<ZScreenManager>
{
	friend class ZSingleton <ZScreenManager>;
	friend class ZLevel;

	Q_OBJECT

public:
	QTime& getTime() { return m_time; }
	void init();
	ZScreen* GetCurrentScreen() { return m_currentScreen; }
	void setCurrentScreen(ZScreen::ScreenType _screenType);
	void setFullScreen(bool _isFullScreen, int _width = 800, int _height = 600);	
	void refreshBuffers();
	void makeDisplayList();
	void releaseDisplayList();
	void drawBackground(ZTexture* _textureBackground);
	void drawBackground();
	void perspectiveMode();
	void orthoMode(int _left, int _top, int _right, int _bottom);
	
public slots:	
	void timerEvent(QTimerEvent* _event);

protected:
	//graphics
	virtual void initializeGL();
	virtual void resizeGL(int width, int height);
	virtual void paintGL();
	//events
	virtual void closeEvent(QCloseEvent* e) { e->ignore(); }
	virtual void mousePressEvent(QMouseEvent* e) { e->ignore(); }
	virtual void mouseMoveEvent(QMouseEvent* e) { e->ignore(); }
	virtual void keyReleaseEvent(QKeyEvent* _keyboardEvent);
	virtual void keyPressEvent(QKeyEvent* _keyboardEvent);
	virtual void changeEvent(QEvent* event);

private:
	explicit ZScreenManager();
	virtual ~ZScreenManager();
	
	void StartScreen();

	static ZScreenManager* _instance;	
	ZScreen* m_currentScreen;	
	QTime m_time;
	QBasicTimer m_timer;
	int m_indexDisplayListBackground;

	//--- 3d scene ---//
	ZTexture* m_textureScene;
	GLuint m_fboScene;
	GLuint m_rboScene;

	ZTexture* m_textureSceneStencil;
	GLuint m_fboSceneStencil;
	GLuint m_rboSceneStencil;
	
	//--- high pass ---//
	ZTexture* m_textureHighpass;
	GLuint m_fboHighpass;

	//--- gauss h ---//
	ZTexture* m_textureGaussH;
	GLuint m_fboGaussH;

	//--- gauss v ---//
	ZTexture* m_textureGaussV;
	GLuint m_fboGaussV;
};

#endif