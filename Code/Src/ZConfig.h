#ifndef ZConfig_h
#define ZConfig_h

#include <QSize>

class ZConfig
{
public:
	enum DebugSelectionType
	{
		EXPOSURE_SELECTED,
		FACTOR_SELECTED,
		GAMMA_SELECTED,
		ZOOM_SELECTED,
		BLINKING_SELECTED,
		COUNT_DEBUG_SELECTION_TYPE
	};
	static QSize s_size;
	static bool s_isDrawFps;
	static bool s_isDrawDebug;
	static bool s_isDrawBV;	
	static bool s_isPause;
	static bool s_isGameOver;
	static bool s_isFullScreen;	
	static bool s_isSoundEnabledByDefault;
	static float s_interpolation;
	static int s_numLights;	
	static int s_currentDebugSelection;
	static float s_exposure;
	static float s_factor;
	static float s_gamma;
	static float& s_zoom;
	static int s_blinking;
	// status
	static bool s_isLoading;
};

#endif