#include <iostream>
#include <QFile>
#include <QDataStream>
#include <QDebug>
#include "ZMeshManager.h"
#include "ZMath.h"

ZMeshManager::ZMeshManager() 
{
}

ZMeshManager::~ZMeshManager() 
{
	for(std::set<ZMesh*>::iterator it = m_meshes.begin(), end = m_meshes.end();it != end;++it)
	{
		delete (*it);
	}
	m_meshes.clear();
}

void ZMeshManager::removeMesh(ZMesh* mesh)
{
	m_meshes.erase(mesh);
	delete mesh;
}

ZMesh* ZMeshManager::loadMesh(const std::string& _filename, Vector3& _scale)
{	
	qDebug() << "[ZMeshManager::LoadMesh] Loading: " << QString::fromStdString(_filename) << "...";
	ZMesh* mesh = new ZMesh(_scale);
	if (mesh->m_loadingResult = load3DS(*mesh, _filename))
	{	
		mesh->makeMesh();
		m_meshes.insert(mesh);
	}
	else
	{
		qDebug() << "[ZMeshManager::LoadMesh] Error";
	}

	return mesh;
}

ZMesh* ZMeshManager::loadSimpleMesh()
{
	ZMesh* mesh = new ZMesh(Vector3(1.0f));
	mesh->makeMesh(true);
	return mesh;
}

/*
* Caricamento di un file 3DS, in input vuole un oggetto mesh e il nome del file.
* return true se e'stato caricato il file, false altrimenti
* per la struttura del formato 3ds: http://en.wikipedia.org/wiki/.3ds
*/
bool ZMeshManager::load3DS(ZMesh& _mesh, const std::string& _filename)
{	
	QFile file(_filename.c_str());
	if (!file.exists()) 
	{
		qDebug() << "[ZMeshManager::Load3DS] Error: File 3DS not found";
		return false;
	}

	//------------- CHUNK info ---------------//
	quint16 idChunk;
	quint32 lengthChunk;	
	//----------------------------------------//

	unsigned short cardinality; // numero di elementi in ogni chunk
	unsigned char charTemp; // char variabile	
	unsigned short face_flags; // Flag che immagazzina alcune informazioni sulla faccia
	
	file.open(QIODevice::ReadOnly);
	QDataStream in(&file);
	// STUDIARE IL DISCORSO ENDIAN
	in.setByteOrder(QDataStream::LittleEndian);	
	in.setVersion(11);

	while(!file.atEnd())
	{		
		in >> idChunk;
		in >> lengthChunk;

		switch (idChunk)
        {
			//----------------- MAIN3DS -----------------
			// descrizione: chunk principale contiene tutti gli altri chunk
			// ID chunk: 4d4d 
			// lunghezza del chunk: 0 + sotto chunks
			//-------------------------------------------
			case 0x4d4d: 
			break;    

			//----------------- EDIT3DS  -----------------
			// descrizione: objects layout info
			// ID chunk: 3d3d (hex)
			// lunghezza del chunk: 0 + sotto chunks
			//-------------------------------------------
			case 0x3d3d:
			break;

			//--------------- EDIT_OBJECT ---------------
			// descrizione: objects info 
			// ID chunk: 4000 (hex)
			// lunghezza del chunk: len(object name) + sotto chunks
			//-------------------------------------------
			case 0x4000:
				for (int i = 0;i < 20;++i)
				{
					in >> charTemp;
					_mesh.m_name[i] = charTemp;
					if (charTemp == '\0')
					{
						break;
					}
				}
			break;

			//--------------- OBJ_TRIMESH ---------------
			// descrizione: contiene i dati della mesh triangolare
			// ID chunk: 4100 (hex)
			// lunghezza del chunk: 0 + sotto chunks
			//-------------------------------------------
			case 0x4100:
			break;
			
			//--------------- TRI_VERTEXL ---------------
			// descrizione: lista dei Vertici
			// ID chunk: 4110 (hex)
			// lunghezza del chunk: 
			//               1 unsigned short per la cardinality dei vertici
			//             + 3 float per le coordinate vertice * cardinality vertici
			//             + sotto chunks
			//---------------------------------------------
			case 0x4110:
				in >> cardinality;
				_mesh.m_verticesCardinality = cardinality;
				if (_mesh.m_verticesCardinality > MAX_VERTICES) 
				{
					qDebug() << "[ZMeshManager::Load3DS] Error: Too many vertices (" << _mesh.m_verticesCardinality << ")";
					return false;
				}

				_mesh.m_vertices = new Vector3[_mesh.m_verticesCardinality];
				_mesh.m_normals = new Vector3[_mesh.m_verticesCardinality];
				
                for (int i = 0; i < _mesh.m_verticesCardinality; ++i)
                {
					in >> _mesh.m_vertices[i].m_x;
					in >> _mesh.m_vertices[i].m_y;
					in >> _mesh.m_vertices[i].m_z;
				}
				break;

			//--------------- TRI_FACEL1 ----------------
			// descrizione: lista dei poligoni (facce)
			// ID chunk: 4120 (hex)
			// lunghezza del chunk: 
			//				 1 unsigned short (cardinality dei poligoni) 
			//             + 3 x unsigned short (polygon points) * cardinality dei poligoni
			//             + sotto chunks
			//-------------------------------------------
			case 0x4120:
				in >> cardinality;
                _mesh.m_polygonsCardinality = cardinality;				    
				if (cardinality > MAX_POLYGONS)
				{
					qDebug() << "[ZMeshManager::Load3DS] Error: Too many polygons (" << _mesh.m_polygonsCardinality << ")";
					return false;
				}

				_mesh.m_triangles = new Vec3<short>[_mesh.m_polygonsCardinality];
                for (int i = 0; i < _mesh.m_polygonsCardinality; ++i)
                {
					in >> _mesh.m_triangles[i].m_x;
					in >> _mesh.m_triangles[i].m_y;
					in >> _mesh.m_triangles[i].m_z;
					in >> face_flags;
				}
                break;

			//------------- TRI_MAPPINGCOORS ------------ 
			// descrizione: Mapping Coordinates
			// ID chunk: 4140 (hex)
			// lunghezza del chunk: 1 unsigned short (cardinality dei punti da mappare)
			//             + 2 float (coordinate di mapping) x cardinality dei punti da mappare
			//             + sub chunks
			//-------------------------------------------
			case 0x4140:
				in >> cardinality;
				
				_mesh.m_texCoords = new Vector2[cardinality];
				for (int i = 0; i < cardinality; ++i)
				{
					in >> _mesh.m_texCoords[i].m_x;
					in >> _mesh.m_texCoords[i].m_y;
				}
                break;

			//----------- Skip dei chunks sconosciuti ------------
			// skippiamo i chunk non utilizzati, utilizziamo la lunghezza del chunk per puntare al chunk successivo
			//-------------------------------------------
			default:
				in.skipRawData(lengthChunk - 6);			
        } 
	}
	file.close();
	return true;
}