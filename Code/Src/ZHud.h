#ifndef ZHud_h
#define ZHud_h

#include <QtGui>
#include "ZScreenManager.h"
#include "ZTextComponent.h"

class ZGameScene;

class ZHud : public QObject
{
	Q_OBJECT
public:
    explicit ZHud();
	virtual ~ZHud() = 0;
	virtual void refreshHud() = 0;
	virtual void destroyHud() = 0;
	virtual void update() = 0;
	virtual void draw() = 0;

	void beginDrawString();	
	void endDrawString() const;
	void setOrthographicProjection() const;
	void resetPerspectiveProjection() const;
};

#endif