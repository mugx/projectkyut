#include "ZResourcesParser.h"

#include <QtXml> 
#include <sstream>
#include <iostream>
#include "ZConst.h"
#include "ZLevel.h"
#include "ZPlayerManager.h"
#include "ZEnemyManager.h"
#include "ZScenarioManager.h"

ZResourcesParser::ZResourcesParser(ZLevel* const _level)
: m_level(_level),
m_canContinue(false)
{
	std::stringstream fileName;
	fileName << ":/data/resources.xml";
	std::cout << "Parsing enemies..." << fileName.str().c_str() << std::endl;
	QFile xmlFile(fileName.str().c_str());
	QXmlInputSource source(&xmlFile);
	QXmlSimpleReader reader;
	reader.setContentHandler(this);
	reader.parse(source);
	std::cout << "Parsing enemies...OK" << std::endl;
}

bool ZResourcesParser::startDocument()
{
	return true;
}

bool ZResourcesParser::startElement(const QString& namespaceURI, const QString& localName, const QString& qName, const QXmlAttributes& attr)
{	
	m_currentTag = qName;
	m_currentAttributes = attr;

	if (m_currentTag == ZConst::TAG_RESOURCES)
	{
		m_canContinue = m_currentAttributes.value("id").toUInt() == m_level->getId() ? true : false;
	}
	else if (m_canContinue && m_currentTag == ZConst::TAG_PLAYER_RESOURCE)
	{
		int playerResourceId = m_currentAttributes.value("id").toUInt();
		QString meshName = m_currentAttributes.value("mesh");
		QString textureName = m_currentAttributes.value("texture");
		QStringList splitted = m_currentAttributes.value("scale").split(",");
		Vector3 scale(splitted.at(0).toFloat(), splitted.at(1).toFloat(), splitted.at(2).toFloat());

		ZPlayerManager::getInstance()->addPlayerMesh(playerResourceId, meshName, scale);
		ZPlayerManager::getInstance()->addPlayerTexture(playerResourceId, textureName);
	}
	else if (m_canContinue && m_currentTag == ZConst::TAG_ENEMY_RESOURCE)
	{	
		int enemyResourceId = m_currentAttributes.value("id").toUInt();
		QString meshName = m_currentAttributes.value("mesh");
		QString textureName = m_currentAttributes.value("texture");
		QStringList splitted = m_currentAttributes.value("scale").split(",");
		Vector3 scale(splitted.at(0).toFloat(), splitted.at(1).toFloat(), splitted.at(2).toFloat());

		ZEnemyManager::getInstance()->addEnemyMesh(enemyResourceId, meshName, scale);
		ZEnemyManager::getInstance()->addEnemyTexture(enemyResourceId, textureName);
	}
	else if (m_canContinue && m_currentTag == ZConst::TAG_BACKGROUND_RESOURCE)
	{	
		int backgroundResourceId = m_currentAttributes.value("id").toUInt();
		QString meshName = m_currentAttributes.value("mesh");
		QString textureName = m_currentAttributes.value("texture");
		QStringList splitted = m_currentAttributes.value("scale").split(",");
		Vector3 scale(splitted.at(0).toFloat(), splitted.at(1).toFloat(), splitted.at(2).toFloat());

		ZScenarioManager::getInstance()->addBackgroundMesh(backgroundResourceId, meshName, scale);
		ZScenarioManager::getInstance()->addBackgroundTexture(backgroundResourceId, textureName);
	}
	else if (m_canContinue && m_currentTag == ZConst::TAG_SKYBOX_RESOURCE)
	{
		int skyboxResourceId = m_currentAttributes.value("id").toUInt();
		QString textureName = m_currentAttributes.value("texture");
		ZScenarioManager::getInstance()->addSkyboxTexture(skyboxResourceId, textureName);
	}

	return true;
}

bool ZResourcesParser::characters(const QString& tag_value)
{
	return true;
}

bool ZResourcesParser::endElement(const QString& namespaceURI, const QString& localName, const QString& qName)
{
	m_currentTag = ZConst::TAG_UNKNOWN;
	m_currentAttributes.clear();
	m_canContinue = localName == ZConst::TAG_RESOURCES ? false : m_canContinue;
	return true;
}