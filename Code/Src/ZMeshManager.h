#ifndef ZMeshManager_h
#define ZMeshManager_h

#include <string>
#include <set>
#include "ZMesh.h"
#include "ZSingleton.h"

class ZBaseEntity;
class ZMesh;

class ZMeshManager : public ZSingleton <ZMeshManager>
{
	friend class ZSingleton <ZMeshManager>;

public:
	void removeMesh(ZMesh* mesh);
	ZMesh* loadMesh(const std::string& filename, Vector3& _scale);
	ZMesh* loadSimpleMesh();

private:
	explicit ZMeshManager();
	virtual ~ZMeshManager();

	bool load3DS(ZMesh& mesh, const std::string& filename);

	std::set<ZMesh*> m_meshes;
};

#endif