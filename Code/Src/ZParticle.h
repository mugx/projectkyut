#ifndef ZParticle_h
#define ZParticle_h

#include "ZBaseEntity.h"

class ZParticleSystem;

class ZParticle  : public ZBaseEntity
{
public:
	typedef enum
	{
		NO_TYPE,
		EXPLOSION1
	} ParticleType;

	explicit ZParticle();
	virtual ~ZParticle();
	virtual void draw();
	virtual void update();

	ParticleType getType() { return m_type; }
	void setType(enum ParticleType _type) { m_type = _type; }
	void setParticleSystem(ZParticleSystem* _particleSystem) { m_particleSystem = _particleSystem; }
	float getLife() { return life; }
	void setLife(float _life) { life = _life; }
	float getFade() { return fade; }
	void setFade(float _fade) { fade = _fade; }

private:
	ParticleType m_type;
	ZParticleSystem* m_particleSystem;
	float life;
	float fade;
};

#endif