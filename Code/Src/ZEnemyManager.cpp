#include "ZEnemyManager.h"

#include "ZConst.h"
#include "ZCollisionProxy.h"
#include "ZAsteroid.h"

ZEnemyManager::ZEnemyManager()
: m_level(0)
{

}

ZEnemyManager::~ZEnemyManager()
{
	resetEnemies();	
}

void ZEnemyManager::resetEnemies()
{
	m_level = 0;

	for (std::vector<ZEnemy*>::iterator it = m_enemies.begin(), end = m_enemies.end(); it != end;)
	{
		delete (*it++);
	}
	m_enemies.clear();

	for (std::map<int, ZMesh*>::iterator it = m_meshesForEnemies.begin(), end = m_meshesForEnemies.end(); it != end;)
	{
		ZMeshManager::getInstance()->removeMesh(it++->second);
	}
	m_meshesForEnemies.clear();

	for (std::map<int, ZTexture*>::iterator it = m_texturesForEnemies.begin(), end = m_texturesForEnemies.end(); it != end;)
	{
		ZTextureManager::getInstance()->removeTexture(it++->second);
	}
	m_texturesForEnemies.clear();
}

ZMesh* ZEnemyManager::getMesh(int _id)
{
	return m_meshesForEnemies.find(_id)->second;	
}

ZTexture* ZEnemyManager::getTexture(int _id)
{
	return m_texturesForEnemies.find(_id)->second;
}

void ZEnemyManager::addEnemyMesh(int _id, QString _resourceName, Vector3& _scale)
{
	QString fullResourceName = ZConst::PATH_ENEMY_MODELS + _resourceName;
	ZMesh* mesh = ZMeshManager::getInstance()->loadMesh(fullResourceName.toStdString(), _scale);
	m_meshesForEnemies[_id] = mesh;
}

void ZEnemyManager::addEnemyTexture(int _id, QString _resourceName)
{
	QString fullResourceName = ZConst::PATH_ENEMY_TEXTURES + _resourceName;
	ZTexture* texture = ZTextureManager::getInstance()->loadTexture(fullResourceName);
	m_texturesForEnemies[_id] = texture;
}

void ZEnemyManager::addEnemy(int _id, ZLevel* _level, Vector3& _position)
{
	ZEnemy* enemy = 0;
	switch (_id)
	{
	case ASTEROID_TYPE:
		{
			enemy = new ZAsteroid(_id, _level, _position);
		}
		break;
	}

	if (enemy)
	{
		m_enemies.push_back(enemy);
	}
}

ZEnemy* ZEnemyManager::getEnemy(int _id)
{
	for (int i = 0;i < m_enemies.size();++i)
	{
		if (m_enemies[i]->getId() == _id)
		{
			return m_enemies[i];
		}
	}

	return 0;
}

void ZEnemyManager::update()
{
	m_visibleEnemies.clear();
	
	for (std::vector<ZEnemy*>::iterator it = m_enemies.begin(), end = m_enemies.end();it != end;++it)
	{
		ZEnemy* currentEnemy = *it;
		if (currentEnemy->getIsAlive())
		{
			FrustumIntersection culling = (m_level->getCollisionProxy()->sphereInFrustum(currentEnemy->getPosition(), 10));
			if (!culling[ZCollisionProxy::EXTERN_TO_FRUSTUM])
			{
				currentEnemy->update();
				m_visibleEnemies.push_back(currentEnemy);
			}
		}
	}
}

void ZEnemyManager::draw()
{
	for_each(m_visibleEnemies.begin(), m_visibleEnemies.end(), std::mem_fun(&ZEnemy::draw));
}