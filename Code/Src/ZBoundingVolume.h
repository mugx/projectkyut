#ifndef ZBoundingVolume_h
#define ZBoundingVolume_h

#include <GL\glew.h>
#include "ZMath.h"

class ZBoundingVolume
{
public:
	explicit ZBoundingVolume();
	explicit ZBoundingVolume(const Vector3& _position, const Vector3& _velocity, const Vector3& _scale);
	virtual ~ZBoundingVolume();

	bool isCollision(const ZBoundingVolume& otherBV) const;
	void update();
	void draw();

	const Vector3& m_position;
	const Vector3& m_velocity;
	const Vector3& m_scale;
	Vector3 m_min;
	Vector3 m_max;
	Vector3 m_center;
	float m_radius;
	GLUquadricObj* m_quadratic;

private:		

};

#endif