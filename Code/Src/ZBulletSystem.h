#ifndef ZBulletSystem_h
#define ZBulletSystem_h

#include "ZBullet.h"

class ZBullet;

class ZBulletSystem
{
public:
	virtual void initBullet(ZBullet* _bullet) = 0;
	virtual void update(ZBullet* _bullet) {}
	virtual void draw(ZBullet* _bullet) {}

protected:
	ZBulletSystem();
	virtual ~ZBulletSystem();
};

#endif