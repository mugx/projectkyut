#include "ZSplashScreen.h"

#include <sstream>
#include "ZSoundManager.h"
#include "ZCore.h"
#include "ZTextureManager.h"
#include "ZScreenManager.h"
#include "ZCamera.h"

GLfloat ZSplashScreen::s_verticesArray_front[] = { 500.0f, -500.0f, -500.0f, -500.0f, -500.0f, -500.0f, -500.0f,  500.0f, -500.0f, 500.0f,  500.0f, -500.0f };;
GLfloat ZSplashScreen::s_texCoordsArray_front[] = { 0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f };	

ZSplashScreen::ZSplashScreen()
: ZScreen(SPLASH_SCREEN)
{
	std::stringstream ss;
	ss << ":/textures/screen/splashScreen.jpg";	
	m_splashScreenTexture = ZTextureManager::getInstance()->loadTexture(ss.str().c_str(), true);
	m_splashScreenTexture->m_texCoordsArray = s_texCoordsArray_front;

	m_splashScreenHud = new ZSplashScreenHud();	
	m_splashScreenHud->refreshHud();
}

ZSplashScreen::~ZSplashScreen()
{
	z_delete_0(m_splashScreenHud);
	ZTextureManager::getInstance()->removeTexture(m_splashScreenTexture);
}

void ZSplashScreen::setKeyboardAction(QKeyEvent* _keyboardEvent, bool _isPressed)
{
	if (!_isPressed)
	{
		if (_keyboardEvent->key() == Qt::Key_Return || _keyboardEvent->key() == Qt::Key_Space)
		{
			ZSoundManager::getInstance()->play("menu_select_item");
			ZScreenManager::getInstance()->setCurrentScreen(ZScreen::GAME_SCREEN);
		}
	}
}

void ZSplashScreen::refreshResolution()
{
	m_wantRefreshResolution = true;
}

void ZSplashScreen::update()
{
	m_splashScreenHud->update();
}

void ZSplashScreen::draw()
{
	ZCamera::getInstance()->beginLookAt();	

	glEnableClientState(GL_VERTEX_ARRAY);	
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	//	glEnableClientState(GL_NORMAL_ARRAY);

	glDepthMask(FALSE);
	glDisable(GL_CULL_FACE);

	glPushMatrix();
	{
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); //no alpha blending
		glVertexPointer(3, GL_FLOAT, 0, s_verticesArray_front);
		glTexCoordPointer(2, GL_FLOAT, 0, s_texCoordsArray_front);
		glColor4f(1.0f, 1.0f, 1.0f, 1.0f);

		glBindTexture(GL_TEXTURE_2D, m_splashScreenTexture->m_idTexture);
		glDrawArrays(GL_QUADS, 0, 4);
		glBindTexture(GL_TEXTURE_2D, 0);
	}
	glPopMatrix();

	glEnable(GL_CULL_FACE);
	glDepthMask(TRUE);	

	//glDisableClientState(GL_NORMAL_ARRAY);	
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	glDisableClientState(GL_VERTEX_ARRAY);

	//---------------- draw HUD --------------------------//
	if (!m_wantRefreshResolution)
	{
		m_splashScreenHud->draw();
	}
	else
	{
		m_splashScreenHud->refreshHud();
		m_wantRefreshResolution = false;
	}
}