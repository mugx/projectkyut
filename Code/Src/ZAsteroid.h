#ifndef ZAsteroid_h
#define ZAsteroid_h

#include "ZEnemy.h"

class ZLevel;

class ZAsteroid : public ZEnemy
{
public:
	explicit ZAsteroid(int _id, ZLevel* _level, Vector3& _position);
	virtual ~ZAsteroid();
	virtual void restart(ZLevel* _level, Vector3& _position);
	virtual void update();
};

#endif
