#include "ZParticle.h"

#include "ZConfig.h"
#include "ZParticleSystem.h"

ZParticle::ZParticle()
: m_type(NO_TYPE)
{
	setIsVisible(false);
}

ZParticle::~ZParticle()
{
}

void ZParticle::update()
{
	if (!getIsVisible()) return;

	m_particleSystem->update(this);
}

void ZParticle::draw()
{
	if (!getIsVisible()) return;

	m_particleSystem->draw(this);
}