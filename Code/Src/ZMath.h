#ifndef ZMath_h
#define ZMath_h

#include <stdlib.h>
#include <math.h>

#define PI 3.141593f
#define round(m_x) (m_x<0?ceil((m_x)-0.5):floor((m_x)+0.5))
#define MINIMUM_FLOAT 0.00001f

template <typename T> class Vec3
{
public:
	T m_x;
	T m_y;
	T m_z;
};

class Vector3
{
public:
	float m_x;
	float m_y;
	float m_z;

	static const Vector3 s_zero;

	Vector3() : m_x(0.0f), m_y(0.0f), m_z(0.0f) {}
	Vector3(float _input) : m_x(_input), m_y(_input), m_z(_input) {}
	Vector3(float _x, float _y, float _z) : m_x(_x), m_y(_y), m_z(_z) {}	

	char* toString();

	Vector3 operator/(const float A) 
	{
		return Vector3(m_x / A, m_y / A, m_z / A);
	}

	Vector3& operator/=(const float A) 
	{
		m_x /= A;
		m_y /= A;
		m_z /= A;
		return *this;
	}

	Vector3 operator*(const float A) 
	{
		return Vector3(m_x * A, m_y * A, m_z * A);
	}

	Vector3& operator*=(const float A) 
	{
		m_x *= A;
		m_y *= A;
		m_z *= A;
		return *this;
	}

	Vector3& operator*=(const Vector3& A) 
	{
		m_x *= A.m_x;
		m_y *= A.m_y;
		m_z *= A.m_z;
		return *this;
	}

	Vector3 operator+(const Vector3& A) 
	{
		return Vector3(m_x + A.m_x, m_y + A.m_y, m_z + A.m_z);
	}

	Vector3& operator+=(const Vector3& A) 
	{
		m_x += A.m_x;
		m_y += A.m_y;
		m_z += A.m_z;
		return *this;
	}

	Vector3 operator-(const Vector3& A) 
	{		
		return Vector3(m_x - A.m_x, m_y - A.m_y, m_z - A.m_z);
	}

	Vector3& operator-=(const Vector3& A) 
	{
		m_x -= A.m_x;
		m_y -= A.m_y;
		m_z -= A.m_z;
		return *this;
	}

	Vector3& operator=(const Vector3& A) {		
		m_x = A.m_x;
		m_y = A.m_y;
		m_z = A.m_z;
		return *this;
	}

	bool operator<(const Vector3& A) 
	{
		return (m_x < A.m_x) && (m_y < A.m_y) && (m_z < A.m_z);
	}

	bool operator>(const Vector3& A) 
	{
		return (m_x > A.m_x) && (m_y > A.m_y) && (m_z > A.m_z);
	}

	Vector3 operator~()
	{
		return Vector3(-m_x, -m_y, -m_z);
	}

	Vector3 operator^(const Vector3& A)
	{
		Vector3 result;
		result.m_x = m_y * A.m_z - m_z * A.m_y;
		result.m_y = -m_x * A.m_z + m_z * A.m_x;
		result.m_z = m_x * A.m_y + m_y * A.m_x;
		return result;
	}

	float operator*(const Vector3& A)
	{
		return m_x * A.m_x + m_y * A.m_y + m_z * A.m_z;
	}
};


class Vector4
{
public:
	float m_x;
	float m_y;
	float m_z;
	float m_a;

	Vector4() : m_x(0.0f), m_y(0.0f), m_z(0.0f), m_a(0.0f) {}
	Vector4(float input) : m_x(input), m_y(input), m_z(input), m_a(input) {}
	Vector4(float _x, float _y, float _z, float _a) : m_x(_x), m_y(_y), m_z(_z), m_a(_a) {}
};

class Vector2
{
public:
	float m_x;
	float m_y;

	Vector2::Vector2(float _x = 0, float _y = 0) : m_x(_x), m_y(_y) { }
	void set(float _x, float _y);
	char* toString();
};

class ZMath
{
public:
	static int mod(int _m, int _n) 
	{
		return _m >= 0 ? _m % _n : ( _n - abs (_m % _n) ) % _n; 
	}

	static float rand_FloatRange(const float& _a, const float& _b)
	{
		return ((_b - _a) * (static_cast<float>(rand())/RAND_MAX)) + _a;
	}

	//-------------- Operazioni sui vettori --------------//
	static float vectLength (const Vector3& _vector)
	{
		return sqrtf(_vector.m_x * _vector.m_x + _vector.m_y * _vector.m_y + _vector.m_z * _vector.m_z);
	}

	static float distanceWithoutRoot(const Vector3& _vector1, const Vector3& _vector2)
	{
		return ((_vector2.m_x - _vector1.m_x) * (_vector2.m_x - _vector1.m_x)) +
			((_vector2.m_y - _vector1.m_y) * (_vector2.m_y - _vector1.m_y)) +
			((_vector2.m_z - _vector1.m_z) * (_vector2.m_z - _vector1.m_z));
	}

	static float distance(const Vector3& _vector1, const Vector3& _vector2)
	{
		static Vector3 temp;
		temp.m_x = _vector2.m_x - _vector1.m_x;
		temp.m_y = _vector2.m_y - _vector1.m_y;
		temp.m_z = _vector2.m_z - _vector1.m_z;

		return sqrtf(temp.m_x * temp.m_x + temp.m_y * temp.m_y + temp.m_z * temp.m_z);
	}

	static void vectNormalize(Vector3& _vector)
	{
		float length;
		length = vectLength(_vector);
		if (length == 0) length = 1;
		_vector /= length;
	}

	static void vectCreate(const Vector3& _start, const Vector3& _end, Vector3& _vector)
	{
		_vector.m_x = _end.m_x - _start.m_x;
		_vector.m_y = _end.m_y - _start.m_y;
		_vector.m_z = _end.m_z - _start.m_z;
		vectNormalize(_vector);
	}

	static float dotProduct(const Vector3& _vector1, const Vector3& _vector2)
	{
		return (_vector1.m_x * _vector2.m_x + _vector1.m_y * _vector2.m_y + _vector1.m_z * _vector2.m_z);
	}

	static void crossProduct(const Vector3& _vector1, const Vector3& _vector2, Vector3& _normal)
	{
		_normal.m_x = (_vector1.m_y * _vector2.m_z) - (_vector1.m_z * _vector2.m_y);
		_normal.m_y = -(_vector1.m_x * _vector2.m_z) + (_vector1.m_z * _vector2.m_x);
		_normal.m_z = (_vector1.m_x * _vector2.m_y) - (_vector1.m_y * _vector2.m_x);
	}

	static int sign(float _num)
	{
		return (_num > 0) - (_num < 0);
	}

	static void vectTruncate(Vector3 _vector, float _max)
	{
		if (vectLength(_vector) > _max)
		{
			vectNormalize(_vector);
			_vector *= _max;
		}
	}
};

#endif