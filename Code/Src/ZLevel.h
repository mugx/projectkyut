#ifndef ZLevel_h
#define ZLevel_h

#include "ZLevelParser.h"
#include <list>
#include <vector>

class ZParticleProxy;
class ZCollisionProxy;
class ZBulletProxy;
class ZSkybox;
class ZBackground;
class ZScreenManager;
class ZTexture;

class ZLevel
{
	friend class ZScreenManager;
	friend class ZLevelParser;

public:
	explicit ZLevel(int _id);
	virtual ~ZLevel();

	void update();
	void draw();
	ZParticleProxy* getParticleProxy() const { return m_particleProxy; }
	ZCollisionProxy* getCollisionProxy() const { return m_collisionProxy; }
	ZBulletProxy* getBulletProxy() const { return m_bulletProxy; }	
	int getId() { return m_id; }
	void SetId(int _id) { m_id = _id; }

private:
	int m_id;
	std::string m_name;
	ZBulletProxy* m_bulletProxy;
	ZCollisionProxy* m_collisionProxy;
	ZParticleProxy* m_particleProxy;
};

#endif