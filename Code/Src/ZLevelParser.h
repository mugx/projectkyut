#ifndef ZLevelParser_h
#define ZLevelParser_h

#include <QtXml>
#include <string>

class ZLevel;

class ZLevelParser : public QXmlDefaultHandler
{	
public:		
	ZLevelParser(ZLevel* const _level);

private:	
	bool characters(const QString& ch);
	bool startDocument();
	bool startElement(const QString&, const QString&, const QString&, const QXmlAttributes&);
	bool endElement(const QString&, const QString&, const QString&);
	void printCurrentAttributes();

	ZLevel* const m_level;
	QString m_currentTag;
	QXmlAttributes m_currentAttributes;
	bool m_canContinue;
};

#endif