#ifndef ZEnemy_h
#define ZEnemy_h

#include "ZAiEntity.h"

class ZEnemyManager;
class ZExplosionSystem;

class ZEnemy : public ZAiEntity
{
	friend class ZEnemyManager;

public:	
	virtual void update();
	virtual void draw();
	virtual void shoot();
	virtual void die();

protected:
	explicit ZEnemy();
	virtual ~ZEnemy();

private:
	ZExplosionSystem* m_explosionSystem;
};

#endif