#ifndef ZCamera_h
#define ZCamera_h

#include <gl/glew.h>
#include "ZSingleton.h"
#include "ZMath.h"
#include "ZCamera.h"

class ZCamera : public ZSingleton<ZCamera>
{
	friend class ZSingleton <ZCamera>;
public:
	explicit ZCamera();
	virtual ~ZCamera();

	void restartCamera();
	void update();
	void init3D();
	void extractFrustum();
	void beginLookAt();
	void endLookAt();

	float deltaEyeY;
	Vector3 velocity;
	Vector3 eye;
	Vector3 eyeView;
	Vector3 center;
	Vector3 centerView;
	Vector3 up;
	double frustum[6][4];

private:
	GLdouble projection[16];
	GLdouble model[16];
	GLint viewport[4];
};
#endif