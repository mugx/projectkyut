#include "ProjectKyut.h"

#include <QApplication>
#include "ZSoundManager.h"
#include "ZConfig.h"
#include "ZMath.h"
#include "ZTextureManager.h"
#include "ZShaderManager.h"
#include "ZCamera.h"
#include "ZPlayerManager.h"
#include "ZEnemyManager.h"
#include "ZScenarioManager.h"
#include "ZScreenManager.h"

ProjectKyut::ProjectKyut(int argc, char *argv[]) 
: QApplication(argc, argv)
{

}

ProjectKyut::~ProjectKyut()
{
	ZConfig::s_isPause = true;
	ZScreenManager::s_destroy();
	ZEnemyManager::s_destroy();
	ZPlayerManager::s_destroy();
	ZScenarioManager::s_destroy();
	ZCamera::s_destroy();
	ZMeshManager::s_destroy();	
	ZShaderManager::s_destroy();
	ZTextureManager::s_destroy();
	ZSoundManager::s_destroy();	
	Q_CLEANUP_RESOURCE(resources);
}

int ProjectKyut::exec()
{
	Q_INIT_RESOURCE(resources);
	if (!QGLFormat::hasOpenGL()) { return false; }
	ZScreenManager::getInstance()->init();
	return QApplication::exec();
}

int main(int argc, char *argv[])
{
	ProjectKyut projectKyut(argc, argv);
	return projectKyut.exec();
}
