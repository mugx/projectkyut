#include "ZBulletProxy.h"
#include "ZCollisionProxy.h"
#include "ZLevel.h"
#include "ZBullet.h"
#include "ZBulletSystem.h"
#include <gl/glew.h>

#define NUM_BULLETS_AVAILABLE 50

ZBulletProxy::ZBulletProxy(ZLevel& _level)
: m_level(_level)
{
	for (int i = 0;i < NUM_BULLETS_AVAILABLE;++i)
	{
		m_bullets.push_back(new ZBullet());
	}
}

ZBulletProxy::~ZBulletProxy()
{
	for (std::vector<ZBullet*>::iterator it = m_bullets.begin(), end = m_bullets.end();it != end;++it)
	{
		delete *it;
	}
	m_bullets.clear();
	m_visibleBullets.clear();
}

void ZBulletProxy::doEmit(ZBulletSystem* _bulletSystem)
{
	for (std::vector<ZBullet*>::iterator it = m_bullets.begin(), end = m_bullets.end();it != end;++it)
	{
		ZBullet* currentBullet = *it;
		if (!currentBullet->getIsVisible())
		{
			_bulletSystem->initBullet(currentBullet);
			break;
		}
	}
}

void ZBulletProxy::update()
{
	m_visibleBullets.clear();

	for (std::vector<ZBullet*>::iterator it = m_bullets.begin(), end = m_bullets.end();it != end;++it)
	{
		ZBullet* currentBullet = *it;
		if (currentBullet->getIsVisible())
		{
			FrustumIntersection culling = (m_level.getCollisionProxy()->sphereInFrustum(currentBullet->getPosition(), 10));
			if (culling[ZCollisionProxy::EXTERN_TO_FRUSTUM])
			{
				currentBullet->setIsVisible(false);
			}
			else
			{
				currentBullet->update();
				m_visibleBullets.push_back(currentBullet);
			}
		}
	}
}

void ZBulletProxy::draw()
{
	glEnable(GL_BLEND);
	glBlendFunc(GL_ONE, GL_ONE);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);

	std::for_each(m_visibleBullets.begin(), m_visibleBullets.end(), std::mem_fun(&ZBullet::draw));
	
	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
	glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
}