#ifndef ZGameScreenHud_h
#define ZGameScreenHud_h

#include "ZHud.h"

class ZGameScreen;

class ZGameScreenHud : public ZHud
{
public:
	explicit ZGameScreenHud(ZGameScreen* _gameScreen);
	virtual ~ZGameScreenHud();
	virtual void refreshHud();
	virtual void destroyHud();
	virtual void update();
	virtual void draw();

private:
	ZGameScreen* m_gameScreen;
	
	ZTextComponent* m_selectedComponent;
	ZTextComponent* m_gameOverTextComponent;
	ZTextComponent* m_pauseTextComponent;
	ZTextComponent* m_levelTextComponent;
	ZTextComponent* m_levelNumberTextComponent;
	ZTextComponent* m_scoreTextComponent;
	ZTextComponent* m_scoreNumberTextComponent;
	ZTextComponent* m_livesTextComponent;
	ZTextComponent* m_livesNumberTextComponent;

	ZTextComponent* m_fpsTextComponent;
	ZTextComponent* m_currentDebugSelectionTextComponent;
	ZTextComponent* m_interpolationTextComponent;
	ZTextComponent* m_exposureTextComponent;
	ZTextComponent* m_factorTextComponent;
	ZTextComponent* m_gammaTextComponent;
	ZTextComponent* m_zoomTextComponent;
	ZTextComponent* m_blinkingTextComponent;
};

#endif