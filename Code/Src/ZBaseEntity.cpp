#include "ZBaseEntity.h"

#include "ZConfig.h"

ZBaseEntity::ZBaseEntity()
: m_position(0.0f, 0.0f, 0.0f),
m_viewInterpolation(ZConfig::s_interpolation),
m_isVisible(false),
m_mesh(0),
m_texture(0),
m_boundingVolume(0),
m_currentLevel(0)
{
}

ZBaseEntity::~ZBaseEntity()
{

}

void ZBaseEntity::makeBoundingVolume()
{
	m_boundingVolume = new ZBoundingVolume(getPosition(), getVelocity(), getScale());

	int verticesCardinality = m_mesh->m_verticesCardinality;
	int polygonsCardinality = m_mesh->m_polygonsCardinality;
	Vector3* vertices = m_mesh->m_vertices;
	Vec3<short>* triangles = m_mesh->m_triangles;

	for (int i = 0;i < polygonsCardinality;++i)
	{     
		Vector3& firstVertex = vertices[triangles[i].m_x];
		Vector3& secondVertex = vertices[triangles[i].m_y];
		Vector3& thirdVertex = vertices[triangles[i].m_z];

		if (firstVertex.m_x <= m_boundingVolume->m_min.m_x) m_boundingVolume->m_min.m_x = firstVertex.m_x;
		if (firstVertex.m_y <= m_boundingVolume->m_min.m_y) m_boundingVolume->m_min.m_y = firstVertex.m_y;
		if (firstVertex.m_z <= m_boundingVolume->m_min.m_z) m_boundingVolume->m_min.m_z = firstVertex.m_z;

		if (firstVertex.m_x >= m_boundingVolume->m_max.m_x) m_boundingVolume->m_max.m_x = firstVertex.m_x;
		if (firstVertex.m_y >= m_boundingVolume->m_max.m_y) m_boundingVolume->m_max.m_y = firstVertex.m_y;
		if (firstVertex.m_z >= m_boundingVolume->m_max.m_z) m_boundingVolume->m_max.m_z = firstVertex.m_z;

		if (secondVertex.m_x <= m_boundingVolume->m_min.m_x) m_boundingVolume->m_min.m_x = secondVertex.m_x;
		if (secondVertex.m_y <= m_boundingVolume->m_min.m_y) m_boundingVolume->m_min.m_y = secondVertex.m_y;
		if (secondVertex.m_z <= m_boundingVolume->m_min.m_z) m_boundingVolume->m_min.m_z = secondVertex.m_z;

		if (secondVertex.m_x >= m_boundingVolume->m_max.m_x) m_boundingVolume->m_max.m_x = secondVertex.m_x;
		if (secondVertex.m_y >= m_boundingVolume->m_max.m_y) m_boundingVolume->m_max.m_y = secondVertex.m_y;
		if (secondVertex.m_z >= m_boundingVolume->m_max.m_z) m_boundingVolume->m_max.m_z = secondVertex.m_z;

		if (thirdVertex.m_x <= m_boundingVolume->m_min.m_x) m_boundingVolume->m_min.m_x = thirdVertex.m_x;
		if (thirdVertex.m_y <= m_boundingVolume->m_min.m_y) m_boundingVolume->m_min.m_y = thirdVertex.m_y;
		if (thirdVertex.m_z <= m_boundingVolume->m_min.m_z) m_boundingVolume->m_min.m_z = thirdVertex.m_z;

		if (thirdVertex.m_x >= m_boundingVolume->m_max.m_x) m_boundingVolume->m_max.m_x = thirdVertex.m_x;
		if (thirdVertex.m_y >= m_boundingVolume->m_max.m_y) m_boundingVolume->m_max.m_y = thirdVertex.m_y;
		if (thirdVertex.m_z >= m_boundingVolume->m_max.m_z) m_boundingVolume->m_max.m_z = thirdVertex.m_z;
	}
	m_boundingVolume->m_center = (m_boundingVolume->m_min + m_boundingVolume->m_max) / 2.0f;	

	float max_distance_without_root = 0;
	float current_distance_without_root = 0;
	for (int i = 0;i < verticesCardinality;++i)
	{
		current_distance_without_root = ZMath::distanceWithoutRoot(m_boundingVolume->m_center, vertices[i]);
		if (current_distance_without_root > max_distance_without_root)
		{
			max_distance_without_root = current_distance_without_root;
		}
	}
	m_boundingVolume->m_radius = sqrtf(max_distance_without_root);
}

ZBoundingVolume* ZBaseEntity::getBoundingVolume()
{
	return m_boundingVolume; 
}