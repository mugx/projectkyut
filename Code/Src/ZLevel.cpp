#include "ZLevel.h"

#include <sstream>
#include <iostream>
#include "ZConfig.h"
#include "ZCore.h"
#include "ZLevelParser.h"
#include "ZShaderManager.h"
#include "ZBulletProxy.h"
#include "ZParticleProxy.h"
#include "ZCollisionProxy.h"
#include "ZResourcesParser.h"
#include "ZBackground.h"
#include "ZCamera.h"
#include "ZScenarioManager.h"
#include "ZPlayerManager.h"
#include "ZEnemyManager.h"
#include "ZScreenManager.h"

ZLevel::ZLevel(int _id)
: m_id(_id),
m_bulletProxy(0),
m_particleProxy(0),
m_collisionProxy(0)
{
	ZResourcesParser m_resourcesParser(this);
	ZLevelParser m_levelParser(this);

	m_bulletProxy = new ZBulletProxy(*this);
	m_particleProxy = new ZParticleProxy(*this);
	m_collisionProxy = new ZCollisionProxy(*this);

	ZPlayerManager::getInstance()->setCurrentLevel(this);
	ZEnemyManager::getInstance()->setCurrentLevel(this);

	// restart player
	std::vector<ZPlayer*> players = ZPlayerManager::getInstance()->getPlayers();
	players[0]->setNumLives(3);
	ZConfig::s_isGameOver = false;

	// restart camera
	ZCamera::getInstance()->restartCamera();
}

ZLevel::~ZLevel()
{
	//--- delete singletons ---//
	ZCamera::s_destroy();
	ZPlayerManager::s_destroy();
	ZEnemyManager::s_destroy();

	//--- delete proxies ---//
	z_delete_0(m_bulletProxy);
	z_delete_0(m_particleProxy);	
	z_delete_0(m_collisionProxy);
}

void ZLevel::update()
{
	ZPlayerManager::getInstance()->update();
	ZEnemyManager::getInstance()->update();
	ZScenarioManager::getInstance()->update();
	ZCamera::getInstance()->update();

	m_bulletProxy->update();
	m_particleProxy->update();
	m_collisionProxy->update();
}

//--- RENDERING PIPELINE ---//
void ZLevel::draw()
{
	//1) 3D Scene + Phong Shading
	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, ZScreenManager::getInstance()->m_fboScene);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
	
	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glEnableClientState(GL_NORMAL_ARRAY);

	glStencilFunc(GL_ALWAYS, 1, 0x1);
	glStencilOp (GL_REPLACE, GL_REPLACE, GL_REPLACE);
	ZCamera::getInstance()->beginLookAt();
	ZShaderManager::getInstance()->shaderPHONG.bind();
	ZScenarioManager::getInstance()->drawBackgrounds();
	ZPlayerManager::getInstance()->draw();
	ZEnemyManager::getInstance()->draw();
	ZShaderManager::getInstance()->shaderPHONG.unbind();

	// 2D
	glDepthMask(FALSE);
	glStencilFunc(GL_EQUAL, 0, 0x1);
	glStencilOp (GL_KEEP, GL_KEEP, GL_REPLACE);
	glDisable(GL_CULL_FACE);
	ZScenarioManager::getInstance()->drawSkybox();
	glStencilFunc(GL_ALWAYS, 1, 0x1);

	glStencilOp (GL_REPLACE, GL_REPLACE, GL_REPLACE);
	glBlendFunc(GL_ONE, GL_ONE);
	ZShaderManager::getInstance()->shaderPARTICLES.bind();
	m_bulletProxy->draw();
	m_particleProxy->draw();
	ZShaderManager::getInstance()->shaderPARTICLES.unbind();

	glEnable(GL_CULL_FACE);
	glDepthMask(TRUE);

	glDisableClientState(GL_NORMAL_ARRAY);	
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	glDisableClientState(GL_VERTEX_ARRAY);

	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);

	// TEXTURE Scene CON STENCIL TEST
	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, ZScreenManager::getInstance()->m_fboSceneStencil);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
	glBindFramebufferEXT(GL_READ_FRAMEBUFFER_EXT, ZScreenManager::getInstance()->m_fboScene);
	glBindFramebufferEXT(GL_DRAW_FRAMEBUFFER_EXT, ZScreenManager::getInstance()->m_fboSceneStencil);
	glBlitFramebufferEXT(0, 0, ZConfig::s_size.width(), ZConfig::s_size.height(), 0, 0, ZConfig::s_size.width(), ZConfig::s_size.height(), GL_STENCIL_BUFFER_BIT, GL_NEAREST);
	glStencilFunc(GL_EQUAL, 0x1, 0x1);
	glStencilOp (GL_KEEP, GL_KEEP, GL_KEEP);
	ZScreenManager::getInstance()->drawBackground(ZScreenManager::getInstance()->m_textureScene);
	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);

	// HDR1: Texture Highpass
	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, ZScreenManager::getInstance()->m_fboHighpass);
	glClear(GL_COLOR_BUFFER_BIT);
	glViewport(0, 0, ZConfig::s_size.width() / 2, ZConfig::s_size.height() / 2);
	ZShaderManager::getInstance()->shaderHIGHPASS.bind();
	ZScreenManager::getInstance()->drawBackground(ZScreenManager::getInstance()->m_textureSceneStencil);
	ZShaderManager::getInstance()->shaderHIGHPASS.unbind();
	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);	

	// HDR2: Texture Gauss-Horizontal
	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, ZScreenManager::getInstance()->m_fboGaussH);
	glClear(GL_COLOR_BUFFER_BIT);
	ZShaderManager::getInstance()->shaderGAUSSH.bind();
	ZScreenManager::getInstance()->drawBackground(ZScreenManager::getInstance()->m_textureHighpass);
	ZShaderManager::getInstance()->shaderGAUSSH.unbind();
	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);	

	// HDR3: Texture Gauss-Vertical
	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, ZScreenManager::getInstance()->m_fboGaussV);
	glClear(GL_COLOR_BUFFER_BIT);
	ZShaderManager::getInstance()->shaderGAUSSV.bind();
	ZScreenManager::getInstance()->drawBackground(ZScreenManager::getInstance()->m_textureGaussH);
	ZShaderManager::getInstance()->shaderGAUSSV.unbind();
	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);	
	
	// HDR: TONE MAPPING
	glStencilFunc(GL_GEQUAL, 0x0, 0x1);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, ZScreenManager::getInstance()->m_textureScene->m_idTexture);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, ZScreenManager::getInstance()->m_textureGaussV->m_idTexture);

#ifdef DEBUG
	ZShaderManager::getInstance()->shaderTONEMAP.uniform("exposureFactor", ZConfig::s_exposure);
	ZShaderManager::getInstance()->shaderTONEMAP.uniform("bloomFactor", ZConfig::s_factor);
	ZShaderManager::getInstance()->shaderTONEMAP.uniform("gammaFactor", ZConfig::s_gamma);
#endif

	glViewport(0, 0, ZConfig::s_size.width(), ZConfig::s_size.height());
	ZShaderManager::getInstance()->shaderTONEMAP.bind();
	ZScreenManager::getInstance()->drawBackground();
	ZShaderManager::getInstance()->shaderTONEMAP.unbind();
	glActiveTexture(GL_TEXTURE0);
}