#ifndef ZPlayerManager_h
#define ZPlayerManager_h

#include "ZSingleton.h"
#include "ZPlayer.h"
#include "ZLevel.h"
#include "ZMath.h"
#include <algorithm>
#include <list>
#include <vector>
#include <QKeyEvent>

class ZPlayerManager : public ZSingleton <ZPlayerManager>
{
	friend class ZSingleton <ZPlayerManager>;

public:
	enum PlayerType
	{
		PLAYER_WITH_KEYBOARD_TYPE,
		PLAYER_WITH_PAD_TYPE
	};

	void resetPlayers();
	ZMesh* getMesh(int _id);
	ZTexture* getTexture(int _id);
	void addPlayerMesh(int _id, QString _resourceName, Vector3& _scale);	
	void addPlayerTexture(int _id, QString _resourceName);
	void addPlayer(int _id, ZLevel* _level, Vector3& _position);
	ZPlayer* getPlayer(int _id);
	ZPlayer* const getNearestPlayer(Vector3& distance);
	bool arePlayersAlive();
	void setKeyboardAction(QKeyEvent* nKeyEvent, bool nIsPressed);
	std::vector<ZPlayer*> getPlayers() { return m_players; }
	void setCurrentLevel(ZLevel* _level) { m_level = _level; }
	void update();
	void draw();

private:
	explicit ZPlayerManager();
	virtual ~ZPlayerManager();

	ZLevel* m_level;
	ZPlayer* playerWithKeyboard;
	ZPlayer* playerWithPad;
	std::vector<ZPlayer*> m_players;
	std::map<int, ZMesh*> m_meshesForPlayers;
	std::map<int, ZTexture*> m_texturesForPlayers;
};

#endif