#ifndef ZCore_h
#define ZCore_h

#include <QtCore>

#define z_delete_0(m_a) if (m_a != 0) { delete m_a; m_a = 0; }
#define z_delete(m_a) if (m_a != 0) { delete m_a; }

static const char* stringConcat(const char* _a, const char* _b)
{
	QString c = QString(_a) + QString(_b);
	return c.toStdString().c_str();
}

template<typename T>
static const char* stringConcat(T _a, T _b)
{
	QString c = QString(_a) + QString(_b);
	return c.toStdString().c_str();
}

#endif