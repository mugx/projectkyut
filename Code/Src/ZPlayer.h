#ifndef ZPlayer_h
#define ZPlayer_h

#include <map>
#include "ZTimer.h"
#include "ZAiEntity.h"
#include "ZLevel.h"
#include "ZLaserSystem.h"

class ZPlayer : public ZAiEntity
{
public:
	enum ActionType
	{
		ACTION_UP,
		ACTION_DOWN,
		ACTION_LEFT,
		ACTION_RIGHT,
		ACTION_FIRE,
	};

	explicit ZPlayer(int _id, ZLevel* _level, Vector3& _position);
	virtual ~ZPlayer();
	virtual void update();
	virtual void draw();
	virtual void die();
	virtual void takeHit();

	void setAction(ActionType nAction, bool nIsPressed); 
	void restart(ZLevel* _level, Vector3& _position);

	bool isInThirdPerson;

private:
	void updateStatus();

	std::map<ActionType, bool> actionMap;
	ZLaserSystem* m_laserSystem;
	ZTimer m_fireTimer;
	ZTimer m_invulnerabilityTimer;
};

#endif