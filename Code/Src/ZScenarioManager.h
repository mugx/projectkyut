#ifndef ZScenarioManager_h
#define ZScenarioManager_h

#include "ZSingleton.h"
#include "ZBackground.h"
#include "ZLevel.h"
#include "ZMath.h"
#include <algorithm>
#include <list>
#include <vector>
#include <QKeyEvent>

class ZScenarioManager : public ZSingleton <ZScenarioManager>
{
	friend class ZSingleton <ZScenarioManager>;

public:
	enum BackgroundType
	{
		ANY_TYPE
	};
	
	//background
	void addBackground(int _id, ZLevel* _level, Vector3& _position);
	ZBackground* getBackground(int _id);
	void addBackgroundTexture(int _id, QString _resourceName);
	ZTexture* getBackgroundTexture(int _id);
	ZMesh* getBackgroundMesh(int _id);
	void addBackgroundMesh(int _id, QString _resourceName, Vector3& _scale);
	//skybox
	void addSkybox(int _id, ZLevel* _level);
	ZSkybox* getSkybox();
	void addSkyboxTexture(int _id, QString resourceName);
	ZTexture* getSkyboxTexture(int _id);
	
	void update();
	void drawBackgrounds();
	void drawSkybox();
	void resetScenario();
	
private:
	explicit ZScenarioManager();
	virtual ~ZScenarioManager();
	
	//backgrounds
	std::vector<ZBackground*> m_backrounds;
	std::map<int, ZMesh*> m_meshesForBackgrounds;
	std::map<int, ZTexture*> m_texturesForBackgrounds;
	//skybox
	ZSkybox* m_skybox;
	std::map<int, ZTexture*> m_texturesForSkybox;
};

#endif