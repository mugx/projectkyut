#ifndef ZSoundManager_h
#define ZSoundManager_h

#include <QtCore>
#include <map>
#include <audiere.h>
#include "ZSingleton.h"

class ZSoundManager : public ZSingleton <ZSoundManager>
{
	friend class ZSingleton <ZSoundManager>;

public:
	void play(const char* _soundName);
	void AddSound(QString _fileName);
	void enableSound(bool _isMute);
	bool getIsSoundEnabled() { return m_isSoundEnabled; }

private:
	explicit ZSoundManager();
	virtual ~ZSoundManager();
	
	std::map<std::string, audiere::OutputStreamPtr> m_sounds;
	audiere::AudioDevicePtr m_device;
	bool m_isSoundEnabled;
};

#endif