#ifndef ZLaserSystem_h
#define ZLaserSystem_h

#include "ZBulletSystem.h"
#include "ZTextureManager.h"
#include "ZBaseEntity.h"
#include "ZBullet.h"


class ZLaserSystem : public ZBulletSystem
{
	friend class ZBullet;

public:
	ZLaserSystem(ZBaseEntity& _baseEntityParent);
	virtual ~ZLaserSystem();
	virtual void initBullet(ZBullet* _bullet);
	virtual void update(ZBullet* _bullet);
	virtual void draw(ZBullet* _bullet);
	
private:
	ZBaseEntity& m_baseEntityParent;
	ZTexture* m_textureLaser;
};

#endif