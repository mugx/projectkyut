#ifndef ZBackground_h
#define ZBackground_h

#include "ZMeshManager.h"
#include "ZBaseEntity.h"
#include "ZLevel.h"

class ZBackground : public ZBaseEntity
{
public:
	explicit ZBackground(int _id, ZLevel* _level, Vector3& _position);
	virtual ~ZBackground();
	virtual void update();
	virtual void draw();
};

#endif