#ifndef ZResourcesParser_h
#define ZResourcesParser_h

#include <QtXml>
#include <string>

class ZLevel;
class Vector3;

class ZResourcesParser : public QXmlDefaultHandler
{
public:
	ZResourcesParser(ZLevel* const _level);
	
private:
	bool characters(const QString& ch);
	bool startDocument();
	bool startElement(const QString&, const QString&, const QString&, const QXmlAttributes&);
	bool endElement(const QString&, const QString&, const QString&);

	ZLevel* const m_level;
	QString m_currentTag;
	QXmlAttributes m_currentAttributes;
	bool m_canContinue;
};

#endif