#ifndef ZGameplaySystem_h
#define ZGameplaySystem_h

#include "ZSingleton.h"
#include "ZLevel.h"

class ZGameplaySystem : public ZSingleton<ZGameplaySystem>
{
	friend class ZSingleton <ZGameplaySystem>;

public:
	void update();
	void draw();
	void init();
	void restartLevel();
	ZLevel* getCurrentLevel();

private:
	explicit ZGameplaySystem();
	virtual ~ZGameplaySystem();

	ZLevel* m_currentLevel;
};

#endif