#include "ZEnemy.h"

#include <gl/glew.h>
#include "ZConfig.h"
#include "ZMath.h"
#include "ZParticleProxy.h"
#include "ZLevel.h"
#include "ZExplosionSystem.h"
#include "ZCore.h"

ZEnemy::ZEnemy()
{
	m_explosionSystem = new ZExplosionSystem(*this); // setting particle system
}

ZEnemy::~ZEnemy()
{
	z_delete_0(m_explosionSystem);
}

void ZEnemy::update()
{
	if (!getIsVisible() || !getIsAlive()) return;
}

void ZEnemy::draw()
{
	if (!getIsVisible() || !getIsAlive()) return;

	glPushMatrix();
	{
		// calcolo lerp per il 'Constant Game Speed independent of Variable FPS
		m_viewPosition = m_position + (m_velocity * m_viewInterpolation);	
		glTranslatef(m_viewPosition.m_x, m_viewPosition.m_y, m_viewPosition.m_z);
		m_viewRotation = m_rotation + (m_angularVelocity * m_viewInterpolation);
		glRotatef(m_viewRotation.m_x, 1.0f, 0.0f, 0.0f);
		glRotatef(m_viewRotation.m_y, 0.0f, 1.0f, 0.0f);
		glRotatef(m_viewRotation.m_z, 0.0f, 0.0f, 1.0f);
		// fine calcolo lerp

		glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
		m_mesh->bindTexture(m_texture);
		m_mesh->draw();
	}
	glPopMatrix();

#ifdef DEBUG
	m_boundingVolume->draw();
#endif
}

void ZEnemy::shoot()
{
}

void ZEnemy::die()
{
	setStatus(STATUS_DEAD);
	setNumLives(0);

	// do big explosion
	m_currentLevel->getParticleProxy()->doEmit(m_explosionSystem);
}