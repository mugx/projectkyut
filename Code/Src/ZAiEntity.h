#ifndef ZAiEntity_h
#define ZAiEntity_h

#include "ZBaseEntity.h"

class ZAiEntity : public ZBaseEntity
{
public:
	typedef enum 
	{		
		STATUS_IDLE,
		STATUS_DEAD,	
		STATUS_ALIVE,
		STATUS_BEGIN_INVULNERABLE,
		STATUS_INVULNERABLE
	} AiStatus;

	virtual void die() = 0;
	virtual void takeHit();

	AiStatus getStatus();
	void setStatus(AiStatus _status);
	bool getIsAlive();
	short getNumLives();
	void setNumLives(short _numLives);
	
protected:
	explicit ZAiEntity();
	virtual ~ZAiEntity();	

private:
	short m_numLives;
	AiStatus m_currentStatus;
};

#endif 