#include "ZShaderManager.h"

ZShaderManager::ZShaderManager() 
{
	// Shaders settings
	shaderPHONG.init(":/shaders/phong/phong.vert", ":/shaders/phong/phong.frag");
	shaderPARTICLES.init(":/shaders/hdr/simple.vert", ":/shaders/hdr/particles.frag");
	shaderHIGHPASS.init(":/shaders/hdr/simple_multitexture.vert", ":/shaders/hdr/highpass.frag");
	shaderGAUSSH.init(":/shaders/hdr/simple_multitexture.vert", ":/shaders/hdr/bloomH.frag");
	shaderGAUSSV.init(":/shaders/hdr/simple_multitexture.vert", ":/shaders/hdr/bloomV.frag");
	shaderTONEMAP.init(":/shaders/hdr/simple_multitexture.vert", ":/shaders/hdr/tonemapping.frag");
}

ZShaderManager::~ZShaderManager() 
{

}