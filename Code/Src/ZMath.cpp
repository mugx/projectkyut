#include "ZMath.h"
#include <stdio.h>

const Vector3 Vector3::s_zero(0.0f, 0.0f, 0.0f);

char* Vector3::toString()
{
	static char s_vector3[50];
	sprintf_s(s_vector3, "%.2f %.2f %.2f", m_x, m_y, m_z);
	return s_vector3;
}

char* Vector2::toString()
{
	static char s_vector2[50];
	sprintf_s(s_vector2, "%.2f %.2f", m_x, m_y);
	return s_vector2;
}