#include "ZBullet.h"

#include "ZConfig.h"
#include "ZBulletSystem.h"
#include "ZBoundingVolume.h"

ZBullet::ZBullet()
: m_type(NO_TYPE)
{
	setIsVisible(false);
	m_mesh = ZMeshManager::getInstance()->loadSimpleMesh();
	makeBoundingVolume();
	//m_boundingVolume = new ZBoundingVolume(GetPosition(), GetVelocity(), GetScale());
	m_boundingVolume->m_min = Vector3(-0.05f, 0.0f, 0.0f);	
	m_boundingVolume->m_max = Vector3(0.05f, 0.7f, 0.0f);
	m_mesh->m_polygonsCardinality = 5;
}

ZBullet::~ZBullet()
{
}

void ZBullet::update()
{
	if (!getIsVisible()) return;

	m_bulletSystem->update(this);
}

void ZBullet::draw()
{
	if (!getIsVisible()) return;

	m_bulletSystem->draw(this);
#ifdef DEBUG
	getBoundingVolume()->draw();
#endif
}