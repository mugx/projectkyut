#include "ZBoundingVolume.h"

#include "ZConfig.h"

#define RESCALE 0.8f

ZBoundingVolume::ZBoundingVolume(const Vector3& _position, const Vector3& _velocity, const Vector3& _scale) 
: m_position(_position),
m_velocity(_velocity),
m_scale(_scale),
m_min(100000, 100000, 100000),
m_max(-100000, -100000, -100000),
m_quadratic(gluNewQuadric())
{}

ZBoundingVolume::~ZBoundingVolume() {}

bool ZBoundingVolume::isCollision(const ZBoundingVolume& _otherBB) const
{
	static Vector3 temp_min = 0;
	static Vector3 temp_max = 0;

	temp_min = m_min;
	temp_max = m_max;

	temp_min *= m_scale;
	temp_max *= m_scale;

	temp_min *= RESCALE;
	temp_max *= RESCALE;

	temp_min += m_position;
	temp_max += m_position;

	static Vector3 other_temp_min = 0;
	static Vector3 other_temp_max = 0;

	other_temp_min = _otherBB.m_min;
	other_temp_max = _otherBB.m_max;

	other_temp_min *= _otherBB.m_scale;
	other_temp_max *= _otherBB.m_scale;

	other_temp_min *= RESCALE;
	other_temp_max *= RESCALE;

	other_temp_min += _otherBB.m_position;
	other_temp_max += _otherBB.m_position;

	return temp_min < other_temp_max && temp_max > other_temp_min;
}

void ZBoundingVolume::update() {}

void ZBoundingVolume::draw()
{
	if (!ZConfig::s_isDrawBV) return;

	glPushMatrix();
	{
		glDisable(GL_TEXTURE_2D);	
		static Vector3 m_viewPosition;
		m_viewPosition = Vector3(m_position) + Vector3(m_velocity.m_x * ZConfig::s_interpolation, m_velocity.m_y * ZConfig::s_interpolation, m_velocity.m_z * ZConfig::s_interpolation);
		glTranslatef(m_viewPosition.m_x, m_viewPosition.m_y, m_viewPosition.m_z);
		// fine calcolo lerp	
		glColor4f(0.0f, 1.0f, 0.0f, 1.0f);
		glPolygonMode(GL_FRONT, GL_LINE);
		glPolygonMode(GL_BACK, GL_LINE);
		gluSphere(m_quadratic, 0.5, 16, 16);
		glPolygonMode(GL_FRONT, GL_FILL);
		glPolygonMode(GL_BACK, GL_FILL);

		glScalef(m_scale.m_x * RESCALE, m_scale.m_y * RESCALE, m_scale.m_z * RESCALE);

		glColor4f(1.0f, 0.0f, 0.0f, 1.0f);
		glBegin(GL_LINE_LOOP);
		glVertex3f(m_min.m_x, m_min.m_y, m_min.m_z);
		glVertex3f(m_min.m_x, m_max.m_y, m_min.m_z);
		glVertex3f(m_max.m_x, m_max.m_y, m_min.m_z);
		glVertex3f(m_max.m_x, m_min.m_y, m_min.m_z);
		glEnd();

		glBegin(GL_LINE_LOOP);
		glVertex3f(m_min.m_x, m_min.m_y, m_max.m_z);
		glVertex3f(m_min.m_x, m_max.m_y, m_max.m_z);
		glVertex3f(m_max.m_x, m_max.m_y, m_max.m_z);
		glVertex3f(m_max.m_x, m_min.m_y, m_max.m_z);
		glEnd();

		glBegin(GL_LINE_LOOP);
		glVertex3f(m_max.m_x, m_max.m_y, m_min.m_z);
		glVertex3f(m_max.m_x, m_max.m_y, m_max.m_z);
		glVertex3f(m_min.m_x, m_max.m_y, m_max.m_z);
		glVertex3f(m_min.m_x, m_max.m_y, m_min.m_z);
		glEnd();

		glBegin(GL_LINE_LOOP);
		glVertex3f(m_max.m_x, m_min.m_y, m_max.m_z);
		glVertex3f(m_min.m_x, m_min.m_y, m_max.m_z);
		glVertex3f(m_min.m_x, m_min.m_y, m_min.m_z);
		glVertex3f(m_max.m_x, m_min.m_y, m_min.m_z);				
		glEnd();

		glEnable (GL_TEXTURE_2D);
		glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
	}
	glPopMatrix();
}