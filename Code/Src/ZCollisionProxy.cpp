#include "ZCollisionProxy.h"

#include "ZBulletProxy.h"
#include "ZBullet.h"
#include "ZCamera.h"
#include "ZLevel.h"
#include "ZEnemyManager.h"
#include "ZPlayerManager.h"

ZCollisionProxy::ZCollisionProxy(ZLevel& nLevel)
: m_level(nLevel)
{

}

ZCollisionProxy::~ZCollisionProxy()
{

}

FrustumIntersection ZCollisionProxy::sphereInFrustum(const Vector3& center, float radius) 
{	
	float distance = 0;
	FrustumIntersection collisions = 0;
	collisions[INTERN_TO_FRUSTUM] = 1;	

	ZCamera* camera = ZCamera::getInstance();
	for (int face = 0; face < 6; ++face)
	{
		distance = camera->frustum[face][0] * center.m_x + 
			camera->frustum[face][1] * center.m_y + 
			camera->frustum[face][2] * center.m_z + 
			camera->frustum[face][3];

		if (distance <= -radius) // the sphere is outside the frustum, return 
		{
			collisions[EXTERN_TO_FRUSTUM] = 1;
			return collisions;
		}
		else if (distance > radius) // the sphere is inside the frustum, related to 'face'
		{			
			continue;
		}
		else if ((distance <= +radius) && (distance > -radius)) // la sfera interseca la faccia 'face' del frustum
		{
			collisions[INTERN_TO_FRUSTUM] = 0;
			collisions[face] = 1;
			continue; 
		}
	}

	return collisions;
}

void ZCollisionProxy::update()
{
	std::list<ZEnemy*> visibleEnemies = ZEnemyManager::getInstance()->getVisibleEnemies();
	for (std::list<ZEnemy*>::iterator itEnemy = visibleEnemies.begin(), endEnemy = visibleEnemies.end();itEnemy != endEnemy;++itEnemy)
	{
		ZEnemy* currentEnemy = *itEnemy;

		// collision ENEMY / BULLET
		std::list<ZBullet*> visibleBullets = m_level.getBulletProxy()->getVisibleBullets();
		for (std::list<ZBullet*>::iterator itBullet = visibleBullets.begin(), endBullet = visibleBullets.end();itBullet != endBullet;++itBullet)
		{
			ZBullet* currentBullet = *itBullet;
			ZBoundingVolume* bulletBoundingVolume = currentBullet->getBoundingVolume();
			ZBoundingVolume* enemyBoundingVolume = currentEnemy->getBoundingVolume();
			if (bulletBoundingVolume->isCollision(*enemyBoundingVolume))
			{
   				currentEnemy->takeHit();
  				currentBullet->setIsVisible(false);
				break;
			}
		}

		if (!currentEnemy->getIsAlive()) continue;

		// collision ENEMY / PLAYER
		std::vector<ZPlayer*> players = ZPlayerManager::getInstance()->getPlayers();
		for (std::vector<ZPlayer*>::iterator itPlayer = players.begin(), endPlayer = players.end();itPlayer != endPlayer;++itPlayer)
		{
			ZPlayer* currentPlayer = *itPlayer;
			if (!currentPlayer->getIsAlive()) continue;

			ZBoundingVolume* playerBoundingVolume = currentPlayer->getBoundingVolume();
			ZBoundingVolume* enemyBoundingVolume = currentEnemy->getBoundingVolume();
			if (playerBoundingVolume->isCollision(*enemyBoundingVolume))
			{
				currentEnemy->die();
				currentPlayer->takeHit();
				break;
			}
		}
	}
}