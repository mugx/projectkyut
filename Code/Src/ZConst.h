#ifndef ZConst_h
#define ZConst_h

#include <QString>

class ZConst
{
public:
	static const int NUMBER_OF_UPDATE_PER_SECOND = 25;
	static const int TIME_OF_UPDATE = 1000 / NUMBER_OF_UPDATE_PER_SECOND;
	static const int MAX_FRAMESKIP = 5;
	static float top_color_white[4];
	static float bottom_color_violet[4];
	static float bottom_color_red[4];
	static float bottom_color_blue[4];
	static float bottom_color_black[4];
	static float bottom_color_green[4];
	static const int FIRST_LEVEL_NUMBER;
	static const int PLAYER_WITH_KEYBOARD_ID;
	static const int PLAYER_WITH_PAD_ID;
	static const QString PATH_SOUNDS;
	static const QString PATH_PLAYER_MODELS;
	static const QString PATH_PLAYER_TEXTURES;
	static const QString PATH_ENEMY_MODELS;
	static const QString PATH_ENEMY_TEXTURES;
	static const QString PATH_BACKGROUND_MODELS;
	static const QString PATH_BACKGROUND_TEXTURES;
	static const QString PATH_SKYBOX_TEXTURES;
	static const QString TAG_RESOURCES;
	static const QString TAG_PLAYER_RESOURCE;
	static const QString TAG_ENEMY_RESOURCE;
	static const QString TAG_BACKGROUND_RESOURCE;
	static const QString TAG_SKYBOX_RESOURCE;
	static const QString TAG_LEVEL;
	static const QString TAG_PLAYER;
	static const QString TAG_ENEMY;
	static const QString TAG_BACKGROUND;
	static const QString TAG_SKYBOX;
	static const QString TAG_UNKNOWN;
};

#endif