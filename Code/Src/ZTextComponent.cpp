#include "ZTextComponent.h"

#include <cmath> 
#include <iostream>
#include <QtCore/QHash>
#include <QtCore/QSysInfo>
#include <QtGui/QPainter>
#include <QtGui/QPixmap>
#include <QtOpenGL/QGLFormat>
#include <QtOpenGL/QGLFramebufferObject>
#include "ZConfig.h"
#include "ZCore.h"

namespace
{
	const int TEXTURE_SIZE = 256;

	struct CharData
	{
		GLuint textureId;
		uint width;
		uint height;
		GLfloat s[2];
		GLfloat t[2];
	};
} 

struct TextPrivate
{
	TextPrivate(const QFont& font, const QPen& pen);
	virtual ~TextPrivate();
	void allocateTexture();
	CharData &createCharacter(QChar c);

	QFont font;
	QFontMetrics fontMetrics;
	QPen pen;
	QHash<ushort, CharData> characters;
	QList<GLuint> textures;
	GLint xOffset;
	GLint yOffset;
};

TextPrivate::TextPrivate(const QFont &font, const QPen& pen)
: font(font), fontMetrics(font), pen(pen), xOffset(0), yOffset(0)
{
}

TextPrivate::~TextPrivate()
{
	foreach (GLuint texture, textures)
	{
		glDeleteTextures(1, &texture);
	}
}

void TextPrivate::allocateTexture()
{
	GLuint texture;
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	QImage image(TEXTURE_SIZE, TEXTURE_SIZE, QImage::Format_ARGB32);
	image.fill(Qt::transparent);
	image = QGLWidget::convertToGLFormat(image);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_ALPHA, TEXTURE_SIZE, TEXTURE_SIZE, 0, GL_RGBA, GL_UNSIGNED_BYTE, image.bits());

	textures += texture;
}

CharData &TextPrivate::createCharacter(QChar c)
{
	ushort unicodeC = c.unicode();
	if (characters.contains(unicodeC))
		return characters[unicodeC];

	if (textures.empty())
		allocateTexture();

	GLuint texture = textures.last();

	GLsizei width = fontMetrics.width(c);
	GLsizei height = fontMetrics.height();

	QPixmap pixmap(width, height);
	pixmap.fill(Qt::transparent);

	QPainter painter;
	painter.begin(&pixmap);
	painter.setRenderHints(QPainter::HighQualityAntialiasing | QPainter::TextAntialiasing);
	painter.setFont(font);
	painter.setPen(pen);

	painter.drawText(0, fontMetrics.ascent(), c);
	painter.end();

	QImage image = QGLWidget::convertToGLFormat(pixmap.toImage());
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexSubImage2D(GL_TEXTURE_2D, 0, xOffset, yOffset, width, height, GL_RGBA, GL_UNSIGNED_BYTE, image.bits());

	CharData& character = characters[unicodeC];
	character.textureId = texture;
	character.width = width;
	character.height = height;
	character.s[0] = static_cast<GLfloat>(xOffset) / TEXTURE_SIZE;
	character.t[0] = static_cast<GLfloat>(yOffset) / TEXTURE_SIZE;
	character.s[1] = static_cast<GLfloat>(xOffset + width) / TEXTURE_SIZE;
	character.t[1] = static_cast<GLfloat>(yOffset + height) / TEXTURE_SIZE;

	xOffset += width;
	if (xOffset + fontMetrics.maxWidth() >= TEXTURE_SIZE)
	{
		xOffset = 1;
		yOffset += height;
	}
	if (yOffset + fontMetrics.height() >= TEXTURE_SIZE)
	{
		allocateTexture();
		yOffset = 1;
	}
	return character;
}

ZTextComponent::ZTextComponent(const QFont& font, const QPen& pen)
{
	m_textPrivate = new TextPrivate(font, pen);
}

ZTextComponent::ZTextComponent(int _pointSize)
{
	double aspectRatio = static_cast<double>(ZConfig::s_size.width()) / static_cast<double>(ZConfig::s_size.height());
	QPen pen(QColor(100, 100, 100, 255), 2.0 / aspectRatio);
	QFont font("System", _pointSize * aspectRatio, QFont::Bold, false);
	m_textPrivate = new TextPrivate(font, pen);
}

ZTextComponent::~ZTextComponent()
{
	z_delete(m_textPrivate);
}

QFont ZTextComponent::font() const
{
	return m_textPrivate->font;
}

QFontMetrics ZTextComponent::fontMetrics() const
{
	return m_textPrivate->fontMetrics;
}

void ZTextComponent::renderText(bool _isBlinking)
{
	glPushMatrix();
	{
		glPushAttrib(GL_CURRENT_BIT | GL_ENABLE_BIT | GL_TEXTURE_BIT);	
		glEnable(GL_TEXTURE_2D);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		GLuint texture = 0;
		glTranslatef(m_x, m_y, 0);
		for (int i = 0; i < m_text.length(); ++i)
		{
			CharData &c = m_textPrivate->createCharacter(m_text[i]);
			if (texture != c.textureId)
			{
				texture = c.textureId;
				glBindTexture(GL_TEXTURE_2D, texture);
			}

			glBegin(GL_QUADS);
			if (!_isBlinking)
			{
				glColor4fv(m_topColor);
			}
			else
			{
				glColor4fv(m_topColor_blinking);

				static float factorIncr = +1.0f;
				m_topColor_blinking[3] += factorIncr * static_cast<float>(ZConfig::s_blinking) / 1000.0f;
				factorIncr = m_topColor_blinking[3] > 1.0f ? -1.0f : factorIncr;
				factorIncr = m_topColor_blinking[3] < 0.0f ? +1.0f : factorIncr;
			}

			glTexCoord2f(c.s[0], c.t[0]);
			glVertex2f(0, 0);

			glTexCoord2f(c.s[1], c.t[0]);
			glVertex2f(c.width, 0);

			if (!_isBlinking)
			{
				glColor4fv(m_bottomColor);
			}
			else
			{
				glColor4fv(m_bottomColor_blinking);

				static float factorIncr = +1.0f;
				m_bottomColor_blinking[3] += factorIncr * static_cast<float>(ZConfig::s_blinking) / 1000.0f;
				factorIncr = m_bottomColor_blinking[3] > 1.0f ? -1.0f : factorIncr;
				factorIncr = m_bottomColor_blinking[3] < 0.0f ? +1.0f : factorIncr;
			}

			glTexCoord2f(c.s[1], c.t[1]);
			glVertex2f(c.width, c.height);

			glTexCoord2f(c.s[0], c.t[1]);
			glVertex2f(0, c.height);
			glEnd();

			glTranslatef(c.width, 0, 0);
		}	
		glPopAttrib();
	}
	glPopMatrix();
}

int ZTextComponent::countPixelsWidth()
{
	int countPixel = 0;
	int count = m_text.length();
	for (int i = 0;i < count;++i)
	{
		countPixel += m_textPrivate->fontMetrics.width(m_text.at(0));
	}

	return countPixel;
}

void ZTextComponent::setTopColor(const float* _topColor) 
{
	m_topColor = _topColor;
	m_topColor_blinking[0] = m_topColor[0];
	m_topColor_blinking[1] = m_topColor[1];
	m_topColor_blinking[2] = m_topColor[2];
	m_topColor_blinking[3] = m_topColor[3];
}

void ZTextComponent::setBottomColor(const float* _bottomColor) 
{
	m_bottomColor = _bottomColor;
	m_bottomColor_blinking[0] = m_bottomColor[0];
	m_bottomColor_blinking[1] = m_bottomColor[1];
	m_bottomColor_blinking[2] = m_bottomColor[2];
	m_bottomColor_blinking[3] = m_bottomColor[3];
}