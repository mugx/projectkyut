#include "ZExplosionSystem.h"
#include "ZConfig.h"
#include "ZMath.h"

#define NUM_PARTICLES 150

ZExplosionSystem::ZExplosionSystem(ZBaseEntity& _baseEntityParent)
: m_baseEntityParent(_baseEntityParent),
m_textureExplosion(0),
m_slowdown(1.0f / 5000.0f)
{
	
	m_textureExplosion = ZTextureManager::getInstance()->loadTexture(":/textures/weapons/particleMiniExplosion.png");
	setNumParticles(NUM_PARTICLES);
}

ZExplosionSystem::~ZExplosionSystem()
{
	ZTextureManager::getInstance()->removeTexture(m_textureExplosion);
}

void ZExplosionSystem::initParticle(ZParticle* _particle)
{
	if (!_particle) return;

	_particle->setType(ZParticle::EXPLOSION1);
	_particle->setPosition(m_baseEntityParent.getPosition());

	// velocity & aceleration	
	_particle->getVelocity().m_x = static_cast<float>(((rand()%40)+(rand()%40)+(rand()%40)-60.0f)*5.0f) * m_slowdown;
	_particle->getVelocity().m_y = static_cast<float>(((rand()%40)+(rand()%40)+(rand()%40)-60.0f)*5.0f) * m_slowdown;
	_particle->getVelocity().m_z = 0.0f;

	_particle->getAceleration().m_x = -_particle->getVelocity().m_x * 0.045f;
	_particle->getAceleration().m_y = -_particle->getVelocity().m_y * 0.045f;
	_particle->getAceleration().m_z = 0.0f;

	_particle->setScale(Vector3(10.0f));
	_particle->setColor(Vector4(0.4f, 0.3f, 0.1f, 1.0f));
	_particle->setLife(1.0f);
	_particle->setFade(static_cast<float>(rand() % 100) * 0.001f + 0.023f);
	_particle->setIsVisible(true);
	_particle->setParticleSystem(this);
}

void ZExplosionSystem::orientTowardsCamera()
{
	float modelview[16];
	glGetFloatv(GL_MODELVIEW_MATRIX , modelview); 
	for (int i = 0;i < 3;++i)
	{
		for (int j = 0;j < 3;++j)
		{
			if (i == j) 
			{
				modelview[i * 4 + j] = 1.0f;
			}
			else
			{
				modelview[i * 4 + j] = 0.0f;
			}
		}
	}
	glLoadMatrixf(modelview);
}	

void ZExplosionSystem::update(ZParticle* _particle)
{
	if (!_particle->getIsVisible()) return;

	_particle->setPosition(_particle->getPosition() + _particle->getVelocity());
	_particle->setVelocity(_particle->getVelocity() + _particle->getAceleration());	
	_particle->setLife(_particle->getLife() - _particle->getFade());

	if (_particle->getLife() < 0.4f) 
	{
		_particle->setLife(_particle->getLife() - _particle->getFade());

		if (_particle->getLife() <= 0.0f)
		{
			_particle->setIsVisible(false);	
		}
	}
}

void ZExplosionSystem::draw(ZParticle* _particle)
{	
	if (!_particle->getIsVisible()) return;

	glPushMatrix();
	{
		_particle->SetViewPosition(_particle->getPosition() + _particle->getVelocity() * ZConfig::s_interpolation);
		glTranslatef(_particle->getViewPosition().m_x, _particle->getViewPosition().m_y, _particle->getViewPosition().m_z);		
		orientTowardsCamera();	
		glBindTexture(GL_TEXTURE_2D, m_textureExplosion->m_idTexture);	
		glColor4f(_particle->getColor().m_x, _particle->getColor().m_y, _particle->getColor().m_z, _particle->getLife());
		glScalef(_particle->getScale().m_x, _particle->getScale().m_y, _particle->getScale().m_z);
		glBegin(GL_TRIANGLE_STRIP);
		glTexCoord2d(1,1); glVertex3f(0.05f, 0.05f, 0.0f);
		glTexCoord2d(0,1); glVertex3f(-0.05f, 0.05f, 0.0f);
		glTexCoord2d(1,0); glVertex3f(0.05f, -0.05f, 0.0f);
		glTexCoord2d(0,0); glVertex3f(-0.05f, -0.05f, 0.0f);
		glEnd();
	}
	glPopMatrix();
}