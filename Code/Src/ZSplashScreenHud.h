#ifndef ZSplashScreenHud_h
#define ZSplashScreenHud_h

#include "ZHud.h"

class ZSplashScreenHud : public ZHud
{
public:
	explicit ZSplashScreenHud();
	virtual ~ZSplashScreenHud();
	virtual void refreshHud();
	virtual void destroyHud();
	virtual void update() {}
	virtual void draw();

private:
	ZTextComponent* m_textComponent1;
	ZTextComponent* m_textComponent2;
};

#endif