#ifndef ZParticleProxy_h
#define ZParticleProxy_h

#include <vector>
#include <list>
#include <algorithm>

class ZLevel;
class ZParticle;
class ZParticleSystem;

class ZParticleProxy
{
public:	
	explicit ZParticleProxy(ZLevel& _level);
	virtual ~ZParticleProxy();

	void doEmit(ZParticleSystem* _particleSystem);
	std::list<ZParticle*> getVisibleParticles() { return m_visibleParticles; }
	void update();
	void draw();	

private:
	std::vector<ZParticle*> m_particles;
	std::list<ZParticle*> m_visibleParticles;
	ZLevel& m_level;
};

#endif

/*
#ifndef ZParticleProxy_h
#define ZParticleProxy_h

#include <list>
//#include "ParticleSystem.h"
//#include "AfterburnSystem.h"
//#include "BaseEntity.h"
//#include "SoundManager.h"

#define MAX_EXPLOSION 10
#define MAX_MINI_EXPLOSION 20
#define MAX_AFTERBURN 1

class ZLevel;

class ZParticleProxy
{
public:
	explicit ZParticleProxy(ZLevel& nLevel);
	virtual ~ZParticleProxy();

	void update();
	void draw();

private:
	ZLevel& mLevel;
	
	static ZParticleManager* getInstance();	
	SoundManager* soundManager;

	AfterburnSystem* MakeAfterburn(BaseEntity* baseEntity);
	void MakeMiniExplosion(BaseEntity* baseEntity);
	void MakeExplosion(BaseEntity* baseEntity);

	void BeginDraw()
	{		
		glDisable(GL_CULL_FACE);
	}

	void EndDraw()
	{
		glEnable(GL_CULL_FACE);
		//glBindTexture(GL_TEXTURE_2D, 0);
	}

	void update();
	void draw();
protected:
	ParticleManager();	
	~ParticleManager();
private:
	static ParticleManager* _instance;
	std::list<ParticleSystem*> particleSystems;

	void Init();
};

#endif
*/