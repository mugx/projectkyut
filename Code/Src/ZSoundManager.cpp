#include "ZSoundManager.h"

#include <QtGui>
#include <qdir.h>
#include <qfile.h>
#include <qsound.h>
#include <qtimer.h>
#include "ZConst.h"
#include "ZConfig.h"

ZSoundManager::ZSoundManager() 
: m_isSoundEnabled(ZConfig::s_isSoundEnabledByDefault)
{
	m_device = audiere::OpenDevice();
	AddSound("robotVoice.wav");
	AddSound("enigma.mod");
	AddSound("laser.wav");
	AddSound("menu_select_item.mp3");

	enableSound(m_isSoundEnabled);
}

ZSoundManager::~ZSoundManager()
{
	m_sounds.clear(); 
}

void ZSoundManager::AddSound(QString _filename)
{
	QString finalPath = ZConst::PATH_SOUNDS + _filename;

	QFile robotVoice_file(finalPath.toStdString().c_str());
	if (!robotVoice_file.open(QIODevice::ReadOnly))
	{
		return;
	}

	QByteArray& robotVoice_array = robotVoice_file.readAll();
	robotVoice_file.close();

	audiere::File* audiereFile = audiere::CreateMemoryFile(robotVoice_array.constData(), robotVoice_array.size());
	std::string soundKey(QFileInfo(robotVoice_file).baseName().toStdString());
	audiere::OutputStreamPtr soundValue(audiere::OpenSound(m_device, audiereFile));

	m_sounds[soundKey] = soundValue;
}

void ZSoundManager::play(const char* _soundName)
{
	if (m_isSoundEnabled)
	{
		std::map<std::string, audiere::OutputStreamPtr>::iterator it = m_sounds.find(_soundName);
		if (it != m_sounds.end())
		{
			m_sounds[_soundName]->setPosition(0);
			m_sounds[_soundName]->play();
		}
	}
}

void ZSoundManager::enableSound(bool _isSoundEnabled) 
{
	m_isSoundEnabled = _isSoundEnabled;

	for (std::map<std::string, audiere::OutputStreamPtr>::iterator it = m_sounds.begin(), end = m_sounds.end();it != end;++it)
	{
		(*it).second->setVolume(m_isSoundEnabled ? 1.0f : 0.0f);
	}
}