#ifndef ZGameScreen_h
#define ZGameScreen_h

#include "ZScreen.h"
#include "ZGameScreenHud.h"

class ZGameScreen : public ZScreen
{
	friend class ZGameScreenHud;
public:
	ZGameScreen();
	virtual ~ZGameScreen();
	virtual void update();
	virtual void draw();
	virtual void setKeyboardAction(QKeyEvent* _keyboardEvent, bool _isPressed);
	virtual void refreshResolution();

	ZLevel* getCurrentLevel();
	void startGame();

private:	
	ZGameScreenHud* m_gameScreenHud;
};

#endif