#include "ZSkybox.h"

#include <sstream>
#include "ZConfig.h"
#include "ZTextureManager.h"
#include "ZScenarioManager.h"
#include "ZPlayerManager.h"

GLfloat ZSkybox::s_verticesArray_top[] = { -500.0f, 500.0f, -500.0f, -500.0f, 500.0f, 500.0f, 500.0f,  500.0f, 500.0f, 500.0f, 500.0f, -500.0f };	
GLfloat ZSkybox::s_verticesArray_bottom[] = { -500.0f, -500.0f, -500.0f, -500.0f, -500.0f, 500.0f, 500.0f, -500.0f, 500.0f, 500.0f, -500.0f, -500.0f };
GLfloat ZSkybox::s_verticesArray_left[] = { -500.0f, -500.0f, -500.0f, -500.0f, -500.0f, 500.0f, -500.0f, 500.0f, 500.0f, -500.0f, 500.0f, -500.0f };
GLfloat ZSkybox::s_verticesArray_right[] = { 500.0f, -500.0f, 500.0f, 500.0f, -500.0f, -500.0f, 500.0f,  500.0f, -500.0f, 500.0f, 500.0f, 500.0f };
GLfloat ZSkybox::s_verticesArray_front[] = { 500.0f, -500.0f, -500.0f, -500.0f, -500.0f, -500.0f, -500.0f,  500.0f, -500.0f, 500.0f,  500.0f, -500.0f };
GLfloat ZSkybox::s_verticesArray_back[] = { -500.0f, -500.0f, 500.0f, 500.0f, -500.0f, 500.0f, 500.0f, 500.0f, 500.0f, -500.0f, 500.0f, 500.0f };

GLfloat ZSkybox::s_texCoordsArray_top[] = { 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f };
GLfloat ZSkybox::s_texCoordsArray_bottom[] = { 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f };
GLfloat ZSkybox::s_texCoordsArray_left[] = { 0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f };	
GLfloat ZSkybox::s_texCoordsArray_right[] = { 0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f };
GLfloat ZSkybox::s_texCoordsArray_front[] = { 0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f };
GLfloat ZSkybox::s_texCoordsArray_back[] = { 0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f };


ZSkybox::ZSkybox(int _id, ZLevel* _level)
: m_position(0.0f, 0.0f, 0.0f),
m_id(_id),
m_currentLevel(_level)
{	
	m_textures[SKYBOX_TOP] = ZScenarioManager::getInstance()->getSkyboxTexture(SKYBOX_TOP);
	m_textures[SKYBOX_TOP]->m_texCoordsArray = s_texCoordsArray_top;

	m_textures[SKYBOX_BOTTOM] = ZScenarioManager::getInstance()->getSkyboxTexture(SKYBOX_BOTTOM);
	m_textures[SKYBOX_BOTTOM]->m_texCoordsArray = s_texCoordsArray_bottom;

	m_textures[SKYBOX_LEFT] = ZScenarioManager::getInstance()->getSkyboxTexture(SKYBOX_LEFT);
	m_textures[SKYBOX_LEFT]->m_texCoordsArray = s_texCoordsArray_left;

	m_textures[SKYBOX_RIGHT] = ZScenarioManager::getInstance()->getSkyboxTexture(SKYBOX_RIGHT);
	m_textures[SKYBOX_RIGHT]->m_texCoordsArray = s_texCoordsArray_right;

	m_textures[SKYBOX_FRONT] = ZScenarioManager::getInstance()->getSkyboxTexture(SKYBOX_FRONT);
	m_textures[SKYBOX_FRONT]->m_texCoordsArray = s_texCoordsArray_front;

	m_textures[SKYBOX_BACK] = ZScenarioManager::getInstance()->getSkyboxTexture(SKYBOX_BACK);
	m_textures[SKYBOX_BACK]->m_texCoordsArray = s_texCoordsArray_back;
}

ZSkybox::~ZSkybox()
{

}

void ZSkybox::update()
{
	if (m_rotation.m_x == 360.0f) 
	{
		m_rotation.m_x = 0.0f;
	}	
	m_angularVelocity.m_x = -0.05f;
	m_angularVelocity.m_x *= ZPlayerManager::getInstance()->arePlayersAlive() ? 1.0f : 0.0f;
	m_rotation += m_angularVelocity;
}

void ZSkybox::draw()
{
	glPushMatrix();
	{
		m_viewRotation = m_rotation + m_angularVelocity * ZConfig::s_interpolation;
		glRotatef(m_viewRotation.m_x, 1.0f, 0.0f, 0.0f);
		glRotatef(m_viewRotation.m_y, 0.0f, 1.0f, 0.0f);
		glRotatef(m_viewRotation.m_z, 0.0f, 0.0f, 1.0f);

		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); //no alfa blending

		// back
		glVertexPointer(3, GL_FLOAT, 0, s_verticesArray_back);
		glTexCoordPointer(2, GL_FLOAT, 0, s_texCoordsArray_back);
		glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
		glBindTexture(GL_TEXTURE_2D, m_textures[SKYBOX_BACK]->m_idTexture);
		glDrawArrays(GL_QUADS, 0, 4);
		glBindTexture(GL_TEXTURE_2D, 0);

		// right
		glVertexPointer(3, GL_FLOAT, 0, s_verticesArray_right);
		glTexCoordPointer(2, GL_FLOAT, 0, s_texCoordsArray_right);
		glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
		glBindTexture(GL_TEXTURE_2D, m_textures[SKYBOX_RIGHT]->m_idTexture);
		glDrawArrays(GL_QUADS, 0, 4);
		glBindTexture(GL_TEXTURE_2D, 0);

		// front
		glVertexPointer(3, GL_FLOAT, 0, s_verticesArray_front);
		glTexCoordPointer(2, GL_FLOAT, 0, s_texCoordsArray_front);
		glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
		glBindTexture(GL_TEXTURE_2D, m_textures[SKYBOX_FRONT]->m_idTexture);
		glDrawArrays(GL_QUADS, 0, 4);
		glBindTexture(GL_TEXTURE_2D, 0);

		// left
		glVertexPointer(3, GL_FLOAT, 0, s_verticesArray_left);
		glTexCoordPointer(2, GL_FLOAT, 0, s_texCoordsArray_left);
		glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
		glBindTexture(GL_TEXTURE_2D, m_textures[SKYBOX_LEFT]->m_idTexture);
		glDrawArrays(GL_QUADS, 0, 4);
		glBindTexture(GL_TEXTURE_2D, 0);	

		// top
		glVertexPointer(3, GL_FLOAT, 0, s_verticesArray_top);
		glTexCoordPointer(2, GL_FLOAT, 0, s_texCoordsArray_top);
		glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
		glBindTexture(GL_TEXTURE_2D, m_textures[SKYBOX_TOP]->m_idTexture);
		glDrawArrays(GL_QUADS, 0, 4);
		glBindTexture(GL_TEXTURE_2D, 0);

		// bottom
		glVertexPointer(3, GL_FLOAT, 0, s_verticesArray_bottom);
		glTexCoordPointer(2, GL_FLOAT, 0, s_texCoordsArray_bottom);
		glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
		glBindTexture(GL_TEXTURE_2D, m_textures[SKYBOX_BOTTOM]->m_idTexture);
		glDrawArrays(GL_QUADS, 0, 4);
		glBindTexture(GL_TEXTURE_2D, 0);
	}
	glPopMatrix();
}