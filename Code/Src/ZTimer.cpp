#include "ZTimer.h"
#include "ZConst.h"

ZTimer::ZTimer(int _timeout)
: m_timeout(_timeout),
m_initialTimeout(_timeout)
{
}

ZTimer::~ZTimer()
{
}

void ZTimer::startTimer()
{
	m_timeout = m_initialTimeout;
	m_isRunning = true;
}

void ZTimer::stopTimer()
{
	m_isRunning = false;
}

bool ZTimer::isRunning()
{
	return m_isRunning && m_timeout > 0;
}

bool ZTimer::isTimeout()
{
	return m_timeout <= 0;
}

void ZTimer::update()
{
	if (m_isRunning && m_timeout > 0)
	{
		m_timeout -= ZConst::TIME_OF_UPDATE;
	}
}