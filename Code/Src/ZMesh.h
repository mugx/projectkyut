#ifndef ZMesh_h
#define ZMesh_h

#include "ZMath.h"
#include "ZTexture.h"
#include "ZBoundingVolume.h"
#include "ZBaseEntity.h"

#define MAX_VERTICES 25000
#define MAX_POLYGONS 25000

class ZBaseEntity;
class ZBoundingVolume;

class ZMesh
{
public:	
	enum VertexType
	{
		FIRST_VERTEX,
		SECOND_VERTEX,
		THIRD_VERTEX,
	};

	explicit ZMesh(Vector3 _scale);
	virtual ~ZMesh();

	void bindTexture(ZTexture* _texture);
	void makeMesh(bool _isSimple = false);
	void draw();

	Vector3 m_color;
	float m_alpha;
	char m_name[20];
	bool m_loadingResult;
	int m_verticesCardinality;
    int m_polygonsCardinality;
	Vector3* m_vertices;
	Vector3* m_normals;
	Vector2* m_texCoords;
	Vec3<short>* m_triangles;    
	GLfloat* m_verticesArray;
	GLfloat* m_normalsArray;
	GLfloat* m_texCoordsArray;
	unsigned int m_idMesh;
	GLuint m_idTexture;
	Vector3 m_scale;	

private:
	void makeTexCoordsArray();
	void makeVerticesArray();
	void makeNormalsArray();	
};

#endif