#ifndef ProjectKyut_h
#define ProjectKyut_h

#include <QApplication>

class ProjectKyut : public QApplication
{
public:
	ProjectKyut(int argc, char *argv[]);
	virtual ~ProjectKyut();
	int exec();
};

#endif
	