#ifndef ZMenuScreen_h
#define ZMenuScreen_h

#include "ZScreen.h"

#include <QtGui>
#include <QGLWidget>

class ZMenuScreen : public ZScreen
{
	Q_OBJECT
public:		
	explicit ZMenuScreen();
	virtual ~ZMenuScreen();
	virtual void refreshResolution();
	virtual void update();
	virtual void draw();
};

#endif