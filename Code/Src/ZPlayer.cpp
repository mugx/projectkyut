#include "ZPlayer.h"

#include "ZSoundManager.h"
#include "ZConfig.h"
#include <gl/glew.h>
#include "ZMeshManager.h"
#include "ZTextureManager.h"
#include "ZBulletProxy.h"
#include "ZCollisionProxy.h"
#include "ZMath.h"
#include "ZPlayerManager.h"
#include "ZCore.h"

#define FIRE_TIMEOUT 500
#define INVULNERABILITY_TIMEOUT 2000
#define NUM_LIVES_PLAYER 3

ZPlayer::ZPlayer(int _id, ZLevel* _level, Vector3& _position)
: m_laserSystem(0),
isInThirdPerson(false),
m_fireTimer(FIRE_TIMEOUT),
m_invulnerabilityTimer(INVULNERABILITY_TIMEOUT)
{
	setId(_id);
	restart(_level, _position);

	m_viewPosition = Vector3(0.0f);
	m_multiplicativeFactorOfSpeed = 4.0f;
	m_initialSpeed = 0.05f;
	m_initialRotation = Vector3(0, 0, 270);
	m_rotation = m_initialRotation;
	m_laserSystem = new ZLaserSystem(*this); // setting weapon system

	m_mesh = ZPlayerManager::getInstance()->getMesh(_id);
	m_scale = m_mesh->m_scale;
	m_texture = ZPlayerManager::getInstance()->getTexture(_id);

	makeBoundingVolume();
}

ZPlayer::~ZPlayer()
{
	actionMap.clear();
	z_delete_0(m_laserSystem);
}

void ZPlayer::setAction(ActionType nAction, bool nIsPressed)
{
	actionMap[nAction] = nIsPressed;
}

void ZPlayer::die()
{
	setStatus(STATUS_DEAD);
	setNumLives(0);
}

void ZPlayer::takeHit()
{
	if (getStatus() != STATUS_ALIVE) return;

	setNumLives(getNumLives() - 1);	
	setStatus(STATUS_BEGIN_INVULNERABLE);

	if (getNumLives() == 0)
	{
		die();
	}
}

void ZPlayer::restart(ZLevel* _level, Vector3& _position)
{
	setStatus(STATUS_ALIVE);
	setNumLives(NUM_LIVES_PLAYER);
	setCurrentLevel(_level);
	setPosition(_position);
	setIsVisible(m_id == ZPlayerManager::PLAYER_WITH_KEYBOARD_TYPE);
}

void ZPlayer::updateStatus()
{
	AiStatus currentStatus = getStatus();
	switch (currentStatus)
	{
	case STATUS_IDLE: break;
	case STATUS_BEGIN_INVULNERABLE: 
		m_invulnerabilityTimer.startTimer();
		setStatus(STATUS_INVULNERABLE);
		break;
	case STATUS_INVULNERABLE:
		if (m_invulnerabilityTimer.isRunning()) 
		{
			m_invulnerabilityTimer.update();

			static int factor = -1;
			m_mesh->m_alpha += 0.2f * factor;
			if (m_mesh->m_alpha <= 0)
			{
				factor = +1;
			}
			else if (m_mesh->m_alpha >=  1.0f)
			{
				factor = -1;
			}
		}
		else 
		{
			setStatus(STATUS_ALIVE);
			m_invulnerabilityTimer.stopTimer();
			m_mesh->m_alpha = 1.0f;		
		}		
		break;
	}
}

void ZPlayer::update()
{
	if (!getIsVisible()) return;

	m_fireTimer.update();
	updateStatus();

	// status management

	if (actionMap[ACTION_LEFT])
	{
		m_velocity.m_x = -m_initialSpeed * m_multiplicativeFactorOfSpeed;
		m_angularVelocity.m_y = m_rotation.m_y > (m_initialRotation.m_y - 60.0f) ? -12.0f : 0.0f;
	}
	else if (actionMap[ACTION_RIGHT])
	{
		m_velocity.m_x = +m_initialSpeed * m_multiplicativeFactorOfSpeed;
		m_angularVelocity.m_y = m_rotation.m_y < (m_initialRotation.m_y + 60.0f) ? +12.0f : 0.0f;
	}
	else 
	{
		m_velocity.m_x = 0.0f;
		//--- wings rotation ---//
		if ((m_rotation.m_y > m_initialRotation.m_y) && (m_rotation.m_y <= m_initialRotation.m_y + 60.0f))
		{
			m_angularVelocity.m_y = -12.0f;
		}
		else if ((m_rotation.m_y < m_initialRotation.m_y) && (m_rotation.m_y >= m_initialRotation.m_y - 60.0f))
		{
			m_angularVelocity.m_y = +12.0f;
		}
		else 
		{
			m_angularVelocity.m_y = 0.0f;
		}
	}

	if (actionMap[ACTION_UP])
	{
		m_velocity.m_y = +m_initialSpeed * (m_multiplicativeFactorOfSpeed + 1.0f);
	}
	else if (actionMap[ACTION_DOWN])
	{
		m_velocity.m_y = -m_initialSpeed * m_multiplicativeFactorOfSpeed;
	}
	else 
	{
		m_velocity.m_y = m_initialSpeed;
	}

	if (actionMap[ACTION_FIRE] && !m_fireTimer.isRunning())
	{
		ZSoundManager::getInstance()->play("laser");
		m_currentLevel->getBulletProxy()->doEmit(m_laserSystem);
		m_fireTimer.startTimer();
	}
	else if (!actionMap[ACTION_FIRE])
	{
		m_fireTimer.stopTimer();
	}

	// BEGIN - CHECK COLLISIONE TRA PLAYER E VIEW FRUSTUM 							 
	FrustumIntersection culling = m_currentLevel->getCollisionProxy()->sphereInFrustum(getPosition(), m_boundingVolume->m_radius * m_boundingVolume->m_scale.m_x);
	if (!culling[ZCollisionProxy::INTERN_TO_FRUSTUM]) 
	{
		if (culling[ZCollisionProxy::RIGHT_PLANE]) //check east
		{
			m_velocity.m_x = m_velocity.m_x > 0.0f ? 0.0f : m_velocity.m_x;
			m_velocity.m_y = isInThirdPerson ? m_initialSpeed : m_velocity.m_y;
		}
		else if (culling[ZCollisionProxy::LEFT_PLANE]) //check west
		{
			m_velocity.m_x = m_velocity.m_x < 0.0f ? 0.0f : m_velocity.m_x;
			m_velocity.m_y = isInThirdPerson ? m_initialSpeed : m_velocity.m_y;
		}

		if (culling[ZCollisionProxy::BOTTOM_PLANE]) //check south
		{
			m_velocity.m_y = m_velocity.m_y < 0.0f ? m_initialSpeed : m_velocity.m_y;	
		}
		else if (culling[ZCollisionProxy::TOP_PLANE]) //check north
		{
			m_velocity.m_y = m_velocity.m_y > 0.0f ? m_initialSpeed : m_velocity.m_y;
		}		
	}
	// END - CHECK COLLISIONE TRA PLAYER E VIEW FRUSTUM 

	m_position += m_velocity;
	m_rotation += m_angularVelocity;
}

void ZPlayer::draw()
{
	if (!getIsVisible()) return;

	glPushMatrix();
	{
		// calcolo lerp per il 'Constant Game Speed independent of Variable FPS
		m_viewPosition = m_position + (m_velocity * m_viewInterpolation);	
		glTranslatef(m_viewPosition.m_x, m_viewPosition.m_y, m_viewPosition.m_z);
		m_viewRotation = m_rotation + (m_angularVelocity * m_viewInterpolation);
		glRotatef(m_viewRotation.m_x, 1.0f, 0.0f, 0.0f);
		glRotatef(m_viewRotation.m_y, 0.0f, 1.0f, 0.0f);
		glRotatef(m_viewRotation.m_z, 0.0f, 0.0f, 1.0f);
		// fine calcolo lerp

		m_mesh->bindTexture(m_texture);
		m_mesh->draw();
	}
	glPopMatrix();

#ifdef DEBUG
	m_boundingVolume->draw();
#endif
}
