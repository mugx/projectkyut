#include "ZAsteroid.h"

#include "ZEnemyManager.h"

#define NUM_LIVES_ASTEROID 3

ZAsteroid::ZAsteroid(int _id, ZLevel* _level, Vector3& _position)
{
	setId(_id);
	restart(_level, _position);

	m_viewPosition = Vector3(0.0f);
	m_multiplicativeFactorOfSpeed = 4.0f;
	m_initialSpeed = 0.05f;
	m_initialRotation = Vector3(0, 90, 90);
	m_rotation = m_initialRotation;

	m_mesh = ZEnemyManager::getInstance()->getMesh(_id);
	m_scale = m_mesh->m_scale;
	m_texture = ZEnemyManager::getInstance()->getTexture(_id);

	makeBoundingVolume();
}

ZAsteroid::~ZAsteroid()
{

}

void ZAsteroid::update()
{
	if (!getIsVisible() || !getIsAlive()) return;
}

void ZAsteroid::restart(ZLevel* _level, Vector3& _position)
{
	setStatus(STATUS_ALIVE);
	setNumLives(NUM_LIVES_ASTEROID);
	setCurrentLevel(_level);
	setPosition(_position);
	setIsVisible(true);
}