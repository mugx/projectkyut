#ifndef ZBullet_h
#define ZBullet_h

#include "ZAiEntity.h"

class ZBulletSystem;

class ZBullet : public ZBaseEntity
{
public:
	enum BulletType
	{
		NO_TYPE,
		LASER1,
		LASER2
	};

	explicit ZBullet();
	virtual ~ZBullet();
	virtual void draw();
	virtual void update();
	
	BulletType getType() { return m_type; }
	void setType(enum BulletType _type) { m_type = _type; }
	void setBulletSystem(ZBulletSystem* _bulletSystem) { m_bulletSystem = _bulletSystem; }
	
private:
	BulletType m_type;
	ZBulletSystem* m_bulletSystem;
};

#endif