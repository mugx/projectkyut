#ifndef ZParticleSystem_h
#define ZParticleSystem_h

#include "ZParticle.h"

class ZParticle;

class ZParticleSystem
{
public:
	virtual void initParticle(ZParticle* _particle) = 0;
	virtual void update(ZParticle* _particle) {}
	virtual void draw(ZParticle* _particle) {}
	int getNumParticles() { return m_numParticles; }
	void setNumParticles(int _numParticles) { m_numParticles = _numParticles; }

protected:
	ZParticleSystem();
	virtual ~ZParticleSystem();

private:
	int m_numParticles;
};

#endif