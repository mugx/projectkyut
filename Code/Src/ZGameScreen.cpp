#include "ZGameScreen.h"

#include "ZConst.h"
#include "ZConfig.h"
#include "ZCore.h"
#include "ZGameplaySystem.h"
#include "ZPlayerManager.h"

ZGameScreen::ZGameScreen()
: ZScreen(GAME_SCREEN)
{
	m_gameScreenHud = new ZGameScreenHud(this);
	m_gameScreenHud->refreshHud();
}

ZGameScreen::~ZGameScreen()
{
	z_delete_0(m_gameScreenHud);
}

void ZGameScreen::refreshResolution()
{
	if (!m_wantRefreshResolution)
	{
		m_wantRefreshResolution = true;
	}
}

void ZGameScreen::startGame()
{
	ZGameplaySystem::getInstance()->init();
}

void ZGameScreen::setKeyboardAction(QKeyEvent* _keyboardEvent, bool _isPressed)
{
	ZPlayerManager::getInstance()->setKeyboardAction(_keyboardEvent, _isPressed);

	if (_isPressed) return;

	if (_keyboardEvent->key() == Qt::Key_P)
	{
		ZConfig::s_isPause = !ZConfig::s_isPause;
	}
#ifdef DEBUG
	else if (_keyboardEvent->key() == Qt::Key_F1)
	{
		ZConfig::s_currentDebugSelection = ++ZConfig::s_currentDebugSelection % ZConfig::COUNT_DEBUG_SELECTION_TYPE;
	}
	else if (_keyboardEvent->key() == Qt::Key_Plus)
	{
		switch(ZConfig::s_currentDebugSelection)
		{
		case ZConfig::EXPOSURE_SELECTED: ZConfig::s_exposure += 0.01f; break;
		case ZConfig::FACTOR_SELECTED: ZConfig::s_factor += 0.01f; break;
		case ZConfig::GAMMA_SELECTED: ZConfig::s_gamma += 0.01f; break;
		case ZConfig::ZOOM_SELECTED: ZConfig::s_zoom += 1.0f; break;
		case ZConfig::BLINKING_SELECTED: 
			{
				ZConfig::s_blinking = ZMath::mod(++ZConfig::s_blinking, 100);
				break;
			}
		}
	}
	else if (_keyboardEvent->key() == Qt::Key_Minus)
	{
		switch(ZConfig::s_currentDebugSelection)
		{
		case ZConfig::EXPOSURE_SELECTED: ZConfig::s_exposure -= 0.01f; break;
		case ZConfig::FACTOR_SELECTED: ZConfig::s_factor -= 0.01f; break;
		case ZConfig::GAMMA_SELECTED: ZConfig::s_gamma -= 0.01f; break;
		case ZConfig::ZOOM_SELECTED: ZConfig::s_zoom -= 1.0f; break;
		case ZConfig::BLINKING_SELECTED: 
			{
				ZConfig::s_blinking = ZMath::mod(--ZConfig::s_blinking, 100);
				break;
			}
		}
	}
	else if (_keyboardEvent->key() == Qt::Key_R)
	{
		ZGameplaySystem::getInstance()->restartLevel();
	}
#endif
}

ZLevel* ZGameScreen::getCurrentLevel() 
{
	return ZGameplaySystem::getInstance()->getCurrentLevel();
}

void ZGameScreen::update()
{
	if (ZConfig::s_isLoading) return;

	ZGameplaySystem::getInstance()->update();
	m_gameScreenHud->update();
}

void ZGameScreen::draw()
{
	if (ZConfig::s_isLoading) return;
	
	ZGameplaySystem::getInstance()->draw();
	
	if (!m_wantRefreshResolution)
	{
		m_gameScreenHud->draw();
	}
	else
	{
		m_wantRefreshResolution = false;
		m_gameScreenHud->refreshHud();
	}
}