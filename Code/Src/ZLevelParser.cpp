#include "ZLevelParser.h"

#include <QtXml> 
#include <sstream>
#include <iostream>
#include "ZConst.h"
#include "ZBackground.h"
#include "ZSkybox.h"
#include "ZLevel.h"
#include "ZPlayerManager.h"
#include "ZEnemyManager.h"
#include "ZScenarioManager.h"

ZLevelParser::ZLevelParser(ZLevel* const _level)
: m_level(_level),
m_canContinue(false)
{
	std::stringstream fileName;
	fileName << ":/data/levels.xml";
	std::cout << "Parsing level..." << fileName.str().c_str() << std::endl;
	QFile xmlFile(fileName.str().c_str());
	QXmlInputSource source(&xmlFile);
	QXmlSimpleReader reader;
	reader.setContentHandler(this);
	reader.parse(source);
	std::cout << "Parsing level...OK" << std::endl;
}

bool ZLevelParser::startDocument()
{
	return true;
}

bool ZLevelParser::startElement(const QString& namespaceURI, const QString& localName, const QString& qName, const QXmlAttributes& attr)
{	
	m_currentTag = qName;
	m_currentAttributes = attr;

	if (m_currentTag == ZConst::TAG_LEVEL)
	{
		m_canContinue = m_currentAttributes.value("id").toUInt() == m_level->getId() ? true : false;
		m_level->m_name = m_currentAttributes.value("name").toStdString();
	}
	else if (m_canContinue && m_currentTag == ZConst::TAG_PLAYER)
	{
		int playerId = m_currentAttributes.value("id").toUInt();
		QStringList splitted = m_currentAttributes.value("position").split(",");
		Vector3 position(splitted.at(0).toFloat(), splitted.at(1).toFloat(), splitted.at(2).toFloat());
		ZPlayerManager::getInstance()->addPlayer(playerId, m_level, position);
	}
	else if (m_canContinue && m_currentTag == ZConst::TAG_ENEMY)
	{		
		int enemyId = m_currentAttributes.value("id").toUInt();
		QStringList splitted = m_currentAttributes.value("position").split(",");
		Vector3 position(splitted.at(0).toFloat(), splitted.at(1).toFloat(), splitted.at(2).toFloat());
		ZEnemyManager::getInstance()->addEnemy(enemyId, m_level, position);
	}
	else if (m_canContinue && m_currentTag == ZConst::TAG_BACKGROUND)
	{
		int enemyId = m_currentAttributes.value("id").toUInt();
		QStringList splitted = m_currentAttributes.value("position").split(",");
		Vector3 position(splitted.at(0).toFloat(), splitted.at(1).toFloat(), splitted.at(2).toFloat());
		ZScenarioManager::getInstance()->addBackground(enemyId, m_level, position);
	}
	else if (m_canContinue && m_currentTag == ZConst::TAG_SKYBOX)
	{
		int skyboxId = m_currentAttributes.value("id").toUInt();		
		ZScenarioManager::getInstance()->addSkybox(skyboxId, m_level);
	}

	return true;
}

bool ZLevelParser::characters(const QString& tag_value)
{
	// to use the value: QStringList splittedValue = tag_value.split(",");
	return true;
}

bool ZLevelParser::endElement(const QString& namespaceURI, const QString& localName, const QString& qName)
{
	m_currentTag = ZConst::TAG_UNKNOWN;
	m_currentAttributes.clear();
	m_canContinue = localName == ZConst::TAG_LEVEL ? false : m_canContinue;
	return true;
}

void ZLevelParser::printCurrentAttributes()
{
	for (unsigned int index = 0;index < m_currentAttributes.count(); ++index)
	{
		printf("Attr_name: %s, Attr_value: %s\n", m_currentAttributes.localName(index).toStdString().c_str(), m_currentAttributes.value(index).toStdString().c_str());
	}
}