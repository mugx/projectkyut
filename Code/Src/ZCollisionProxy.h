#ifndef ZCollisionProxy_h
#define ZCollisionProxy_h

#include "ZMath.h"
#include <bitset>

class ZLevel;

typedef std::bitset<8> FrustumIntersection;

class ZCollisionProxy
{
public:
	enum FrustumIntersectionType
	{		
		RIGHT_PLANE,
		LEFT_PLANE,
		BOTTOM_PLANE,
		TOP_PLANE,
		FAR_PLANE,
		NEAR_PLANE,
		EXTERN_TO_FRUSTUM,
		INTERN_TO_FRUSTUM
	};

	explicit ZCollisionProxy(ZLevel& nLevel);
	virtual ~ZCollisionProxy();

	void update();
	FrustumIntersection sphereInFrustum(const Vector3& center, float radius);

private:
	ZLevel& m_level;
};

#endif