#include "ZScreenManager.h"

#include <GL/glew.h>
#include <QGLWidget>
#include "ProjectKyut.h"
#include "ZConfig.h"
#include "ZConst.h"
#include "ZCore.h"
#include "ZSoundManager.h"
#include "ZShaderManager.h"
#include "ZPlayerManager.h"
#include "ZScreen.h"
#include "ZSplashScreen.h"
#include "ZGameScreen.h"
 
ZScreenManager::ZScreenManager()
: QGLWidget(QGLFormat(QGL::SampleBuffers | QGL::DoubleBuffer | QGL::DepthBuffer | QGL::StencilBuffer)),
m_currentScreen(0),

m_indexDisplayListBackground(0),
m_textureScene(0),
m_textureSceneStencil(0),
m_textureHighpass(0),
m_textureGaussH(0),
m_textureGaussV(0)
{
	QGLFormat& format = this->format();	
	format.setSwapInterval(0);
	format.setDirectRendering(true);

	makeDisplayList();
}

ZScreenManager::~ZScreenManager() 
{
	disconnect();
	
	z_delete_0(m_currentScreen);

	ZTextureManager::getInstance()->removeTexture(m_textureScene);
	glDeleteFramebuffersEXT(1, &m_fboScene);
	glDeleteRenderbuffersEXT(1, &m_rboScene);

	ZTextureManager::getInstance()->removeTexture(m_textureSceneStencil);
	glDeleteFramebuffersEXT(1, &m_fboSceneStencil);
	glDeleteRenderbuffersEXT(1, &m_rboSceneStencil);

	ZTextureManager::getInstance()->removeTexture(m_textureHighpass);
	glDeleteFramebuffersEXT(1, &m_fboHighpass);

	ZTextureManager::getInstance()->removeTexture(m_textureGaussH);
	glDeleteFramebuffersEXT(1, &m_fboGaussH);

	ZTextureManager::getInstance()->removeTexture(m_textureGaussV);
	glDeleteFramebuffersEXT(1, &m_fboGaussV);

	releaseDisplayList();
}

void ZScreenManager::init()
{
	thread()->setPriority(QThread::TimeCriticalPriority);
	setFocusPolicy(Qt::NoFocus);
	setMouseTracking(false);
	setAttribute(Qt::WA_TransparentForMouseEvents);
	setAttribute(Qt::WA_NoSystemBackground);
	setAttribute(Qt::WA_NoMousePropagation);
	setAutoBufferSwap(false);
	makeCurrent();

	setCurrentScreen(ZScreen::SPLASH_SCREEN);
	setFullScreen(ZConfig::s_isFullScreen);
}

void ZScreenManager::refreshBuffers()
{
	ZTextureManager::getInstance()->removeTexture(m_textureScene);
	glDeleteFramebuffersEXT(1, &m_fboScene);
	glDeleteRenderbuffersEXT(1, &m_rboScene);

	ZTextureManager::getInstance()->removeTexture(m_textureSceneStencil);
	glDeleteFramebuffersEXT(1, &m_fboSceneStencil);
	glDeleteRenderbuffersEXT(1, &m_rboSceneStencil);

	ZTextureManager::getInstance()->removeTexture(m_textureHighpass);
	glDeleteFramebuffersEXT(1, &m_fboHighpass);

	ZTextureManager::getInstance()->removeTexture(m_textureGaussH);
	glDeleteFramebuffersEXT(1, &m_fboGaussH);

	ZTextureManager::getInstance()->removeTexture(m_textureGaussV);
	glDeleteFramebuffersEXT(1, &m_fboGaussV);

	releaseDisplayList();
	makeDisplayList();

	//--- OPENGL STUFF ---//
	// FBO 3d Scene
	glGenFramebuffersEXT(1, &m_fboScene);
	//FRAME_BUFFER
	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, m_fboScene);
	m_textureScene = ZTextureManager::getInstance()->makeTexture(2);
	//DEPTH_BUFFER
	glGenRenderbuffersEXT(1, &m_rboScene);
	glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, m_rboScene);
	glRenderbufferStorageEXT(GL_RENDERBUFFER_EXT, GL_DEPTH_STENCIL_EXT, ZConfig::s_size.width(), ZConfig::s_size.height());
	glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, 0);
	glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, m_textureScene->m_idTexture, 0);
	glFramebufferRenderbufferEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT_EXT, GL_RENDERBUFFER_EXT, m_rboScene);
	glFramebufferRenderbufferEXT(GL_FRAMEBUFFER_EXT, GL_STENCIL_ATTACHMENT_EXT, GL_RENDERBUFFER_EXT, m_rboScene);
	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);

	// FBO SCENE STENCIL
	glGenFramebuffersEXT(1, &m_fboSceneStencil);
	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, m_fboSceneStencil);
	m_textureSceneStencil = ZTextureManager::getInstance()->makeTexture(1);
	//DEPTH_BUFFER
	glGenRenderbuffersEXT(1, &m_rboSceneStencil);
	glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, m_rboSceneStencil);
	glRenderbufferStorageEXT(GL_RENDERBUFFER_EXT, GL_DEPTH_STENCIL_EXT, ZConfig::s_size.width(), ZConfig::s_size.height());
	glBindRenderbufferEXT(GL_RENDERBUFFER_EXT, 0);

	glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, m_textureSceneStencil->m_idTexture, 0);
	glFramebufferRenderbufferEXT(GL_FRAMEBUFFER_EXT, GL_DEPTH_ATTACHMENT_EXT, GL_RENDERBUFFER_EXT, m_rboSceneStencil);
	glFramebufferRenderbufferEXT(GL_FRAMEBUFFER_EXT, GL_STENCIL_ATTACHMENT_EXT, GL_RENDERBUFFER_EXT, m_rboSceneStencil);
	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);

	// FBO HIGHPASS
	glGenFramebuffersEXT(1, &m_fboHighpass);
	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, m_fboHighpass);
	m_textureHighpass = ZTextureManager::getInstance()->makeTexture(0);
	glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, m_textureHighpass->m_idTexture, 0);
	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);

	// FBO GAUSS - HORIZONTAL
	glGenFramebuffersEXT(1, &m_fboGaussH);
	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, m_fboGaussH);
	m_textureGaussH = ZTextureManager::getInstance()->makeTexture(0);
	glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, m_textureGaussH->m_idTexture, 0);
	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);

	// FBO GAUSS2 - VERTICAL
	glGenFramebuffersEXT(1, &m_fboGaussV);
	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, m_fboGaussV);
	m_textureGaussV = ZTextureManager::getInstance()->makeTexture(0);
	glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, m_textureGaussV->m_idTexture, 0);
	glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);			

	ZShaderManager::getInstance()->shaderPHONG.uniform("numLights", ZConfig::s_numLights);
	//*****************************************************************************************
	// Utilizziamo il kernel separabile perche' nonostante abbiamo una write in piu' per ogni 
	// pixel, il numero di letture della textura e' ridotto da width*height--->width+height
	//
	// Settings dei pesi e degli offset
	//*****************************************************************************************
	float weights[9] = {0.0677841f, 0.0954044f, 0.121786f, 0.140999f, 0.148054f, 0.140999f, 0.121786f, 0.0954044f, 0.0677841f};
	float offsetsH[9], offsetsV[9];

	for(int i = 0; i < 9; ++i)
	{
		offsetsH[i] = (i - 4.0f) / float(ZConfig::s_size.width() / 2.0f);
		offsetsV[i] = (i - 4.0f) / float(ZConfig::s_size.height() / 2.0f);
	}

	ZShaderManager::getInstance()->shaderGAUSSH.uniform("Weights", 9, weights);
	ZShaderManager::getInstance()->shaderGAUSSH.uniform("Offsets", 9, offsetsH);
	ZShaderManager::getInstance()->shaderGAUSSV.uniform("Weights", 9, weights);
	ZShaderManager::getInstance()->shaderGAUSSV.uniform("Offsets", 9, offsetsV);
	//////////////////////////

	ZShaderManager::getInstance()->shaderTONEMAP.uniform("Tex0", 0);
	ZShaderManager::getInstance()->shaderTONEMAP.uniform("Tex1", 1);			

	ZConfig::s_exposure = 1.0f;
	ZConfig::s_factor = 1.0f;
	ZConfig::s_gamma = 0.75f;

	ZShaderManager::getInstance()->shaderTONEMAP.uniform("exposureFactor", ZConfig::s_exposure);
	ZShaderManager::getInstance()->shaderTONEMAP.uniform("bloomFactor", ZConfig::s_factor);
	ZShaderManager::getInstance()->shaderTONEMAP.uniform("gammaFactor", ZConfig::s_gamma);
}

void ZScreenManager::initializeGL()
{
	glewInit();

	// -------------------- COLOR_BUFFER -------------------- //
	//glClearColor(0.5, 0.5, 0.5, 1.0);
	glClearColor(0.0, 0.0, 0.0, 0.0);
	glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

	// -------------------- DEPTH_BUFFER -------------------- //
	glClearDepth(1.0f); // Depth buffer setup
	glEnable(GL_DEPTH_TEST); // Enable depth testing
	glDepthFunc(GL_LEQUAL); // Set type of depth test
	//glDepthFunc(GL_LESS);

	// -------------------- GL_ALPHA_TEST -------------------- //
	glEnable(GL_ALPHA_TEST);

	// -------------------- GL_BLEND -------------------- //
	glEnable(GL_BLEND);
	glAlphaFunc(GL_GREATER,0.1f);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	// -------------------- GL_CULL_FACE -------------------- //
	glEnable(GL_CULL_FACE);

	// -------------------- Line/Point Corrections -------------------- //
	/*
	glEnable (GL_LINE_SMOOTH);		
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	glHint(GL_POINT_SMOOTH_HINT, GL_NICEST);
	glHint(GL_LINE_SMOOTH_HINT, GL_DONT_CARE);
	glLineWidth (1.5);
	*/
	// -------------------- Shading -------------------- //
	glShadeModel(GL_SMOOTH);

	// Enable lighting and set the position of the light
	// -------------------- LIGHTS -------------------- //
	glDisable(GL_LIGHTING);
	GLfloat lmodel_ambient[] = {0.6f, 0.2f, 0.6f, 0.5};
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);

	GLfloat light_ambient[] = {0.2f, 0.2f, 0.2f, 0.5f};
	GLfloat light_diffuse[] = {0.2f, 0.2f, 0.2f, 0.5f};
	GLfloat light_specular[] = {0.0f, 1.0f, 0.0f, 1.0f};
	GLfloat light_position[] = {0.0f, 0.0f, +50.0f, 0.5f};

	glEnable(GL_LIGHT0);
	glLightfv (GL_LIGHT0, GL_AMBIENT, light_ambient);
	glLightfv (GL_LIGHT0, GL_DIFFUSE, light_diffuse);
	glLightfv (GL_LIGHT0, GL_SPECULAR, light_specular);
	glLightfv (GL_LIGHT0, GL_POSITION, light_position);	

	GLfloat light_ambient1[] = {0.0f, 0.0f, 0.0f, 0.0f};
	GLfloat light_diffuse1[] = {0.0f, 0.0f, 0.0f, 0.0f};
	GLfloat light_specular1[] = {0.0f, 1.0f, 0.0f, 0.5f};
	GLfloat light_position1[] = {0.0f, 0.0f, -50.0f, 0.5f};

	glEnable(GL_LIGHT1);
	glLightfv (GL_LIGHT1, GL_AMBIENT, light_ambient1);
	glLightfv (GL_LIGHT1, GL_DIFFUSE, light_diffuse1);
	glLightfv (GL_LIGHT1, GL_SPECULAR, light_specular1);
	glLightfv (GL_LIGHT1, GL_POSITION, light_position1);	

	// -------------------- MATERIALS -------------------- //
	GLfloat mat_ambient[] = {1.0f, 1.0f, 1.0f, 0.0f};
	GLfloat mat_diffuse[] = {1.0f, 1.0f, 1.0f, 1.0f};
	GLfloat mat_specular[] = {1.0f, 1.0f, 1.0f, 0.0f};
	GLfloat mat_shininess[] = {64.0f};

	glEnable(GL_COLOR_MATERIAL);
	glMaterialfv (GL_FRONT, GL_AMBIENT, mat_ambient);
	glMaterialfv (GL_FRONT, GL_DIFFUSE, mat_diffuse);
	glMaterialfv (GL_FRONT, GL_SPECULAR, mat_specular);
	glMaterialfv (GL_FRONT, GL_SHININESS, mat_shininess);

	// -------------------- GL_FOG -------------------- //
	/*
	glEnable(GL_FOG);
	GLfloat fogColor[] = {0.5f, 0.5f, 0.5f, 1.0f};
	//glFogi(GL_FOG_MODE, GL_EXP2); // fog mode
	glFogi(GL_FOG_MODE, GL_LINEAR); // fog mode
	glFogfv(GL_FOG_COLOR, fogColor); // fog color
	glFogf(GL_FOG_DENSITY, 1.1f);
	//glHint(GL_FOG_HINT, GL_NICEST);
	glHint(GL_FOG_HINT, GL_DONT_CARE);
	glFogf(GL_FOG_START, -2.0f);
	glFogf(GL_FOG_END, 2000.0f);
	*/
	// -------------------- GL_STENCIL_TEST -------------------- //
	glEnable(GL_STENCIL_TEST);

	// -------------------- GL_TEXTURE_2D -------------------- //	
	glEnable(GL_TEXTURE_2D);

	// Sounds Settings
	ZSoundManager::getInstance()->play("robotVoice");
	ZSoundManager::getInstance()->play("enigma");	
	
	// start logic loop	
	m_timer.start(0, this);
	m_time.start();
}

void ZScreenManager::resizeGL(int width, int height)
{
	GLdouble aspectRatio = static_cast<GLdouble>(width) / static_cast<GLdouble>(height);
	glViewport(0, 0, width, height);

	glMatrixMode(GL_PROJECTION); 
	glLoadIdentity();
	gluPerspective(50.0, aspectRatio, 1.0, 3000.0);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

void ZScreenManager::setFullScreen(bool _isFullScreen, int _width, int _height)
{	
	ZConfig::s_isFullScreen = _isFullScreen;
	if (ZConfig::s_isFullScreen)
	{
		showFullScreen();

		ZConfig::s_size = size();
		m_currentScreen->refreshResolution();
		refreshBuffers();
	}
	else
	{	
		setFixedSize(_width, _height);
		showMaximized();

		ZConfig::s_size = size();
		m_currentScreen->refreshResolution();
		refreshBuffers();
	}
}

void ZScreenManager::changeEvent(QEvent *event) 
{
	switch (event->type()) 
	{
	case QEvent::WindowStateChange:
		if (windowState() == Qt::WindowFullScreen)
			setCursor(Qt::BlankCursor);
		else
			unsetCursor();
		break;
	default:
		break;
	}
}

void ZScreenManager::setCurrentScreen(ZScreen::ScreenType _screenType)
{
	if (m_currentScreen)
	{
		delete m_currentScreen;
		m_currentScreen;
	}
	
	ZScreen* newScreen = 0;
	switch(_screenType)
	{
	case ZScreen::SPLASH_SCREEN:
		newScreen = new ZSplashScreen();
		break;
	case ZScreen::GAME_SCREEN:
		ZGameScreen* gameScreen = new ZGameScreen();
		gameScreen->startGame();
		newScreen = gameScreen;
		break;
	}
	m_currentScreen = newScreen;
}

void ZScreenManager::timerEvent(QTimerEvent* _event)
{
	Q_UNUSED(_event);
	static bool isPause = ZConfig::s_isPause;
	static long long next_game_tick = 0;	
	int loops = 0;

	while (m_time.elapsed() > next_game_tick && loops < ZConst::MAX_FRAMESKIP && !isPause)
	{
		m_currentScreen->update();
		next_game_tick += ZConst::TIME_OF_UPDATE;
		++loops;		
	}

	if (!ZConfig::s_isPause)
	{		
		ZConfig::s_interpolation = static_cast<double>(m_time.elapsed() + ZConst::TIME_OF_UPDATE - next_game_tick) / static_cast<double>(ZConst::TIME_OF_UPDATE);
	}
	else
	{
		ZConfig::s_interpolation = 0;
	}
	
	if (ZConfig::s_isPause && (ZConfig::s_isPause != isPause))
	{
		isPause = ZConfig::s_isPause;
		paintGL();
	}
	else if (!ZConfig::s_isPause && (ZConfig::s_isPause != isPause))
	{
		isPause = ZConfig::s_isPause;
		next_game_tick = 0;
		m_time.restart();
	}
	else if (ZConfig::s_isPause == isPause)
	{
		paintGL();
	}
}

void ZScreenManager::releaseDisplayList()
{
	glDeleteLists(m_indexDisplayListBackground, 1);
}

void ZScreenManager::makeDisplayList()
{
	m_indexDisplayListBackground = glGenLists(1);
	glNewList(m_indexDisplayListBackground, GL_COMPILE);
	glBegin(GL_QUADS);
	glTexCoord2i(0, 0); glVertex2i(0, 0);
	glTexCoord2i(1, 0); glVertex2i(ZConfig::s_size.width(), 0);
	glTexCoord2i(1, 1); glVertex2i(ZConfig::s_size.width(), ZConfig::s_size.height());
	glTexCoord2i(0, 1); glVertex2i(0, ZConfig::s_size.height());
	glEnd();
	glEndList();
}

void ZScreenManager::orthoMode(int _left, int _top, int _right, int _bottom)
{
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	glOrtho(_left, _right, _bottom, _top, 0, 1 );
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

void ZScreenManager::perspectiveMode()
{
	glMatrixMode( GL_PROJECTION );
	glPopMatrix();
	glMatrixMode( GL_MODELVIEW );
}

void ZScreenManager::drawBackground(ZTexture* _textureBackground)
{
	orthoMode(0, ZConfig::s_size.height(), ZConfig::s_size.width(), 0);
	glBindTexture(GL_TEXTURE_2D, _textureBackground->m_idTexture);
	glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
	glCallList(m_indexDisplayListBackground);
	perspectiveMode();
}

void ZScreenManager::drawBackground()
{
	orthoMode(0, ZConfig::s_size.height(), ZConfig::s_size.width(), 0);
	glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
	glCallList(m_indexDisplayListBackground);
	perspectiveMode();
}

void ZScreenManager::keyReleaseEvent(QKeyEvent* _keyboardEvent)
{
	m_currentScreen->setKeyboardAction(_keyboardEvent, false);

	if (_keyboardEvent->key() == Qt::Key_Tab && !_keyboardEvent->isAutoRepeat() && !ZConfig::s_isPause)
	{
		ZConfig::s_isPause = true;
		setFullScreen(!ZConfig::s_isFullScreen);
	}
	else if (_keyboardEvent->key() == Qt::Key_B)
	{
		#ifdef DEBUG
		ZConfig::s_isDrawBV = !ZConfig::s_isDrawBV;
		#endif
	}
	else if (_keyboardEvent->key() == Qt::Key_D)
	{
		ZConfig::s_isDrawDebug = !ZConfig::s_isDrawDebug;
		ZConfig::s_isDrawFps = ZConfig::s_isDrawDebug;
	}
	else if (_keyboardEvent->key() == Qt::Key_F)
	{
		ZConfig::s_isDrawFps = !ZConfig::s_isDrawFps;
	}
	else if (_keyboardEvent->key() == Qt::Key_S)
	{
		ZSoundManager::getInstance()->enableSound(!ZSoundManager::getInstance()->getIsSoundEnabled());
	}	
	else if (_keyboardEvent->key() == Qt::Key_V)
	{
		QGLFormat& format = this->format();
		if (format.swapInterval() == 1)
		{
			format.setSwapInterval(0);
		}
		else
		{			
			format.setSwapInterval(1);
		}
	}
	else if (_keyboardEvent->key() == Qt::Key_Escape)
	{
		ProjectKyut::quit();
	}
}

void ZScreenManager::keyPressEvent(QKeyEvent* _keyboardEvent)
{	
	m_currentScreen->setKeyboardAction(_keyboardEvent, true);
}

void ZScreenManager::paintGL()
{
	glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	m_currentScreen->draw();
	swapBuffers();
}