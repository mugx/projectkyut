#ifndef ZBulletProxy_h
#define ZBulletProxy_h

#include <vector>
#include <list>
#include <algorithm>

class ZLevel;
class ZBullet;
class ZBulletSystem;

class ZBulletProxy
{
public:	
	explicit ZBulletProxy(ZLevel& _level);
	virtual ~ZBulletProxy();

	void doEmit(ZBulletSystem* _bulletSystem);
	std::list<ZBullet*> getVisibleBullets() { return m_visibleBullets; }
	void update();
	void draw();	

private:
	std::vector<ZBullet*> m_bullets;
	std::list<ZBullet*> m_visibleBullets;
	ZLevel& m_level;
};

#endif