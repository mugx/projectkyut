#include "ZTextureManager.h"

#include <GL/glew.h>
#include <QGLWidget>
#include <QGLFramebufferObject>
#include <QGLPixelBuffer>
#include <QGLBuffer>
#include "ZConfig.h"
#include "ZCore.h"

#define INITIAL_SIZE 32
#define TEXTURE_STEP 8

ZTextureManager::ZTextureManager()
: m_idCurrentTexture(0)
{
	m_numTextures = 0;
	m_available   = INITIAL_SIZE;
	m_texIDs      = new int [INITIAL_SIZE];

	for (int i = 0;i < m_available;++i) {
		m_texIDs [i] = -1;
	}
}

ZTextureManager::~ZTextureManager()
{
	for(std::set<ZTexture*>::iterator it = m_textures.begin(), end = m_textures.end();it != end;++it)
	{
		delete (*it);
	}
	m_textures.clear();
}

void ZTextureManager::removeTexture(ZTexture* _texture)
{
	if (_texture == 0) return;
	int index = -1;
	for (int i = 0;i < m_available;++i)
	{
		if (m_texIDs [i] == _texture->m_idTexture) 
		{
			m_texIDs [i] = -1;
			index = i;
			break;
		}
	}

	if (index != -1) 
	{
		unsigned int uiGLID = (unsigned int) _texture->m_idTexture;
		glDeleteTextures (1, &uiGLID);
	}

	z_delete(_texture);
	m_textures.erase(_texture);
}

ZTexture* ZTextureManager::loadTexture(QString _filename, bool _isWrap)
{
	ZTexture* texture = new ZTexture();
	texture->m_idTexture = getNewTextureID(++m_idCurrentTexture);
	m_textures.insert(texture);

	QImage& img = QImage(_filename);
	QImage tex = QGLWidget::convertToGLFormat(img);
	glGenTextures(1, &texture->m_idTexture);
	glBindTexture(GL_TEXTURE_2D, texture->m_idTexture);

	if (_isWrap)
	{
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	}	

	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, tex.width(), tex.height(), 0, GL_RGBA, GL_UNSIGNED_BYTE, tex.bits());
	gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGBA, tex.width(), tex.height(), GL_RGBA, GL_UNSIGNED_BYTE, tex.bits());

	glBindTexture(GL_TEXTURE_2D, 0);
	texture->m_loadingResult = true;

	return texture;
}

int ZTextureManager::getNewTextureID (int _possibleTextureID) 
{
	// First check if the possible textureID has already been
	// used, however the default value is -1, err that is what
	// this method is passed from LoadTexture ()
	if (_possibleTextureID != -1) 
	{
		for (int i = 0; i < m_available;++i) 
		{
			if (m_texIDs [i] == _possibleTextureID) 
			{
				(_possibleTextureID);	// sets nTexIDs [i] to -1...
				--m_numTextures; // since we will add the ID again...
				break;
			}
		}
	}

	// Actually look for a new one
	int newTextureID;
	if (_possibleTextureID == -1) 
	{
		unsigned int nGLID;	
		glGenTextures (1, &nGLID);
		newTextureID = (int) nGLID;
	}
	else	
	{
		// If the user is handle the textureIDs
		newTextureID = _possibleTextureID;
	}

	// find an empty slot in the TexID array
	int nIndex = 0;
	while (m_texIDs [nIndex] != -1 && nIndex < m_available)
	{
		++nIndex;
	}

	// all space exaused, make MORE!
	if (nIndex >= m_available) 
	{
		int* pNewIDs = new int [m_available + TEXTURE_STEP];

		// copy the old
		for (int i = 0; i < m_available;++i)
		{
			pNewIDs [i] = m_texIDs [i];
		}

		// set the last increment to the newest ID
		pNewIDs [m_available] = newTextureID;
		// set the new to '-1'
		for (int i = 1; i < TEXTURE_STEP;++i)
		{
			pNewIDs [i + m_available] = -1;
		}

		m_available += TEXTURE_STEP;
		delete[] m_texIDs;
		m_texIDs = pNewIDs;
	}
	else 
	{
		m_texIDs[nIndex] = newTextureID;
	}

	// Welcome to our Texture Array!
	++m_numTextures;
	return newTextureID;
}

ZTexture* ZTextureManager::makeTexture(int _isHighDefinition)
{
	ZTexture* texture = new ZTexture();
	texture->m_idTexture = getNewTextureID(++m_idCurrentTexture);
	m_textures.insert(texture);

	glGenTextures(1, &texture->m_idTexture);
	glBindTexture(GL_TEXTURE_2D, texture->m_idTexture);
	if (_isHighDefinition == 0)
	{		
		texture->m_width = ZConfig::s_size.width() /2;
		texture->m_height = ZConfig::s_size.height() /2;
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, texture->m_width, texture->m_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);		
	}	
	else if (_isHighDefinition == 1)
	{		
		texture->m_width = ZConfig::s_size.width();
		texture->m_height = ZConfig::s_size.height();
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F_ARB, texture->m_width, texture->m_height, 0, GL_RGBA, GL_FLOAT, 0);		
	}
	else if (_isHighDefinition == 2)
	{		
		texture->m_width = ZConfig::s_size.width();
		texture->m_height = ZConfig::s_size.height();
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, texture->m_width, texture->m_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
	}	

	glBindTexture(GL_TEXTURE_2D, 0);
	texture->m_loadingResult = true;

	return texture;
}