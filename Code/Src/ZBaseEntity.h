#ifndef ZBaseEntity_h
#define ZBaseEntity_h

#include "ZMeshManager.h"
#include "ZMesh.h"
#include "ZTextureManager.h"

class ZMesh;
class ZBoundingVolume;
class ZLevel;

class ZBaseEntity
{
public:
	virtual void restart() {}

	void makeBoundingVolume();
	ZBoundingVolume* getBoundingVolume();
	float getInitialSpeed() const { return m_initialSpeed; }
	void setInitialSpeed(const float _initialSpeed) { m_initialSpeed = _initialSpeed; }
	Vector3& getInitialRotation() { return m_initialRotation; }
	void setInitialRotation(const Vector3& _initialRotation) { m_initialRotation = _initialRotation; }
	Vector3& getAngularVelocity() { return m_angularVelocity; }
	void setAngularVelocity(const Vector3& _angularVelocity) { m_angularVelocity = _angularVelocity; }
	Vector3& getAngularAcelaration() { return m_angularAceleration; }
	void setAngularAceleration(const Vector3& _angularAceleration) { m_angularAceleration = _angularAceleration; }
	float getMultiplicativeFactorOfSpeed() const { return m_multiplicativeFactorOfSpeed; }
	void setMultiplicativeFactorOfSpeed(const float _multiplicativeFactorOfSpeed) { m_multiplicativeFactorOfSpeed = _multiplicativeFactorOfSpeed; }
	float getViewInterpolation() const { return m_viewInterpolation; }
	void setViewInterpolation(const float _viewInterpolation) { m_viewInterpolation = _viewInterpolation; }
	Vector4& getColor() { return m_color; }
	void setColor(const Vector4 _color) { m_color = _color; }
	Vector3& getScale() { return m_scale; }
	void setScale(const Vector3& _scale) { m_scale = _scale; }
	Vector3& getViewRotation() { return m_viewRotation; }
	void setViewRotation(const Vector3& _viewRotation) { m_viewRotation = _viewRotation; }
	Vector3& getRotation() { return m_rotation; }
	void setRotation(const Vector3& _rotation) { m_rotation = _rotation; }
	Vector3& getVelocity() { return m_velocity; }
	void setVelocity(const Vector3& _velocity) { m_velocity = _velocity; }
	Vector3& getAceleration() { return m_aceleration; }
	void setAceleration(const Vector3& _aceleration) { m_aceleration = _aceleration; }
	Vector3& getViewPosition() { return m_viewPosition; }
	void SetViewPosition(const Vector3& _viewPosition) { m_viewPosition = _viewPosition; }
	Vector3& getPosition() { return m_position; }
	void setPosition(const Vector3& _position) { m_position = _position; }
	bool getIsVisible() const { return m_isVisible; }
	void setIsVisible(const bool _isVisible) { m_isVisible = _isVisible; }
	int getId() { return m_id; }
	void setCurrentLevel(ZLevel* _currentLevel) { m_currentLevel = _currentLevel; }	

protected:
	explicit ZBaseEntity();
	virtual ~ZBaseEntity() = 0;

	void setId(int _id) { m_id = _id; }

	ZMesh* m_mesh;
	ZBoundingVolume* m_boundingVolume;
	ZTexture* m_texture;
	int m_id;
	bool m_isVisible;
	float& m_viewInterpolation;
	float m_initialSpeed;
	Vector3 m_initialRotation;
	Vector3 m_angularVelocity;
	Vector3 m_angularAceleration;
	float m_multiplicativeFactorOfSpeed;
	Vector4 m_color;
	Vector3 m_scale;
	Vector3 m_viewRotation;
	Vector3 m_rotation;
	Vector3 m_velocity;
	Vector3 m_aceleration;
	Vector3 m_viewPosition;
	Vector3 m_position;
	ZLevel* m_currentLevel;
};

#endif