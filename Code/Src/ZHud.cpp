#include "ZHud.h"

#include "ZConfig.h"
#include <GL\glew.h>

ZHud::ZHud()
{
}

ZHud::~ZHud()
{
}

void ZHud::setOrthographicProjection() const
{	
	glMatrixMode(GL_PROJECTION);	
	glPushMatrix();	
	glLoadIdentity();
	gluOrtho2D(0.0, ZConfig::s_size.width(), 0.0, ZConfig::s_size.height());
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

void ZHud::resetPerspectiveProjection() const
{
	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
}

void ZHud::beginDrawString()
{	
	setOrthographicProjection();
	glPushMatrix();
	glDisable(GL_CULL_FACE);
	glDisable(GL_DEPTH_TEST);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);	// texture trasparenti
}

void ZHud::endDrawString() const
{
	glColor3f(1.0f, 1.0f, 1.0f);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glPopMatrix();
	resetPerspectiveProjection();	
}
