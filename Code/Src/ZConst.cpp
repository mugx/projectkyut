#include "ZConst.h"

float ZConst::top_color_white[4] = {1.0F, 1.0F, 1.0F, 1.0F};
float ZConst::bottom_color_violet[4] = {0.9F, 0.2F, 0.9F, 1.0F};
float ZConst::bottom_color_red[4] = {1.0F, 0.0F, 0.0F, 1.0F};
float ZConst::bottom_color_blue[4] = {0.0F, 0.0F, 1.0F, 1.0F};
float ZConst::bottom_color_black[4] = {0.0F, 0.0F, 0.0F, 1.0F};
float ZConst::bottom_color_green[4] = {0.0F, 1.0F, 0.0F, 1.0F};

const int ZConst::FIRST_LEVEL_NUMBER = 0;
const int ZConst::PLAYER_WITH_KEYBOARD_ID = 0;
const int ZConst::PLAYER_WITH_PAD_ID = 1;

const QString ZConst::PATH_SOUNDS = ":/sounds/";
const QString ZConst::PATH_PLAYER_MODELS = ":/models/player/";
const QString ZConst::PATH_PLAYER_TEXTURES = ":/textures/player/";
const QString ZConst::PATH_ENEMY_MODELS = ":/models/enemy/";
const QString ZConst::PATH_ENEMY_TEXTURES = ":/textures/enemy/";
const QString ZConst::PATH_BACKGROUND_MODELS = ":/models/background/";
const QString ZConst::PATH_BACKGROUND_TEXTURES = ":/textures/background/";
const QString ZConst::PATH_SKYBOX_TEXTURES = ":/textures/skybox/";

const QString ZConst::TAG_RESOURCES = "resources";
const QString ZConst::TAG_PLAYER_RESOURCE = "playerResource";
const QString ZConst::TAG_ENEMY_RESOURCE = "enemyResource";
const QString ZConst::TAG_BACKGROUND_RESOURCE = "backgroundResource";
const QString ZConst::TAG_SKYBOX_RESOURCE = "skyboxResource";

const QString ZConst::TAG_LEVEL = "level";
const QString ZConst::TAG_PLAYER = "player";
const QString ZConst::TAG_ENEMY = "enemy";
const QString ZConst::TAG_BACKGROUND = "background";
const QString ZConst::TAG_SKYBOX = "skybox";
const QString ZConst::TAG_UNKNOWN = "";