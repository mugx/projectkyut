#ifndef ZSplashScreen_h
#define ZSplashScreen_h

#include <GL/glew.h>

#include "ZScreen.h"
#include "ZSplashScreenHud.h"
#include "ZTexture.h"

class ZSplashScreen : public ZScreen
{
public:
	explicit ZSplashScreen();
	virtual ~ZSplashScreen();
	virtual void setKeyboardAction(QKeyEvent* _keyboardEvent, bool _isPressed);
	virtual void refreshResolution();
	virtual void update();
	virtual void draw();

private:
	static GLfloat s_verticesArray_front[12];	
	static GLfloat s_texCoordsArray_front[8];	

	ZTexture* m_textures[6];
	ZTexture* m_splashScreenTexture;
	ZSplashScreenHud* m_splashScreenHud;
};

#endif