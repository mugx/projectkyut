#include "ZGameScreenHud.h"

#include "ZConfig.h"
#include "ZConst.h"
#include "ZCore.h"
#include "ZPlayerManager.h"
#include "ZGameScreen.h"
#include "ZTextComponent.h"

ZGameScreenHud::ZGameScreenHud(ZGameScreen* _gameScreen)
: m_gameScreen(_gameScreen),

m_selectedComponent(0),
m_gameOverTextComponent(0),
m_pauseTextComponent(0),
m_levelTextComponent(0),
m_levelNumberTextComponent(0),
m_scoreTextComponent(0),
m_scoreNumberTextComponent(0),
m_livesTextComponent(0),
m_livesNumberTextComponent(0),

//----- DEBUG INFORMATION ----//
m_fpsTextComponent(0),
m_currentDebugSelectionTextComponent(0),
m_interpolationTextComponent(0),
m_exposureTextComponent(0),
m_factorTextComponent(0),
m_gammaTextComponent(0),
m_zoomTextComponent(0),
m_blinkingTextComponent(0)
{
	refreshHud();
}

ZGameScreenHud::~ZGameScreenHud()
{
	destroyHud();
}

void ZGameScreenHud::destroyHud()
{
	m_selectedComponent = 0;

	z_delete(m_gameOverTextComponent);
	z_delete(m_pauseTextComponent);
	z_delete(m_levelTextComponent);
	z_delete(m_levelNumberTextComponent);
	z_delete(m_scoreTextComponent);
	z_delete(m_scoreNumberTextComponent);
	z_delete(m_livesTextComponent);
	z_delete(m_livesNumberTextComponent);
	
	//----- DEBUG INFORMATION ----//
	z_delete(m_fpsTextComponent);
	z_delete(m_currentDebugSelectionTextComponent);
	z_delete(m_interpolationTextComponent);
	z_delete(m_exposureTextComponent);
	z_delete(m_factorTextComponent);
	z_delete(m_gammaTextComponent);
	z_delete(m_zoomTextComponent);
	z_delete(m_blinkingTextComponent);
}

void ZGameScreenHud::refreshHud()
{
	destroyHud();
	
	//--- game over ---//
	m_gameOverTextComponent = new ZTextComponent(100);
	m_gameOverTextComponent->m_text = tr("Game Over");
	m_gameOverTextComponent->m_x = ZConfig::s_size.width() / 2.0 - m_gameOverTextComponent->countPixelsWidth() / 3.0;
	m_gameOverTextComponent->m_y = ZConfig::s_size.height() / 2.0;
	m_gameOverTextComponent->setTopColor(ZConst::top_color_white);
	m_gameOverTextComponent->setBottomColor(ZConst::bottom_color_red);

	//--- pause ---//
	m_pauseTextComponent = new ZTextComponent(60);
	m_pauseTextComponent->m_text = tr("Pause");
	m_pauseTextComponent->m_x = ZConfig::s_size.width() / 2.0 - m_pauseTextComponent->countPixelsWidth() / 3.0;
	m_pauseTextComponent->m_y = ZConfig::s_size.height() / 2.0;
	m_pauseTextComponent->setTopColor(ZConst::top_color_white);
	m_pauseTextComponent->setBottomColor(ZConst::bottom_color_black);

	//--- level ---//
	m_levelTextComponent = new ZTextComponent(20);
	m_levelTextComponent->m_text = tr("Level");
	m_levelTextComponent->m_x = 10;
	m_levelTextComponent->m_y = ZConfig::s_size.height() - 10 - m_levelTextComponent->font().pointSize();
	m_levelTextComponent->setTopColor(ZConst::top_color_white);
	m_levelTextComponent->setBottomColor(ZConst::bottom_color_green);

	//--- level number ---//
	m_levelNumberTextComponent = new ZTextComponent(20);
	m_levelNumberTextComponent->m_text = QString::number(0);
	m_levelNumberTextComponent->m_x = m_levelTextComponent->m_x + m_levelTextComponent->countPixelsWidth() / 3.0;
	m_levelNumberTextComponent->m_y = m_levelTextComponent->m_y - m_levelNumberTextComponent->font().pointSize() - 5;
	m_levelNumberTextComponent->setTopColor(ZConst::top_color_white);
	m_levelNumberTextComponent->setBottomColor(ZConst::bottom_color_violet);

	//--- score ---//
	m_scoreTextComponent = new ZTextComponent(20);
	m_scoreTextComponent->m_text = tr("Score");
	m_scoreTextComponent->m_x = ZConfig::s_size.width() / 2.0 - m_scoreTextComponent->countPixelsWidth() / 3.0;
	m_scoreTextComponent->m_y = ZConfig::s_size.height() - 10 - m_scoreTextComponent->font().pointSize();
	m_scoreTextComponent->setTopColor(ZConst::top_color_white);
	m_scoreTextComponent->setBottomColor(ZConst::bottom_color_green);

	//--- score number ---//
	m_scoreNumberTextComponent = new ZTextComponent(20);
	m_scoreNumberTextComponent->m_text = QString::number(0);
	m_scoreNumberTextComponent->m_x = m_scoreTextComponent->m_x + m_scoreTextComponent->countPixelsWidth() / 3.0;
	m_scoreNumberTextComponent->m_y = m_scoreTextComponent->m_y - m_scoreNumberTextComponent->font().pointSize() - 5;
	m_scoreNumberTextComponent->setTopColor(ZConst::top_color_white);
	m_scoreNumberTextComponent->setBottomColor(ZConst::bottom_color_violet);

	//--- lives ---//
	m_livesTextComponent = new ZTextComponent(20);
	m_livesTextComponent->m_text = tr("Lives");
	m_livesTextComponent->m_x = ZConfig::s_size.width() - m_livesTextComponent->countPixelsWidth();
	m_livesTextComponent->m_y = ZConfig::s_size.height() - 10 - m_livesTextComponent->font().pointSize();
	m_livesTextComponent->setTopColor(ZConst::top_color_white);
	m_livesTextComponent->setBottomColor(ZConst::bottom_color_green);

	//--- lives number ---//
	m_livesNumberTextComponent = new ZTextComponent(20);
	m_livesNumberTextComponent->m_text = QString::number(0);
	m_livesNumberTextComponent->m_x = m_livesTextComponent->m_x + m_livesTextComponent->countPixelsWidth() / 3.0;
	m_livesNumberTextComponent->m_y = m_livesTextComponent->m_y - m_livesNumberTextComponent->font().pointSize() - 5;
	m_livesNumberTextComponent->setTopColor(ZConst::top_color_white);
	m_livesNumberTextComponent->setBottomColor(ZConst::bottom_color_violet);

	//----- DEBUG INFORMATION ----//
	//--- fps number ---//
	m_fpsTextComponent = new ZTextComponent(10);
	m_fpsTextComponent->m_text = tr("Fps");
	m_fpsTextComponent->m_x = 10;
	m_fpsTextComponent->m_y = m_scoreNumberTextComponent->m_y - m_scoreNumberTextComponent->font().pointSize() * 2;
	m_fpsTextComponent->setTopColor(ZConst::top_color_white);
	m_fpsTextComponent->setBottomColor(ZConst::bottom_color_red);

	//--- interpolation number ---//
	m_interpolationTextComponent = new ZTextComponent(10);
	m_interpolationTextComponent->m_text = tr("Interpolation");
	m_interpolationTextComponent->m_x = 10;
	m_interpolationTextComponent->m_y = m_fpsTextComponent->m_y - m_fpsTextComponent->font().pointSize() * 2;
	m_interpolationTextComponent->setTopColor(ZConst::top_color_white);
	m_interpolationTextComponent->setBottomColor(ZConst::bottom_color_red);

	//--- current debug selection ---//
	m_currentDebugSelectionTextComponent = new ZTextComponent(10);
	m_currentDebugSelectionTextComponent->m_text = tr("------------------------");
	m_currentDebugSelectionTextComponent->m_x = 10;
	m_currentDebugSelectionTextComponent->m_y = m_interpolationTextComponent->m_y - m_interpolationTextComponent->font().pointSize() * 2;
	m_currentDebugSelectionTextComponent->setTopColor(ZConst::top_color_white);
	m_currentDebugSelectionTextComponent->setBottomColor(ZConst::bottom_color_violet);

	//--- exposure number ---//
	m_exposureTextComponent = new ZTextComponent(10);
	m_exposureTextComponent->m_text = tr("Exposure");
	m_exposureTextComponent->m_x = 10;
	m_exposureTextComponent->m_y = m_currentDebugSelectionTextComponent->m_y - m_currentDebugSelectionTextComponent->font().pointSize() * 2;
	m_exposureTextComponent->setTopColor(ZConst::top_color_white);
	m_exposureTextComponent->setBottomColor(ZConst::bottom_color_violet);

	//--- factor number ---//
	m_factorTextComponent = new ZTextComponent(10);
	m_factorTextComponent->m_text = tr("Factor");
	m_factorTextComponent->m_x = 10;
	m_factorTextComponent->m_y = m_exposureTextComponent->m_y - m_exposureTextComponent->font().pointSize() * 2;
	m_factorTextComponent->setTopColor(ZConst::top_color_white);
	m_factorTextComponent->setBottomColor(ZConst::bottom_color_violet);

	//--- gamma number ---//
	m_gammaTextComponent = new ZTextComponent(10);
	m_gammaTextComponent->m_text = tr("Gamma");
	m_gammaTextComponent->m_x = 10;
	m_gammaTextComponent->m_y = m_factorTextComponent->m_y - m_factorTextComponent->font().pointSize() * 2;
	m_gammaTextComponent->setTopColor(ZConst::top_color_white);
	m_gammaTextComponent->setBottomColor(ZConst::bottom_color_violet);

	//--- zoom number ---//
	m_zoomTextComponent = new ZTextComponent(10);
	m_zoomTextComponent->m_text = tr("Zoom");
	m_zoomTextComponent->m_x = 10;
	m_zoomTextComponent->m_y = m_gammaTextComponent->m_y - m_gammaTextComponent->font().pointSize() * 2;
	m_zoomTextComponent->setTopColor(ZConst::top_color_white);
	m_zoomTextComponent->setBottomColor(ZConst::bottom_color_violet);

	//--- zoom number ---//
	m_blinkingTextComponent = new ZTextComponent(10);
	m_blinkingTextComponent->m_text = tr("Zoom");
	m_blinkingTextComponent->m_x = 10;
	m_blinkingTextComponent->m_y = m_zoomTextComponent->m_y - m_zoomTextComponent->font().pointSize() * 2;
	m_blinkingTextComponent->setTopColor(ZConst::top_color_white);
	m_blinkingTextComponent->setBottomColor(ZConst::bottom_color_violet);

	switch(ZConfig::s_currentDebugSelection)
	{
	case ZConfig::EXPOSURE_SELECTED:m_selectedComponent = m_exposureTextComponent;break;
	case ZConfig::FACTOR_SELECTED:m_selectedComponent = m_factorTextComponent;break;
	case ZConfig::GAMMA_SELECTED:m_selectedComponent = m_gammaTextComponent;break;
	case ZConfig::ZOOM_SELECTED:m_selectedComponent = m_zoomTextComponent;break;
	case ZConfig::BLINKING_SELECTED:m_selectedComponent = m_blinkingTextComponent;break;
	}
}

void ZGameScreenHud::update()
{
	if (m_levelNumberTextComponent)
	{
		m_levelNumberTextComponent->m_text = QString::number(m_gameScreen->getCurrentLevel()->getId() + 1);
	}
	
	if (m_scoreNumberTextComponent)
	{
		m_scoreNumberTextComponent->m_text = QString::number(m_gameScreen->getCurrentLevel()->getId() + 1);
	}
	
	if (m_livesNumberTextComponent)
	{
		m_livesNumberTextComponent->m_text = QString::number(ZPlayerManager::getInstance()->getPlayer(ZConst::PLAYER_WITH_KEYBOARD_ID)->getNumLives());
	}
}

void ZGameScreenHud::draw()
{
	beginDrawString();

	//---------------- draw HUD ----------------//
	if (ZConfig::s_isPause)
	{
		m_pauseTextComponent->renderText(true);
	}

	if (ZConfig::s_isGameOver)
	{
		m_gameOverTextComponent->renderText(true);
	}
	
	m_levelTextComponent->renderText();
	m_levelNumberTextComponent->renderText();
	m_scoreTextComponent->renderText();
	m_scoreNumberTextComponent->renderText();
	m_livesTextComponent->renderText();
	m_livesNumberTextComponent->renderText();

	//---------------- draw Debug ----------------//	
	if (ZConfig::s_isDrawFps)
	{
		static float fps = 0.0f;
		static int count = 0;
		static long lastTimeElapsed = 0;

		++count;
		long deltaTime = ZScreenManager::getInstance()->getTime().elapsed() - lastTimeElapsed;
		if (deltaTime >= 1000)
		{		
			fps = static_cast<float>(count) / static_cast<float>(deltaTime) * 1000.0;
			lastTimeElapsed = ZScreenManager::getInstance()->getTime().elapsed();
			count = 0;
		}
		if (m_fpsTextComponent)
		{
			m_fpsTextComponent->m_text = tr("Fps: ") + QString::number(fps);
			m_fpsTextComponent->renderText();
		}

		if (m_interpolationTextComponent)
		{
			m_interpolationTextComponent->m_text = tr("Interpolation: ") + QString::number(ZConfig::s_interpolation);
			m_interpolationTextComponent->renderText();
		}
	}

#ifdef DEBUG
	if (ZConfig::s_isDrawDebug)
	{
		//--- current selection update ---//
		if (m_selectedComponent)
		{
			m_selectedComponent->setBottomColor(ZConst::bottom_color_violet);
			switch(ZConfig::s_currentDebugSelection)
			{
			case ZConfig::EXPOSURE_SELECTED:m_selectedComponent = m_exposureTextComponent;break;
			case ZConfig::FACTOR_SELECTED:m_selectedComponent = m_factorTextComponent;break;
			case ZConfig::GAMMA_SELECTED:m_selectedComponent = m_gammaTextComponent;break;
			case ZConfig::ZOOM_SELECTED:m_selectedComponent = m_zoomTextComponent;break;
			case ZConfig::BLINKING_SELECTED:m_selectedComponent = m_blinkingTextComponent;break;
			}
			m_selectedComponent->setBottomColor(ZConst::bottom_color_green);

			//--- render text debug ---//
			m_currentDebugSelectionTextComponent->m_text = tr("------------------------");
			m_currentDebugSelectionTextComponent->renderText();

			m_exposureTextComponent->m_text = tr("Exposure: ") + QString::number(ZConfig::s_exposure);
			m_exposureTextComponent->renderText();

			m_factorTextComponent->m_text = tr("Factor: ") + QString::number(ZConfig::s_factor);
			m_factorTextComponent->renderText();

			m_gammaTextComponent->m_text = tr("Gamma: ") + QString::number(ZConfig::s_gamma);
			m_gammaTextComponent->renderText();

			m_zoomTextComponent->m_text = tr("Zoom: ") + QString::number(ZConfig::s_zoom);
			m_zoomTextComponent->renderText();

			m_blinkingTextComponent->m_text = tr("Blinking Factor: ") + QString::number(ZConfig::s_blinking);
			m_blinkingTextComponent->renderText();
		}
	}
#endif
	
	endDrawString();
}