#include "ZGameplaySystem.h"

#include "ZConst.h"
#include "ZConfig.h"
#include "ZCore.h"
#include "ZResourcesParser.h"
#include "ZCamera.h"
#include "ZPlayerManager.h"
#include "ZEnemyManager.h"
#include "ZScenarioManager.h"
#include "ZGameplaySystem.h"
#include "ZLevel.h"

ZGameplaySystem::ZGameplaySystem()
: m_currentLevel(0)
{
}

ZGameplaySystem::~ZGameplaySystem()
{
	z_delete_0(m_currentLevel);
}

void ZGameplaySystem::init()
{
	ZPlayerManager::getInstance()->resetPlayers();
	ZEnemyManager::getInstance()->resetEnemies();
	ZScenarioManager::getInstance()->resetScenario();

	z_delete_0(m_currentLevel);
	m_currentLevel = new ZLevel(ZConst::FIRST_LEVEL_NUMBER);
}

void ZGameplaySystem::restartLevel()
{
	if (ZConfig::s_isLoading) return;

	ZConfig::s_isLoading = true;

	// restart level
	int levelId = m_currentLevel->getId();
	z_delete_0(m_currentLevel);	
	m_currentLevel = new ZLevel(levelId);

	ZConfig::s_isLoading = false;
}

ZLevel* ZGameplaySystem::getCurrentLevel()
{
	return m_currentLevel;
}

void ZGameplaySystem::update()
{
	if (m_currentLevel && !ZConfig::s_isLoading)
	{
		m_currentLevel->update();
	}
}

void ZGameplaySystem::draw()
{
	if (m_currentLevel && !ZConfig::s_isLoading)
	{
		m_currentLevel->draw();		
	}
}