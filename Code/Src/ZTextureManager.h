#ifndef ZTextureManager_h
#define ZTextureManager_h

#include <iostream>
#include <set>
#include <QtGui>
#include "ZSingleton.h"
#include "ZTexture.h"

class ZTextureManager : public ZSingleton <ZTextureManager>
{
	friend class ZSingleton <ZTextureManager>;

public:
	void removeTexture(ZTexture* _texture);
	ZTexture* loadTexture(QString _filename, bool _isWrap = false);
	ZTexture* makeTexture(int _isHighDefinition);

private:
	explicit ZTextureManager();
	virtual ~ZTextureManager();
	int getNewTextureID (int _possibleTextureID);

	static ZTextureManager* s_instance;
	std::set<ZTexture*> m_textures;
	int m_numTextures;
	int m_available;
	int* m_texIDs;
	GLuint m_idCurrentTexture;
};

#endif