#ifndef ZEnemyManager_h
#define ZEnemyManager_h

#include "ZSingleton.h"
#include "ZMath.h"
#include "ZEnemy.h"
#include "ZLevel.h"
#include <algorithm>
#include <list>
#include <vector>
#include <QKeyEvent>

class ZEnemyManager : public ZSingleton <ZEnemyManager>
{
	friend class ZSingleton <ZEnemyManager>;

public:
	enum EnemyType
	{
		ASTEROID_TYPE
	};

	virtual void update();
	virtual void draw();

	void resetEnemies();
	ZMesh* getMesh(int _id);
	ZTexture* getTexture(int _id);
	void addEnemyMesh(int _id, QString _resourceName, Vector3& _scale);
	void addEnemyTexture(int _id, QString _resourceName);
	void addEnemy(int _id, ZLevel* _level, Vector3& _position);
	ZEnemy* getEnemy(int _id);
	std::vector<ZEnemy*> getEnemies() { return m_enemies; }
	std::list<ZEnemy*> getVisibleEnemies() { return m_visibleEnemies; }
	void setCurrentLevel(ZLevel* _level) { m_level = _level; }

private:
	explicit ZEnemyManager();
	virtual ~ZEnemyManager();

	ZLevel* m_level;
	std::vector<ZEnemy*> m_enemies;
	std::list<ZEnemy*> m_visibleEnemies;
	std::map<int, ZMesh*> m_meshesForEnemies;
	std::map<int, ZTexture*> m_texturesForEnemies;
};

#endif