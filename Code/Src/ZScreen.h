#ifndef ZScreen_h
#define ZScreen_h

#include <QtCore>
#include <QtGui>

class ZScreen : public QObject
{
	Q_OBJECT
public:
	enum ScreenType
	{
		NO_SCREEN = -1,
		SPLASH_SCREEN = 0,
		MENU_SCREEN,
		GAME_SCREEN
	};

	explicit ZScreen(ScreenType _screenType);
	virtual ~ZScreen();
	virtual void update() = 0;
	virtual void draw() = 0;
	virtual void setKeyboardAction(QKeyEvent* _keyboardEvent, bool _isPressed) = 0;
	virtual void refreshResolution() = 0;
	ScreenType GetType() { return m_screenType; }

protected:
	ScreenType m_screenType;
	bool m_wantRefreshResolution;
};

#endif