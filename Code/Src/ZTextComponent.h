#ifndef ZTextComponent_h
#define ZTextComponent_h

#include <QtGui>

class TextPrivate;

class ZTextComponent
{
public:
    ZTextComponent(const QFont& font, const QPen& pen);
	ZTextComponent(int _pointSize);
    virtual ~ZTextComponent();

    QFont font() const;
    QFontMetrics fontMetrics() const;
	int countPixelsWidth();
	void renderText(bool _isBlinking = false);
	void setTopColor(const float* _topColor);
	void setBottomColor(const float* _bottomColor);

	QString m_text;
	int m_x;
	int m_y;

private:
    Q_DISABLE_COPY(ZTextComponent)

    TextPrivate* m_textPrivate;
	const float* m_topColor;
	const float* m_bottomColor;
	float m_topColor_blinking[4];
	float m_bottomColor_blinking[4];
};

#endif
