#include "ZShader.h"

#include <iostream>
#include <QtGui>
#include "ZConfig.h"

ZShader::ZShader() 
: shader_id(0),
shader_fp(0),
shader_vp(0)
{

}

ZShader::~ZShader() 
{
	glDetachShader(shader_id, shader_fp);
	glDetachShader(shader_id, shader_vp);

	glDeleteShader(shader_fp);
	glDeleteShader(shader_vp);
	glDeleteProgram(shader_id);
}

void ZShader::init(const char *vsFile, const char *fsFile)
{	
	shader_vp = glCreateShader(GL_VERTEX_SHADER);	
	shader_fp = glCreateShader(GL_FRAGMENT_SHADER);

	QFile* vsF = new QFile(vsFile);
	vsF->open(QIODevice::ReadOnly | QIODevice::Text);

	QFile* fsF = new QFile(fsFile);
	fsF->open(QIODevice::ReadOnly | QIODevice::Text);

	QByteArray vsArray = vsF->readAll();
	GLint vShaderLen = (GLint) vsArray.length();
	GLubyte* vShaderSource = (GLubyte *)vsArray.data();

	QByteArray fsArray = fsF->readAll();
	GLint fShaderLen = (GLint) fsArray.length();
	GLubyte* fShaderSource = (GLubyte *)fsArray.data();

	//const QString& vsText = textFileRead(vsFile);
	//const QString& fsText = textFileRead(fsFile);	
/*
	if (vsText.isEmpty() || fsText.isEmpty()) {
		std::cerr << "Vertex Shader o Fragment Shader non trovati." << "\n" ;
		return;
	}
*/
	//const char& vs = vsText.toStdString().c_str();
	//const char& fs = fsText.toStdString().c_str();
	glShaderSource(shader_vp, 1, (const GLchar **)&vShaderSource, &vShaderLen);
	//glShaderSource(shader_vp, 1, &vs, 0);
	//glShaderSource(shader_fp, 1, &fs, 0);
	glShaderSource(shader_fp, 1, (const GLchar **)&fShaderSource, &fShaderLen);


	glCompileShader(shader_vp);
	validateShader(shader_vp, vsFile);
	glCompileShader(shader_fp);
	validateShader(shader_fp, fsFile);

	shader_id = glCreateProgram();
	glAttachShader(shader_id, shader_fp);
	glAttachShader(shader_id, shader_vp);
	glLinkProgram(shader_id);
	validateProgram(shader_id);	
}

int ZShader::getId() 
{
	return shader_id;
}

void ZShader::bind() 
{
	if (!ZConfig::s_isDrawBV)
	glUseProgram(shader_id);
}

void ZShader::unbind() 
{
	if (!ZConfig::s_isDrawBV)
	glUseProgram(0);
}

QString ZShader::textFileRead(const char *fileName) 
{
	QString text;
	if (fileName != NULL) {
		QFile file(fileName);		
		if (!file.open(QIODevice::ReadOnly)) 
		{      
			return NULL;
		}        		 
				
		QTextStream stream(&file);
		while(!stream.atEnd())
		{			
			text.append(stream.readLine() + "\n");
			//text.append(stream.readLine());
		}		
		text.append('\0');
		//qDebug () << text;
		file.close();
	}
	return text;
}

void ZShader::validateShader(GLuint shader, const char* file) 
{
	const unsigned int BUFFER_SIZE = 512;
	char buffer[BUFFER_SIZE];
	memset(buffer, 0, BUFFER_SIZE);
	GLsizei length = 0;

	glGetShaderInfoLog(shader, BUFFER_SIZE, &length, buffer);
	if (length > 0) {
		std::cout << "Shader " << shader << " (" << (file?file:"") << ") validate msg: " << buffer << "\n";
	}
}

void ZShader::validateProgram(GLuint program) 
{
	const unsigned int BUFFER_SIZE = 512;
	char buffer[BUFFER_SIZE];
	memset(buffer, 0, BUFFER_SIZE);
	GLsizei length = 0;

	memset(buffer, 0, BUFFER_SIZE);
	glGetProgramInfoLog(program, BUFFER_SIZE, &length, buffer);
	if (length > 0)
		std::cout << "Program " << program << " validate msg: " << buffer << "\n" ;

	glValidateProgram(program);
	GLint status;
	glGetProgramiv(program, GL_VALIDATE_STATUS, &status);
	if (status == GL_FALSE)
		std::cout << "Error validating program " << program << "\n" ;
}

void ZShader::uniform(const char* name, GLsizei count, const GLfloat *array)
{ 
	GLint loc, oldprog;
	glGetIntegerv(GL_CURRENT_PROGRAM, &oldprog);
	glUseProgram(shader_id);
	loc = glGetUniformLocation(shader_id, name);
	glUniform1fv(loc, count, array);
	glUseProgram(oldprog);
}

void ZShader::uniform(const char* name, GLfloat v0, GLfloat v1, GLfloat v2, GLfloat v3)
{
	GLint loc, oldprog;
	glGetIntegerv(GL_CURRENT_PROGRAM, &oldprog);
	glUseProgram(shader_id);
	loc = glGetUniformLocation(shader_id, name);
	glUniform4f(loc, v0, v1, v2, v3);
	glUseProgram(oldprog);
}

void ZShader::uniform(const char* name, GLfloat v0, GLfloat v1, GLfloat v2)
{
	GLint loc, oldprog;
	glGetIntegerv(GL_CURRENT_PROGRAM, &oldprog);
	glUseProgram(shader_id);
	loc = glGetUniformLocation(shader_id, name);
	glUniform3f(loc, v0, v1, v2);
	glUseProgram(oldprog);
}

void ZShader::uniform(const char* name, GLfloat v0, GLfloat v1)
{
	GLint loc, oldprog;
	glGetIntegerv(GL_CURRENT_PROGRAM, &oldprog);
	glUseProgram(shader_id);
	loc = glGetUniformLocation(shader_id, name);
	glUniform2f(loc, v0, v1);
	glUseProgram(oldprog);
}

void ZShader::uniform(const char* name, GLfloat value)
{
	GLint loc, oldprog;
	glGetIntegerv(GL_CURRENT_PROGRAM, &oldprog);
	glUseProgram(shader_id);
	loc = glGetUniformLocation(shader_id, name);
	glUniform1f(loc, value);
	glUseProgram(oldprog);
}

void ZShader::uniform(const char* name, GLint value)
{
	GLint loc, oldprog;
	glGetIntegerv(GL_CURRENT_PROGRAM, &oldprog);
	glUseProgram(shader_id);
	loc = glGetUniformLocation(shader_id, name);
	glUniform1i(loc, value);
	glUseProgram(oldprog);
}
