#ifndef ZShader_h
#define ZShader_h

#include <GL/glew.h>
#include <QGLWidget>
#include <QGLFramebufferObject>
#include <QGLPixelBuffer>
#include <QGLBuffer>
#include <QtGui>

class ZShader
{
public:	
	explicit ZShader();
	virtual ~ZShader();

	void validateShader(GLuint shader, const char* file);
	void validateProgram(GLuint program);
	void uniform(const char* name, GLint value);
	void uniform(const char* name, GLfloat value);
	void uniform(const char* name, GLfloat v0, GLfloat v1);
	void uniform(const char* name, GLfloat v0, GLfloat v1, GLfloat v2);
	void uniform(const char* name, GLfloat v0, GLfloat v1, GLfloat v2, GLfloat v3);
	void uniform(const char* name, GLsizei count, const GLfloat *array);
	int getId();
	void unbind();
	void bind();
	void init(const char *vsFile, const char *fsFile);

private:
	QString textFileRead(const char *fileName);

	int shader_id;
	int shader_vp;
	int shader_fp;
};

#endif
