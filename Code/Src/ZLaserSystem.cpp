#include "ZLaserSystem.h"

ZLaserSystem::ZLaserSystem(ZBaseEntity& _baseEntity)
: m_baseEntityParent(_baseEntity),
m_textureLaser(0)
{
	m_textureLaser = ZTextureManager::getInstance()->loadTexture(":/textures/weapons/textureLaser.jpg");
}

ZLaserSystem::~ZLaserSystem()
{
	ZTextureManager::getInstance()->removeTexture(m_textureLaser);
}

void ZLaserSystem::initBullet(ZBullet* _bullet)
{
	if (!_bullet) return;

	_bullet->setType(ZBullet::LASER1);
	_bullet->setPosition(m_baseEntityParent.getPosition());
	_bullet->setVelocity(Vector3(0.0f, 0.6f, 0.0f));
	_bullet->setScale(Vector3(1.0f, 0.5f, 1.0f));
	_bullet->setColor(Vector4(1.0f, 0.3f, 0.3f, 1.0f));
	_bullet->setIsVisible(true);
	_bullet->setBulletSystem(this);
}

void ZLaserSystem::update(ZBullet* _bullet)
{
	if (!_bullet->getIsVisible()) return;

	_bullet->setPosition(_bullet->getPosition() + _bullet->getVelocity());
	_bullet->setVelocity(_bullet->getVelocity() + _bullet->getAceleration());	
}

void ZLaserSystem::draw(ZBullet* _bullet)
{
	glPushMatrix();
	{
		_bullet->SetViewPosition(_bullet->getPosition() + _bullet->getVelocity() * _bullet->getViewInterpolation());	
		glTranslatef(_bullet->getViewPosition().m_x, _bullet->getViewPosition().m_y, _bullet->getViewPosition().m_z);
		_bullet->setViewRotation(_bullet->getRotation() + _bullet->getAngularVelocity() * _bullet->getViewInterpolation());	
		glRotatef(_bullet->getViewRotation().m_z, 0.0f, 0.0f, 1.0f);
		glScalef(_bullet->getScale().m_x, _bullet->getScale().m_y, _bullet->getScale().m_z);

		glBindTexture(GL_TEXTURE_2D, m_textureLaser->m_idTexture);
		for (int j = 0;j < 5;++j)
		{
			glRotatef(j, 0.0f, 1.0f, 0.0f);
			glColor4f(_bullet->getColor().m_x, _bullet->getColor().m_y, _bullet->getColor().m_z, _bullet->getColor().m_a);
			glBegin(GL_QUADS);
			glTexCoord2d (0.0, 0.0);glVertex3f (-0.1f, 0.0f, 0.0f);
			glTexCoord2d (1.0, 0.0);glVertex3f (+0.1f, 0.0f, 0.0f);
			glTexCoord2d (1.0, 1.0);glVertex3f (+0.1f, 1.0f, 0.0f);
			glTexCoord2d (0.0, 1.0);glVertex3f (-0.1f, 1.0f, 0.0f);
			glEnd();		
		}
	}
	glPopMatrix();	
}