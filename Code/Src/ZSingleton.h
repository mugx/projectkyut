#ifndef ZSingleton_h
#define ZSingleton_h

template <class T> class ZSingleton
{
public:
	static void s_destroy() 
	{
		if (s_instance)
		{
			delete s_instance;
			s_instance = 0;
		}
	}

	static T* getInstance() 
	{
		if (!s_instance)
		{
			s_instance = new T();
		}
		return s_instance;
	}
protected:
	ZSingleton& operator=(const ZSingleton&);
	
	ZSingleton() {}
	virtual ~ZSingleton() {}
	
private:
	void operator delete(void* ptr)
	{
		if (ptr)
		{
			free(ptr);
		}
	}

	static T* s_instance;
};

template<typename T> T* ZSingleton<T>::s_instance = 0;

#endif