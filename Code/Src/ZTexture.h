#ifndef ZTexture_h
#define ZTexture_h

#include <gl/glew.h>

class ZTexture
{
public:
	explicit ZTexture();
	virtual ~ZTexture();

	GLfloat* m_texCoordsArray;
	GLuint m_idTexture;
	bool m_loadingResult;
	int m_width;
	int m_height;
};

#endif