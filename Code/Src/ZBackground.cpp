#include "ZBackground.h"

#include <sstream>
#include "ZScenarioManager.h"

ZBackground::ZBackground(int _id, ZLevel* _level, Vector3& _position)
{
	setId(_id);
	setCurrentLevel(_level);
	setPosition(_position);

	m_viewPosition = Vector3(0.0f);
	m_multiplicativeFactorOfSpeed = 4.0f;
	m_initialSpeed = 0.05f;
	m_initialRotation = Vector3(0, 0, 270);
	m_rotation = m_initialRotation;	

	m_mesh = ZScenarioManager::getInstance()->getBackgroundMesh(_id);	
	m_scale = m_mesh->m_scale;
	m_texture = ZScenarioManager::getInstance()->getBackgroundTexture(_id);

	makeBoundingVolume();
}

ZBackground::~ZBackground()
{
}

void ZBackground::update()
{
}

void ZBackground::draw()
{
	glPushMatrix();
	{
		glTranslatef(m_position.m_x, m_position.m_y, m_position.m_z);
		glRotatef(180.0f, 0.0f, 0.0f, 1.0f);
		m_mesh->bindTexture(m_texture);
		m_mesh->draw();
	}
	glPopMatrix();
}