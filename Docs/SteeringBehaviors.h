/*
	PROJECT ZED, 3D Vertical Shooter in C++/OpenGL/GLSL
	Copyright (C) 2010 Andrea Mugnaini

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


/* 
* @class SteeringBehavior
* @brief A class containing the possible movement behaviors for a state agent
* @author Giovanni Peroni
*/

#ifndef STEERING_H
#define STEERING_H
#pragma once

#include "Math\\Math.h"
//#include "Enemy.h"
//#include "Spaceship.h"

class Enemy;
class Spaceship;

class SteeringBehavior
{
public:

	enum Deceleration {slow = 3, normal = 2, fast = 1};

	SteeringBehavior(Enemy* m_Agent);
	~SteeringBehavior();

	Vector3 Calculate();

private:
	Enemy* m_Agent;
	Spaceship* m_Spaceship;
	Vector3 m_SteeringForce;

	Vector3 None();
	Vector3 Seek(Vector3 targetPosition);
	Vector3 Flee(Vector3 targetPosition);
	Vector3 Arrive(Vector3 targetPosition, Deceleration deceleration);
	Vector3 Pursuit(Spaceship* evader);
	Vector3 Evade(Spaceship* pursuer);

};

#endif
