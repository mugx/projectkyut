/*
	PROJECT ZED, 3D Vertical Shooter in C++/OpenGL/GLSL
	Copyright (C) 2010 Andrea Mugnaini

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <math.h>
#include <algorithm>

#include "SteeringBehaviors.h"
#include "GameScreen.h"

SteeringBehavior::SteeringBehavior(Enemy* agent)
{
	m_Agent = agent;
	m_Spaceship = GameScreen::GetInstance()->playerManager->spaceship;
}

SteeringBehavior::~SteeringBehavior() {}


Vector3 SteeringBehavior::Calculate()
{
	// Reset the initial force
	m_SteeringForce = Vector3::ZERO;

	// Calculate the total force depending on the active behaviors
	//m_SteeringForce = None();
	//m_SteeringForce = Seek(m_Spaceship->position);
	//m_SteeringForce = Flee(m_Spaceship->position);
	//m_SteeringForce = Arrive(m_Spaceship->position, slow);
	//m_SteeringForce = Pursuit(m_Spaceship);
	m_SteeringForce = Evade(m_Spaceship);

	Math::VectTruncate(m_SteeringForce, m_Agent->max_speed);

	return m_SteeringForce;
}

Vector3 SteeringBehavior::None()
{
	return Vector3::ZERO;
}

Vector3 SteeringBehavior::Seek(Vector3 targetPosition)
{
	Vector3 direction = targetPosition - m_Agent->position;
	float distance = Math::VectLength(direction);
	if (distance > 0)
	{
		float speed = std::min<float>(distance, m_Agent->max_speed);
		Math::VectNormalize(&direction);
		Vector3 desiredVelocity = direction * speed;
		return (desiredVelocity - m_Agent->velocity);
	}
	return Vector3::ZERO;
}

Vector3 SteeringBehavior::Flee(Vector3 targetPosition)
{
	float fleeDistance = 1.0f;
	Vector3 direction = m_Agent->position - targetPosition;
	if (Math::VectLength(direction) > fleeDistance)
		return Vector3::ZERO;
	
	Math::VectNormalize(&direction);
	Vector3 desiredVelocity = direction * m_Agent->max_speed;
	return (desiredVelocity - m_Agent->velocity);
}


Vector3 SteeringBehavior::Arrive(Vector3 targetPosition, Deceleration deceleration)
{
	Vector3 toTarget = targetPosition - m_Agent->position;

	// Calculate distance to the target position
	double distance = Math::VectLength(toTarget);

	if (distance > 0)
	{
		// This is a factor to adjust the int given
		// by the Deceleration enum
		const double DecelerationTweaker = 3.0;
		
		// Speed required to reach the target, given the desired deceleration
		double speed = distance / ((double) deceleration * DecelerationTweaker);
		speed = std::min<double>(speed, m_Agent->max_speed);

		// As in Seek (this also normalizes the vector)
		Vector3 desiredVelocity = toTarget / distance * speed;

		return (desiredVelocity - m_Agent->velocity);
	}
	return Vector3::ZERO;
}

Vector3 SteeringBehavior::Pursuit(Spaceship* evader)
{
	Vector3 toEvader = evader->position - m_Agent->position;

	double RelativeHeading = Math::DotProduct(evader->rotation, m_Agent->rotation);
	
	// If the evader is ahead and facing the agent we just go straight (Seek)
	if ((Math::DotProduct(toEvader, m_Agent->rotation) > 0) &&
		(RelativeHeading < -0.95))	//acos(0.95) = 18 degrees
		return Seek(evader->position);

	// If the evader is not ahead we predict where it will be

	// The look ahead time is proportional to the distance
	// and inversely proportional to the speed of the agents
	double LookAheadTime = Math::VectLength(toEvader) / (m_Agent->max_speed + evader->speedInitial);
	return Seek(evader->position + evader->velocity * LookAheadTime);
}

Vector3 SteeringBehavior::Evade(Spaceship* pursuer)
{
	// No need to check the facing this time
	Vector3 toPursuer = (pursuer->position - m_Agent->position);

	// The look ahead time is proportional to the distance
	// and inversely proportional to the speed of the agents
	double LookAheadTime = (Math::VectLength(toPursuer) / (pursuer->speedInitial + m_Agent->max_speed));

	return Flee(pursuer->position + pursuer->velocity * LookAheadTime);
}
