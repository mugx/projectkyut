# This is my README

Project Kyut, 3D vertical shooter demo.  
[demo video](http://www.youtube.com/watch?v=4UqGEFJP5_I)  
Tech: C++, Qt, OpenGL, GLSL.  
Features:  

* Rendering pipeline with HDR, Phong, Stencil test shaders;
* Particle system;
* Collision Detection mixing Bounding Sphere and AABB;
* Frustum Culling;
* XML level scripting;
