/*
	PROJECT ZED, 3D Vertical Shooter in C++/OpenGL/GLSL
	Copyright (C) 2010 Andrea Mugnaini

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

//******************************************************************************************//
// PASS 4 - FILTRO BLOOM GAUSSIANO-Y Fragment Shader
// author: Andrea Mugnaini
// descrizione: 
//		- l'obiettivo di questo passo e' spargere il colore dei pixel piu' luminosi lungo l'asse y
//		  Offsets e' un parametro che dipende dalla risoluzione della scena.
//		  Weights deriva dalla matrice di convoluzione gaussiana.
//******************************************************************************************//

uniform float Offsets[9];
uniform float Weights[9];
uniform sampler2D Tex0;

void main (void)
{
	vec4 color = vec4(0.0, 0.0, 0.0, 1.0);
	for (int i = 0; i < 9; ++i) {
		color += (texture2D(Tex0, gl_TexCoord[0].st + vec2(0.0, Offsets[i])) * Weights[i]);
	}
	gl_FragColor = color;
}