/*
	PROJECT ZED, 3D Vertical Shooter in C++/OpenGL/GLSL
	Copyright (C) 2010 Andrea Mugnaini

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

//******************************************************************************************//
// PASS 5 - TONE_MAPPING Fragment Shader
// author: Andrea Mugnaini
// descrizione: 
//		- l'obbiettivo di questo passo e' fondere la scena finale con il passo di blur
//		  per generare una scena finale con un ampio range di crominanza, riportando
//		  poi tutto in un range ldr.
//      
// INTERPOLAZIONE LINEARE GENERICA: out = (1-a) * Target + a * Source
//******************************************************************************************//

uniform float exposureFactor;
uniform float bloomFactor;
uniform float gammaFactor;
uniform sampler2D Tex0, Tex1;

void main(void)
{
	vec4 colorTexel = texture2D(Tex0, gl_TexCoord[0].st);
	vec4 bloomTexel = texture2D(Tex1, gl_TexCoord[0].st);	
	
	//! CALCOLO DEL BLENDING	
	//*********************** ADDITIVE BLENDING COLOR_Texel + BLOOM_Texel ****************//
	gl_FragColor = colorTexel + bloomFactor * bloomTexel;
	//if (colorTexel.a == 0.0) bloomedTexel = vec4(1.0, 0.0, 0.0, 1.0);
	//gl_FragColor = bloomedTexel;
	//gl_FragColor.a = 1.0;
	/*
	if (colorTexel.a < 0.8)
	{
		gl_FragColor = (1.0 - colorTexel.a) * skyboxTexel + bloomedTexel * colorTexel.a;
		//gl_FragColor.rgb = skyboxTexel.rgb + (bloomedTexel.rgb * colorTexel.a);
	}
	else 
	{
		gl_FragColor = bloomedTexel;
	}	
	*/
	//gl_FragColor.a = 1.0;
	//gl_FragColor.rgb = (1.0 - bloomedTexel.a) * skyboxTexel.rgb * skyboxTexel.a + (bloomedTexel.a * bloomedTexel.rgb);
	//gl_FragColor = (colorTexel.rgb * colorTexel.a) + (skyboxTexel.rgb * 1.0);
	//gl_FragColor.a = 1.0;
	//gl_FragColor.rgb = skyboxTexel.rgb*1.0;
	//gl_FragColor = colorTexel;
	
	//gl_FragColor = mix(skyboxTexel, colorTexel, colorTexel.a);
	
	//************************************************************************************//
		
	//*********************** LERP BLENDING **********************************************//
	// calcolo dell' interpolazione lineare tra il texel iniziale e finale                
	//gl_FragColor = (1.0 - bloomFactor) * bloomTexel + bloomFactor * colorTexel;
	//************************************************************************************//
	
	//! TONE TexelPING
	//*********************** V1 BASATA SU VIGNETTE ***************************************//
	// una vignette e' il quadrato della distanza del pixel dal centro dello schermo
	vec2 inTex = gl_TexCoord[0].st - 0.5;                                                          
	float vignette = 1.0 - dot(inTex, inTex);                                                      
	gl_FragColor = gl_FragColor * pow(vignette, 4.0);
	gl_FragColor = (1.0 - exposureFactor) * vec4(0.0) + exposureFactor * gl_FragColor;
	gl_FragColor = pow(gl_FragColor, vec4(gammaFactor));
	//***********************************************************************************************//
	
	//*********************** V2 BASATA SU EXPOSURE e GAMMA *******************************//
	//gl_FragColor = (1.0 - exposureFactor) * vec4(0.0) + exposureFactor * gl_FragColor;
	//gl_FragColor = pow(gl_FragColor, vec4(gammaFactor));                                           
	//***********************************************************************************************//

	//*********************** V3 BASATA SU EXPOSURE e GAMMA *******************************//
	//gl_FragColor *= exposureFactor * (exposureFactor / gammaFactor + 1.0) / (exposureFactor + 1.0); 
	//***********************************************************************************************//	
}