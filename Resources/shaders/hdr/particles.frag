/*
	PROJECT ZED, 3D Vertical Shooter in C++/OpenGL/GLSL
	Copyright (C) 2010 Andrea Mugnaini

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

//******************************************************************************************//
// PASS X - Simple pass Fragment Shader
// author: Andrea Mugnaini
// descrizione: Simple pass
//******************************************************************************************//

uniform sampler2D Tex0;

void main(void)
{
	gl_FragColor = gl_Color * texture2D(Tex0, gl_TexCoord[0].st);
	
	float luma = dot(gl_FragColor.rgb, vec3(0.299, 0.587, 0.114));
	gl_FragColor = luma > 0.0 ? gl_FragColor : vec4(0.0, 0.0, 0.0, 0.0);
}

