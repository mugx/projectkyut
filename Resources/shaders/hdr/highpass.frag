/*
	PROJECT ZED, 3D Vertical Shooter in C++/OpenGL/GLSL
	Copyright (C) 2010 Andrea Mugnaini

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

//******************************************************************************************//
// PASS 2 - HIGHPASS Fragment Shader
// author: Andrea Mugnaini
// descrizione: 
//		- l'obiettivo di questo passo e' selezionare una texture con i valori di luminanza che
//        superano una certa soglia
//		- il requisito di questo pass e' lavorare su un framebuffer ridotto di 1/4 in risoluzione
//******************************************************************************************//

uniform sampler2D Tex0;

void main(void)
{
	vec4 texel = texture2D(Tex0, gl_TexCoord[0].st);	
		
	//*********************** VERSIONE 1 **********************************//
	/*
	if (texel.r > 1.0 || texel.g > 1.0 || texel.b > 1.0) {
		gl_FragColor = texel;
	}
	else {
		gl_FragColor = vec4(0.0);
	}
	*/
	//*********************************************************************//	
	
	//*********************** VERSIONE 2 **********************************//
    //texel.x = texel.x > 0.99 ? 1.0 : 0.0;
    //texel.y = texel.y > 0.99 ? 1.0 : 0.0;
    //texel.z = texel.z > 0.99 ? 1.0 : 0.0;
    //gl_FragColor = texel;
    //*********************************************************************//
    
    //*********************** VERSIONE 3 **********************************//
    float luma = dot(texel.rgb, vec3(0.299, 0.587, 0.114));
	gl_FragColor = luma > 0.2 ? texel : vec4(0.0, 0.0, 0.0, texel.a);
	//*********************************************************************//
}