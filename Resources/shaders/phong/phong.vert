/*
	PROJECT ZED, 3D Vertical Shooter in C++/OpenGL/GLSL
	Copyright (C) 2010 Andrea Mugnaini

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

//******************************************************************************************//
// PASS 1 - PER PIXEL LIGHTING (PHONG) Vertex Shader
// author: Andrea Mugnaini
// descrizione: Implementazione dell' equazione di Phong con multilight
//******************************************************************************************//

varying vec3 normal;
varying vec4 eye;
	
void main()
{	
	normal = normalize(gl_NormalMatrix * gl_Normal);
	eye = gl_ModelViewMatrix * gl_Vertex;
			
	gl_TexCoord[0] = gl_MultiTexCoord0;		
	gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
} 
