/*
	PROJECT ZED, 3D Vertical Shooter in C++/OpenGL/GLSL
	Copyright (C) 2010 Andrea Mugnaini

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

//******************************************************************************************//
// PASS 1 - PER PIXEL LIGHTING (PHONG) Fragment Shader
// author: Andrea Mugnaini
// descrizione: Implementazione dell' equazione di Phong con multilight
//******************************************************************************************//

uniform sampler2D Tex0;
uniform int numLights;
varying vec3 normal;
varying vec4 eye;

void main()
{	
	vec3 color = gl_FrontMaterial.emission.rgb + gl_LightModel.ambient.rgb * gl_FrontMaterial.ambient.rgb;
	vec3 N = normalize(normal);
	for (int i = 0;i < 2;++i)
	{
		vec3 lightDir = normalize(vec3(gl_LightSource[i].position - eye));		
		float lambert = max(dot(N, lightDir), 0.0);
		if (lambert > 0.0) {			
			float dist = length(lightDir);
			
			vec3 halfVector = normalize(gl_LightSource[i].halfVector.xyz);	
			vec4 diffuse = gl_FrontMaterial.diffuse * gl_LightSource[i].diffuse;	
			vec4 ambient = gl_FrontMaterial.ambient * gl_LightSource[i].ambient;
					
			float fattoreDiAttenuazione = 1.0 / (gl_LightSource[i].constantAttenuation + gl_LightSource[i].linearAttenuation * dist + gl_LightSource[i].quadraticAttenuation * dist * dist);
			color += fattoreDiAttenuazione * (diffuse.rgb * lambert + ambient.rgb);
		
			vec3 halfV = normalize(halfVector);
			float NdotHV = max(dot(N, halfV),0.0);
			color += fattoreDiAttenuazione * gl_FrontMaterial.specular.rgb * gl_LightSource[i].specular.rgb * pow(NdotHV,gl_FrontMaterial.shininess);
		}
	}

	vec4 texel = texture2D(Tex0, gl_TexCoord[0].st);
    gl_FragColor = vec4(texel.rgb * color.rgb, texel.a * gl_FrontMaterial.diffuse.a);
}